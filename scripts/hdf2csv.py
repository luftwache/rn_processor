# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 15:49:39 2020

@author: ec5
"""
# Standard Library import
import argparse
import numpy as np
import pandas as pd


# Argument Parser
parser = argparse.ArgumentParser()
parser.add_argument("-na", required=False, default=False, action='store_true')
parser.add_argument("-i", required=True, default=None)
parser.add_argument("-o", required=False, default='out.csv')
args = parser.parse_args()

na = args.na
ifile = args.i
ofile = args.o

if ifile is None:
    raise Exception("File Error: No input file")

idata = pd.read_hdf(ifile, 'Rn')
cols = {  # add units
    'ExFlow': 'external_flow',
    'InFlow': 'internal_flow',
    'HV': 'voltage',
    'ULD': 'ULD',
    'TankP': 'differential_pressure',
    'AirT': 'ambient_head_temp',
    'Temp': 'detector_temp',
    'RelHum': 'detector_RH',
    'Press': 'detector_pressure',
    'background': 'background',
    'calibration_factor': 'calibration_factor',
    'radon': 'radon',
    'radon_uncertainty': 'radon_uncertainty',
    'radon_stp': 'radon_stp',
    'radon_uncertainty_stp': 'radon_uncertainty_stp'
    }
odata = pd.DataFrame(np.nan, index=idata.index, columns=cols.values())
for k, v in cols.items():
    odata[v] = idata[k] if k in idata else 0.
if na:
    odata.loc[idata['Flag_a'] != "0"] = np.nan
odata['date'] = odata.index.strftime('%y%m%d')
odata['time'] = odata.index.strftime('%H%M')
odata.replace(0, 0.000001)
selection = ['date', 'time']+list(cols.values())
odata[selection].to_csv(ofile, sep='\t', na_rep='NaN', index=False)
