#!/bin/bash

set -e

idir=$1
pattern=$2
ofile=$3

first=1
for ifile in $(find $idir -maxdepth 1 -regextype posix-extended -regex $pattern | sort) ; do
	imfile=$(dirname $ofile)/int_$(basename "${ifile%.hdf}.csv")
	python hdf2csv.py -na -i $ifile -o $imfile
	if [ "$first" ] ; then
		cat $imfile > $ofile
		first=
	else
		cat $imfile | tail -n +2 >> $ofile
	fi
	rm $imfile
done
