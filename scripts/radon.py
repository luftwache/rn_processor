# -*- coding: utf-8 -*-
r"""
Created on Tue Jan 28 16:14:10 2020

@author: Edward Chung (edward.chung@npl.co.uk)

radon.py

Executable script to process ANSTO Radon measurements.

To use (automatic):
    1) Update the "Settings" section.
    2) If further preprocessing is needed, use "Additional Settings" section.
    3) Run the script,
To use (interactive)
    1) Run and use the GUI when they appear.

Note:
    For GCCompare, set `settings['temporal']['lag'] = '0H'`, i.e. no
    compensation for time deconvolution.

"""
import copy
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import re
import sys
import Rn_processor.io
import Rn_processor.routines
import Rn_processor.gui
import Rn_processor.util
import Rn_processor.plotter


# =============================================================================
# Settings
# =============================================================================
# Interactive mode
#   If True, each step will have tk window to check/update settings.
#   Rest of this section will be considered as the default value for the
#       settings.
interactive = True

# Site name - used for output file name
site = 'HFD'
# File/Directory paths
# input file
ifile = (
    "T:/EAM/TECHNICAL/Heathfield/Technical/Data/Radon/Raw_Data/HeJan20e.CSV"
    )
# output directory for processed data
odir = "T:/EAM/TECHNICAL/Heathfield/Technical/Data/Radon/Analysed_Data/"
# output directory for figures
fdir = (
        "T:/EAM/TECHNICAL/Heathfield/Technical/Data/Radon/Analysed_Data/"
        "Figures/"
        )

# Settings on data processing
#   'time0' and 'time1' is the start and end time, respectively.
#       If you do not want to specify the limits, set them to ''.
#       'time0': empty means start date = start of data.
#       'time1': empty means end date = end of data.
settings = {
    # Background
    'bg': {
        # Calculation Mode
        #   'calc' - Calculate from the month file
        #   'last' - Read the result of last valid event
        #   'file' - Read from file
        #   'cons' - Use the provided constant
        #   all but 'calc' will ignore all other settings but comment
        'mode': 'calc',
        # Name of the file to read if mode == 'file', else will be ignored
        'file': '',
        # Constant to use if mode == 'cons', else will be ignored
        'constant': 0.0,
        # Time window for the event
        #   Required if the event occurs multiple times
        'time0': '2020-01-15 00:00',
        'time1': '2020-01-16 01:30',
        # Timestamps to exclude
        'exclude': '',
        # n measurements to skip at the front: when instrument is reaching down
        #   to the background level
        'dt_decay': '5.5H',
        # n measurements to include after the flagged event
        #   Negative numbers excludes from the rear
        #   If time1 is '', dt_recovery >= 0
        'dt_recovery': '1.5H',
        # Comment
        'comments': ''
        },
    # Calibration
    'cal': {
        # Calculation Mode
        #   'calc' - Calculate from the month file
        #   'last' - Read the result of last valid event
        #   'file' - Read from file
        #   'cons' - Preset constant
        #   all but 'calc' will ignore all other settings but comment
        'mode': 'calc',
        # Name of the file to read if mode == 'file', else will be ignored
        'file': '',
        # Constant to use if mode == 'cons', else will be ignored
        'constant': 0.0,
        # Time window for the event
        #   Required if the event occurs multiple times
        'time0': '2020-01-12 08:00',
        'time1': '2020-01-12 19:00',
        # Injection duration
        #   Duration = End of injection - time0
        'dt_injection': '5H',
        # n measurements to include after the flagged event
        # Negative numbers excludes from the rear
        #   If time1 is '', dt_recovery >= 0
        'dt_recovery': '0H',
        # Comment
        'comments': ''
        },
    # Temporal setting of the data to process
    #   Data outside the temporal range, or in the exlude list will not be read
    'temporal': {
        # Time window for the event
        'time0': '2020-01-11 00:00',
        'time1': '',
        # Lag time for time deconvolution
        'lag': '0H'
        }
    }
# Invalid flag. 0 = valid, 1 = invalid.
invalid = {
    'bg': 0,
    'cal': 0
    }
# No Event flag. 0 = event occured, 1 = no event occurred.
noevent = {
    'bg': 0,
    'cal': 0
    }
# Read in settings if it was performed before.
read_set = True

# =============================================================================
# Additional settings
#   Customised calculation for calculation mode in either set_bg or set_cal
#   Use 'comment' in settings to describe method
# =============================================================================
# e.g
# settings['bg']['mode'] = pd.read_csv('a file').mean()
# settings['bg']['comment'] = 'Using the mean value of "a file"'

# =============================================================================
# Constants
# =============================================================================
# NoneType
NoneType = type(None)
# Tab size for prints
tab = 4
# 30 minutes timedelta
td_30m = pd.to_timedelta('30min')

# =============================================================================
# Check settings
# =============================================================================
settings_dtypes = {
    # parameter: [
    #     (acceptable types), {'', 'file', 'time', 'timedelta', 'times'}
    #     ]
    'site': [(str,), ''],
    'idir': [(str,), 'path'],
    'odir': [(str,), 'path'],
    'fdir': [(str,), 'path'],
    'mode': [(str,), ''],
    'file': [(str,), 'path'],
    'constant': [(float,), ''],
    'time0': [(str,), 'time'],
    'time1': [(str,), 'time'],
    'exclude': [(str,), 'times'],
    'dt_decay': [(str,), 'timedelta'],
    'dt_injection': [(str,), 'timedelta'],
    'dt_recovery': [(str,), 'timedelta'],
    'comments': [(str,), ''],
    'lag': [(str,), 'timedelta'],
    }
settings_names = {
    'site': 'Site Code',
    'idir': 'Input File',
    'odir': 'Output Dir',
    'fdir': 'Figure Dir',
    'mode': 'Mode',
    'file': 'Input File',
    'constant': 'Constant',
    'time0': 'Start Time',
    'time1': 'End Time',
    'exclude': 'Excluded Times',
    'dt_decay': 'Decay Duration',
    'dt_injection': 'Injection Duration',
    'dt_recovery': 'Recovery Duration',
    'comments': 'Comments',
    'lag': 'Timestamp Shift'
    }
if interactive:
    # Configure initial settings
    sw = {}
    sw['init'] = Rn_processor.set_widget.SettingsWidget(
        title='Initial',
        settings={
            'site': site,
            'idir': ifile,
            'odir': odir,
            'fdir': fdir,
            },
        dtypes=settings_dtypes,
        names=settings_names
        )
    sw['init'].draw_pane0()
    sw['init'].mainloop()
    site = sw['init'].settings['site']
    ifile = sw['init'].settings['idir']
    odir = sw['init'].settings['odir']
    fdir = sw['init'].settings['fdir']
else:
    # Check all settings
    for ck, calc_set in settings.items():
        if 'mode' not in calc_set:
            for k, v in calc_set.items():
                Rn_processor.util.istype(
                    k, v, settings_dtypes[k][0], settings_dtypes[k][1], 'raise'
                    )
            continue
        coef_mode = calc_set['mode']
        if (isinstance(coef_mode, str) and
                coef_mode not in ['calc', 'last', 'file', 'cons']):
            raise Exception(
                f"settings[{ck}][\'mode\']"
                f" only accepts the following strings\n"
                f"    ['calc', 'last', 'file', 'cons']"
                )
        for k, v in calc_set.items():
            Rn_processor.util.istype(
                k, v, settings_dtypes[k][0], settings_dtypes[k][1], 'raise'
                )

# Copy settings
settings_raw = copy.deepcopy(settings)

# =============================================================================
# Hard-coded settings
# =============================================================================
# Skip saving at the end if set to False. Use for test purposes.
save = False
# Suppress matplotlib interactive mode. Otherwise windows of plots can appear
#   outside of the GUI.
plt.ioff()
# Name of the processed data
var_name = {
    'bg': 'Background',
    'cal': 'Calibration',
    'Rn': 'Radon mBq m-3',
    'flag': 'Flag_a',
    'comments': 'Comments_a',
    }
# Timestamp of the month of the file
t0 = pd.to_datetime(
    re.match('([A-Za-z0-9]{2})([\w]{5})', os.path.basename(ifile)).groups()[1],
    format='%b%y'
    )
t0_Ym = t0.strftime('%Y%m')
# Output files: Final
#   o_csv = processed data
#   c_csv = coefficients (background, calibration factor) only
#   s_csv = settings
hdf_key = 'Rn'
o_hdf = os.path.join(odir, f"{site}_222Rn_{t0.year}.hdf")
r_hdf = os.path.join(odir, f"{site}_222Rn_{t0.year}_raw_t.hdf")
c_hdf = os.path.join(odir, f"{site}_222Rn_{t0.year}_coeff.hdf")
s_hdf = os.path.join(odir, f"{site}_222Rn_{t0.year}_setting.hdf")
o_csv = os.path.join(odir, f"{site}_222Rn_{t0.year}.csv")
r_csv = os.path.join(odir, f"{site}_222Rn_{t0.year}_raw_t.csv")
c_csv = os.path.join(odir, f"{site}_222Rn_{t0.year}_coeff.csv")
s_csv = os.path.join(odir, f"{site}_222Rn_{t0.year}_setting.csv")
# Output files: if mode == 'calc'
c_hdf_raw = os.path.join(odir, f"{site}_222Rn_{t0.year}_coeff_raw.hdf")
s_hdf_raw = os.path.join(odir, f"{site}_222Rn_{t0.year}_setting_raw.hdf")
c_csv_raw = os.path.join(odir, f"{site}_222Rn_{t0.year}_coeff_raw.csv")
s_csv_raw = os.path.join(odir, f"{site}_222Rn_{t0.year}_setting_raw.csv")
# Figures - for LLD
fig_file = {
    'bg': os.path.join(fdir, f"{site}_222Rn_{t0_Ym}_bg.png"),
    'cal': os.path.join(fdir, f"{site}_222Rn_{t0_Ym}_cal.png"),
    'obs': os.path.join(fdir, f"{site}_222Rn_{t0_Ym}_obs.png")
    }
# Calculation events
calc_flag = {'bg': 1, 'cal': 2}
calc_type_str = {'bg': 'background', 'cal': 'calibration'}
# Figure kwargs
fig_kwargs = {'figsize': [5, 3], 'dpi': 200}
ax_kwargs = {'rect': [0.8/5, 0.6/3, 4.15/5, 2.35/3]}
fig_main_kwargs = {'figsize': [5, 3], 'dpi': 300}
ax_main_kwargs = {'rect': [0.6/5, 0.4/3, 4.35/5, 2.55/3]}

# Variables used for inspection
var_insp = [
        'ExFlow', 'InFlow', 'ULD', 'LLD',
        'AirT', 'HV', 'Press', 'TankP', 'RelHum', 'Temp', 'Flag'
        ]
# Plot variables - for diagnostics, must include LLD
plot_vars = ['LLD', *[k for k in var_insp if k != 'LLD']]
# Flags
flags = ['0', '1', '2', '3', '4', 'F']
pass_flags = ['0']

# =============================================================================
# Read files
# =============================================================================
# Read settings if they exist
if (read_set and os.path.exists(s_hdf) and
    t0 in Rn_processor.io.r_settings(s_hdf, hdf_key).index
    ):
    def_settings = {
        'file': '',
        'time0': '',
        'time1': '',
        'comment': ''
        }
    past_settings = Rn_processor.io.r_settings(s_hdf, hdf_key).loc[t0]
    past_coeffs_raw = Rn_processor.io.r_coeffs(c_hdf_raw, hdf_key).loc[t0]
    for k, v in past_settings.groupby(level=0):
        if (k in noevent and
            past_coeffs_raw[f'{var_name[k]}_Flag'] == 99
            ):
            noevent[k] = 1
        if (k in invalid and
            past_coeffs_raw[f'{var_name[k]}_Flag'] == 1
            ):
            invalid[k] = 1
        for k1, v1 in def_settings.items():
            if k1 not in v[k]:
                continue
            if pd.isnull(v[k][k1]):
                past_settings[k][k1] = v1
        for k1, v1 in v[k].items():
            if settings_dtypes[k1][0] == (int,):
                past_settings[k][k1] = int(v1)#v1.astype(int)
            elif settings_dtypes[k1][0] == (float,):
                past_settings[k][k1] = float(v1)
            elif settings_dtypes[k1][0] == (list,):
                past_settings[k][k1] = [
                    i.strip() for i in v1.strip('[]').split(',') if i.strip()
                    ]
    settings = {
        k: v[k].to_dict()
        for k, v in past_settings.groupby(level=0)
        }

# Call settings
if interactive:
    sw['temporal'] = Rn_processor.set_widget.SettingsWidget(
        title='Select Dates',
        settings=settings['temporal'],
        dtypes=settings_dtypes,
        names=settings_names
        )
    sw['temporal'].draw_pane0()
    sw['temporal'].mainloop()
# Read data file
time0 = Rn_processor.util.na_date(settings['temporal']['time0'])
time1 = Rn_processor.util.na_date(settings['temporal']['time1'])
idata = pd.read_csv(
    ifile, sep=',', skipinitialspace=True,
    parse_dates={'Datetime': ['Year', 'Month', 'DOM', 'Time']},
    index_col='Datetime',
    keep_date_col=True,
    dtype={
        **{k: np.int16 for k in ['Year', 'DOY', 'Month', 'DOM', 'Flag']},
        **{k: str for k in ['Time', 'Comments']},
        },
    na_values=[-9999]
    )[time0:time1]
idata.rename(columns={'DOM': 'Day'}, inplace=True)
# Flag
flag = idata['Flag']

# =============================================================================
# Main
# =============================================================================
# Close all figures from instance
plt.close('all')
# Date used for calculation (inclusive)
date = {}
# Date to not consider as observation (inclusive)
date_exc = {}
# Figures and Axes
fig = {}
ax = {}
# Coefficients
results = {}
coeff = {}
coeff_raw = {}
# Calculate
for ck, calc_set in settings.items():
    # Check if it is an event
    if 'mode' not in calc_set:
        continue
    # Configure calculation settings if interactive
    if interactive:
        # Set GUI
        sw[ck] = Rn_processor.set_widget.SettingsWidget(
            title=f'{var_name[ck]} Settings',
            settings=settings[ck],
            dtypes=settings_dtypes,
            names=settings_names
            )
        sw[ck].idata = idata
        sw[ck].plot_vars = plot_vars
        sw[ck].fig_main_kwargs = fig_main_kwargs
        sw[ck].ax_main_kwargs = ax_main_kwargs
        sw[ck].fig_kwargs = fig_kwargs
        sw[ck].ax_kwargs = ax_kwargs
        # Draw panes
        sw[ck].draw_pane0()
        sw[ck].draw_pane1()
        sw[ck].lock_save()
        # Invalid and No Event checkboxes
        if noevent[ck]:
            sw[ck].check_noevent.select()
        elif invalid[ck]:
            sw[ck].check_invalid.select()
        # Allocate functions
        if ck == 'bg':
            sw[ck].calc_func = Rn_processor.routines.process_bg
            sw[ck].plot_func = Rn_processor.routines.plot_bg
        else:
            sw[ck].calc_func = Rn_processor.routines.process_cal
            sw[ck].plot_func = Rn_processor.routines.plot_cal
        # Start GUI
        sw[ck].mainloop()
        # Get results
        if sw[ck].var_noevent.get():
            if calc_set['mode'] == 'calc':
                calc_set['mode'] = 'last'
        else:
            results[ck] = sw[ck].results
            if ck == 'bg':
                coeff_raw[ck] = results[ck]['bg_mean']
            else:
                coeff_raw[ck] = results[ck]['cal_factor']
            date[ck] = results[ck]['date']
            date_exc[ck] = results[ck]['date_exc']
            fig[ck] = sw[ck].fig_main
            ax[ck] = sw[ck].ax_main
            if sw[ck].var_invalid.get():
                settings_raw[ck].update(sw[ck].values_calc)
                invalid[ck] = 1
                if calc_set['mode'] == 'calc':
                    calc_set['mode'] = 'last'
            else:
                invalid[ck] = 0
            plt.close(fig[ck])
    elif calc_set['mode'] == 'calc' and not noevent[ck]:
        fig[ck] = plt.figure(**fig_main_kwargs)
        ax[ck] = fig[ck].add_axes(
                ax_main_kwargs['rect'],
                **{k: v for k, v in ax_main_kwargs.items() if k != 'rect'}
                )
        if ck == 'bg':
            results[ck] = Rn_processor.routines.process_bg(calc_set, idata)
            if not invalid[ck]:
                coeff_raw[ck] = results[ck]['bg_mean']
            Rn_processor.routines.plot_bg(
                fig[ck], ax[ck], idata, 'LLD', **results[ck]
                )
        else:
            results[ck] = Rn_processor.routines.process_cal(calc_set, idata)
            if not invalid[ck]:
                coeff_raw[ck] = results[ck]['cal_factor']
            Rn_processor.routines.plot_cal(
                fig[ck], ax[ck], idata, 'LLD', **results[ck]
                )
        date[ck] = results[ck]['date']
        date_exc[ck] = results[ck]['date_exc']
        fig[ck].savefig(fig_file[ck], dpi=300)
        plt.close(fig[ck])
        if ck == 'bg':
            print(
                *[
                    f"Summary: Background {var_name[ck]}",
                    f"{'':{1*tab}s}Inputs",
                    *[
                        f"{'':8s}{k:15.15s} {v}"
                        for k, v in calc_set.items()
                        ],
                    f"{'':{1*tab}s}Outputs",
                    f"{'':{2*tab}s}{'start time':15.15s} {date[ck][0]}",
                    f"{'':{2*tab}s}{'end time':15.15s} {date[ck][-1]}",
                    f"{'':{2*tab}s}{'bg_mean':15.15s}"
                    f" {results[ck]['bg_mean']}",
                    f"{'':{2*tab}s}{'Invalid':15.15s} {not invalid[ck]}"
                    ],
                sep='\n', end='\n\n'
                )
        else:
            print(
                *[
                    f"Summary: Calibration {var_name[ck]}",
                    f"{'':{1*tab}s}Inputs",
                    *[
                        f"{'':{2*tab}s}{k:15.15s} {v}"
                        for k, v in calc_set.items()
                        ],
                    f"{'':{1*tab}s}Outputs",
                    f"{'':{2*tab}s}{'start time':15.15s} {date[ck][0]}",
                    f"{'':{2*tab}s}{'end time':15.15s} {date[ck][-1]}",
                    f"{'':{2*tab}s}{'cal_factor':15.15s}"
                    f" {results[ck]['cal_factor']}",
                    f"{'':{2*tab}s}{'NPCPS_amb':15.15s}"
                    f" {results[ck]['NPCPS_amb']}",
                    f"{'':{2*tab}s}{'NPCPS_idx':15.15s}"
                    f" {results[ck]['NPCPS_idx']}",
                    f"{'':{2*tab}s}{'Invalid':15.15s} {not invalid[ck]}"
                    ],
                sep='\n', end='\n\n'
                )
    if calc_set['mode'] != 'file':
        calc_set['file'] = ''
    if calc_set['mode'] != 'cons':
        calc_set['constant'] = 0.0
    # Remove settings if coefficient was never calculated
    if ck not in coeff_raw:
        calc_set['time0'] = ''
        calc_set['time1'] = ''
        calc_set['dt_recovery'] = 0
        if ck == 'bg':
            calc_set['dt_decay'] = 0
            calc_set['exclude'] = ''
        else:
            calc_set['dt_injection'] = '0H'
        if calc_set['mode'] == 'calc':
            calc_set['mode'] = 'last'
    if calc_set['mode'] == 'calc':
        coeff[ck] = coeff_raw[ck]
    elif calc_set['mode'] == 'cons':
        coeff[ck] = np.float64(calc_set['constant'])
    elif calc_set['mode'] == 'last':
        coeff[ck] = Rn_processor.util.get_last_valid(
            [
                os.path.join(odir, f)
                for f in os.listdir(odir)
                if f.endswith('coeff_raw.hdf')
                ],
            t0, [var_name[ck]], flags=[0]
            )[var_name[ck]]
    elif calc_set['mode'] == 'file':
        # tbd
        pass
idata[var_name['bg']] = coeff['bg']
idata[var_name['cal']] = coeff['cal']

# =============================================================================
# Radon Activity - processed data
# =============================================================================
idata[var_name['Rn']] = (
    (idata['LLD'] - coeff['bg']) / 1800 / coeff['cal'] * 1.e3
    )
idata[var_name['flag']] = '0'
idata[var_name['comments']] = ''
#exc_date = pd.concat([v.to_series() for v in date.values()]).index

idata_idx = idata.index.to_series()

#idata.loc[flag != 0, var_name] = np.nan
idata.loc[
    idata.index.isin(
        pd.concat([
            idata_idx[idata_idx.between(v[0], v[-1], inclusive=True)]
            for k, v in date_exc.items()
            ]).index
        ),
    var_name['Rn']
    ] = np.nan
for k, v in date_exc.items():
    idata.loc[
        idata_idx.between(
            v[0], v[-1], inclusive=True
            ),
        [var_name['flag'], var_name['comments']]
        ] = [str(calc_flag[k]), f"{var_name[k]} Event"]

# Inspect data
if interactive:
    sw['inspect'] = Rn_processor.set_widget.FlagWidget(
        title='Inspect',
        idata=idata,
        )
    sw['inspect'].plot_vars = (
        [var_name['Rn']] + var_insp
        + [var_name['bg'], var_name['cal'], var_name['flag']]
        )

    sw['inspect'].flag_name = var_name['flag']
    sw['inspect'].comments_name = var_name['comments']
    sw['inspect'].fig_main_kwargs = fig_main_kwargs
    sw['inspect'].ax_main_kwargs = ax_main_kwargs
    sw['inspect'].fig_kwargs = fig_kwargs
    sw['inspect'].ax_kwargs = ax_kwargs
    sw['inspect'].flags = flags
    sw['inspect'].pass_flags = pass_flags
    sw['inspect'].draw_pane0()
    sw['inspect'].draw_pane1()
    sw['inspect'].mainloop()

# Apply lag time
odata = idata.copy()
odata.index = idata.index + pd.to_timedelta(settings['temporal']['lag'])

# =============================================================================
# Post-processing
# =============================================================================
print("Final Summary")
print(f"{'':{1*tab}s}{var_name['bg']:19s} {coeff['bg']}")
print(f"{'':{1*tab}s}{var_name['cal']:19s} {coeff['cal']}")

df_settings = pd.concat(
    [
        pd.Series(
            [v2],
            dtype=(
                'int8' if isinstance(v2, int) else
                'float64' if isinstance(v2, float) else
                'str'
                ),
            index=[t0]
            )
        for k1, v1 in settings.items()
        for k2, v2 in v1.items()
        ],
    axis=1,
    keys = [(k1, k2) for k1, v1 in settings.items() for k2, v2 in v1.items()],
    )

df_coeffs = pd.DataFrame(
    {
        f"{var_name['bg']}": coeff['bg'],
        f"{var_name['cal']}": coeff['cal']
        },
    index=[t0], dtype='float64'
    )
df_settings_raw = pd.concat(
    [
        pd.Series(
            (
                [v2] if k1 in coeff_raw else
                [99] if isinstance(v2, int) else
                [np.nan] if isinstance(v2, float) else
                ['']
                ),
            dtype=(
                'int8' if isinstance(v2, int) else
                'float64' if isinstance(v2, float) else
                'str'
                ),
            index=[t0], name=(k1, k2)
            )
        for k1 in coeff
        for k2, v2 in settings_raw[k1].items()
        ],
    axis=1,
    )
df_coeffs_raw = pd.concat(
    [
        v2
        for k1 in coeff
        for v2 in [
            pd.Series(
                [coeff_raw[k1]] if k1 in coeff_raw else [np.nan],
                index=[t0], dtype='float64', name=f'{var_name[k1]}'
                ),
            pd.Series(
                [invalid[k1]] if k1 in coeff_raw else [99],
                index=[t0], dtype='int8', name=f'{var_name[k1]}_Flag'
                )
            ]
        ],
    axis=1,
    )

if not save:
    sys.exit()

# Save files
print("Save Files")
cols = [
        'Year', 'Month', 'Day', 'Time',
        var_name['Rn'], 'ExFlow', 'InFlow', 'ULD',
        'AirT', 'HV', 'Press', 'TankP', 'RelHum', 'Temp',
        var_name['bg'], var_name['cal'],
        var_name['flag'], var_name['comments']
        ]
print(f"{'':{1*tab}s}Data")
# Save raw processed data for GCCompare
Rn_processor.io.w_data(odata, cols, o_hdf, fmt='hdf', key=hdf_key)
Rn_processor.io.w_data(odata, cols, o_csv, fmt='csv', key=hdf_key, hdf_file=o_hdf)
Rn_processor.io.w_data(idata, cols, r_hdf, fmt='hdf', key=hdf_key)
Rn_processor.io.w_data(
    Rn_processor.util.flag_fail2nan(
        Rn_processor.io.r_data(r_hdf, fmt='hdf', key=hdf_key),
        var_name['flag'], pass_flags
        )[cols],
    cols, r_csv, fmt='csv', key=hdf_key, hdf_file=r_hdf
    )

print(f"{'':{1*tab}s}Settings")
Rn_processor.io.w_settings(
    df_settings,
    s_hdf,
    columns=(df_settings.columns if not os.path.exists(s_hdf) else []),
    key=hdf_key
    )
print(f"{'':{1*tab}s}Coefficients")
Rn_processor.io.w_coeffs(
    df_coeffs,
    c_hdf,
    columns=(df_coeffs.columns if not os.path.exists(c_hdf) else []),
    key=hdf_key
    )
print(f"{'':{1*tab}s}Settings (Raw)")
Rn_processor.io.w_settings(
    df_settings_raw,
    s_hdf_raw,
    columns=(df_settings_raw.columns if not os.path.exists(s_hdf_raw) else []),
    key=hdf_key
    )
print(f"{'':{1*tab}s}Coefficients (Raw)")
Rn_processor.io.w_coeffs(
    df_coeffs_raw,
    c_hdf_raw,
    columns=(df_coeffs_raw.columns if not os.path.exists(c_hdf_raw) else []),
    key=hdf_key
    )
print(f"{'':{1*tab}s}Flags")

# Plot Figures
print(f"{'':{1*tab}s}Figures")
vlines = []
flag_data = odata[var_name['flag']]
periods = flag_data.loc[
    flag_data.ne(flag_data.shift(-1)) | flag_data.ne(flag_data.shift(1))
    ]
t_e = periods.loc[periods.ne(periods.shift(-1))].index
t_s = pd.concat([periods.index[[0]].to_series(), t_e[:-1].to_series()]).index
if 'obs' not in fig:
    fig['obs'] = plt.figure(**fig_main_kwargs)
    ax['obs'] = fig['obs'].add_axes(
                ax_main_kwargs['rect'],
                **{k: v for k, v in ax_main_kwargs.items() if k != 'rect'}
                )
else:
    ax['obs'].cla()
Rn_processor.plot.generic(
    fig=fig['obs'], ax=ax['obs'],
    idata={
        **{
            i: [
                'line',
                [
                    idata[var_name['Rn']].loc[t_s[i]:t_e[i]].index,
                    idata[var_name['Rn']].loc[t_s[i]:t_e[i]],
                    '-'],
                {
                    'c': (
                        '#005E7E'
                        if flag_data[v] in pass_flags else
                        '#C05A00'
                        ),
                    'lw': '0.5'
                    }
                ]
            for i, v in enumerate(t_e)
            },
        },
    texts=[
        {
            'x':0.05/5 , 'y':1.67/3 , 's': u'$^{222}$Ra (mBq m$^{-3}$)',
            'fontsize': 8,
            'ha': 'left', 'va': 'center', 'rotation': 90,
            'transform': fig['obs'].transFigure
            }, # y label
        ],
    hlines=[],
    vlines=[
        {'x': pd.to_datetime(v), 'c': '#000000', 'lw': '0.2'}
        for v in vlines
        ],
    xlim=[odata.index[0], odata.index[-1]],
    xdate=[True, False, False],
    ylim=[0., 8.e3],
    tick_fontsize=6,
    )
for label in ax['obs'].get_xticklabels():
    label.set_ha("right")
    label.set_rotation(30)

# Save
for k, v in fig.items():
    v.savefig(fig_file[k], dpi=300)


'''
fig['obs'], ax['obs'] = Rn_processor.util.plot(
    [[odata[var_name['Rn']].loc[plt_idx], '#005E7E', 'line', 0.5]],
    ylabel=u'$^{222}$Ra (mBq m$^{-3}$)',
    xlim=[plt_idx[0], plt_idx[-1]],
    ylim=[0, 8.e3],
    vlines=vlines
    )
'''
"""
for i in range(len(vlines) // 2):
    t = pd.to_datetime(vlines)[i*2:i*2+2].mean()
    ax.annotate(
        i+1,
        xy=(t, 3),
        xytext=(t + (-1)**(i+1)*pd.to_timedelta('1D'), 3),
        ha='center',
        va='center',
        size=10,
        arrowprops={
            'arrowstyle': '->'
            #'width': 0.2,
            #'headwidth': 1,
            #'headlength': 1
            }
        )
"""
if not interactive:
    plt.close('all')
