# -*- coding: utf-8 -*-
r"""
Created on Tue Jan 28 16:14:10 2020

@author: Edward Chung (edward.chung@npl.co.uk)

radon_gui.py

Executable script to process ANSTO Radon measurements.

To use (interactive)
    1) Run and use the GUI when they appear.
    2) Change the settings in the script itself to set default values.

Note:
    For GCCompare, set `settings['temporal']['lag'] = '0H'`, i.e. time is not
    shifted.

"""
import copy
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import sys
import Rn_processor.io
import Rn_processor.routines
import Rn_processor.gui
import Rn_processor.util
import Rn_processor.plot


# =============================================================================
# Default Settings
# =============================================================================

# Settings on data processing
#   'time0' and 'time1' is the start and end time, respectively.
#       If you do not want to specify the limits, set them to ''.
#       'time0': empty means start date = start of data.
#       'time1': empty means end date = end of data.
settings = {
    'input': {
        # Site name - used for output file name
        'site': 'HFD',
        # File/Directory paths
        # input file
        'ifile': (
            "T:/EAM/TECHNICAL/Heathfield/Technical/Data/"
            "Radon/Raw_Data/HeJan20e.CSV"
            ),
        # Year-month string
        't0': pd.to_datetime('2020-01-01'),
        #previous settings
        'prev_sfile': '',
        },
    # Background
    'bg': {
        # Calculation Mode
        #   'calc' - Calculate from the month file
        #   'last' - Read the result of last valid event
        #   'file' - Read from file
        #   'cons' - Use the provided constant
        #   all but 'calc' will ignore all other settings but comment
        'mode': 'calc',
        # Name of the file to read if mode == 'file', else will be ignored
        'ifile': '',
        # Constant to use if mode == 'cons', else will be ignored
        'constant': 0.0,
        # Time window for the event
        #   Required if the event occurs multiple times
        'time0': '2020-01-15 00:00',
        'time1': '2020-01-16 01:30',
        # Timestamps to exclude
        'exclude': '',
        # n measurements to skip at the front: when instrument is reaching down
        #   to the background level
        'dt_decay': '5.5H',
        # n measurements to include after the flagged event
        #   Negative numbers excludes from the rear
        #   If time1 is '', dt_recovery >= 0
        'dt_recovery': '1.5H',
        # Event flag: 0 = valid, 1 = invalid, 2 = no event.
        'event_flag': 0,
        'comments': ''
        },
    # Calibration
    'cal': {
        # Calculation Mode
        #   'calc' - Calculate from the month file
        #   'last' - Read the result of last valid event
        #   'file' - Read from file
        #   'cons' - Preset constant
        #   all but 'calc' will ignore all other settings but comment
        'mode': 'calc',
        # Name of the file to read if mode == 'file', else will be ignored
        'ifile': '',
        # Constant to use if mode == 'cons', else will be ignored
        'constant': 0.0,
        # Time window for the event
        #   Required if the event occurs multiple times
        'time0': '2020-01-12 08:00',
        'time1': '2020-01-12 19:00',
        # Injection duration
        #   Duration = End of injection - time0
        'dt_injection': '5H',
        # n measurements to include after the flagged event
        # Negative numbers excludes from the rear
        #   If time1 is '', dt_recovery >= 0
        'dt_recovery': '0H',
        # Source/Standard properties
        'source_str': 49.311,
        'scaling_factor': 1.007,
        'std_str': 20.0,
        'std_delivery': 2.52,
        # Event flag: 0 = valid, 1 = invalid, 2 = no event.
        'event_flag': 0,
        # Comment
        'comments': ''
        },
    'inspect': {
        # Flag str
        'flag-str': ''
        },
    # Temporal setting of the data to process
    #   Data outside the temporal range, or in the exlude list will not be read
    'temporal': {
        # Lag time for time deconvolution
        'lag': '0H'
        },
    'output': {
        # output directory for processed data
        'odir': (
            "T:/EAM/TECHNICAL/Heathfield/Technical/Data/Radon/Analysed_Data/"
            ),
        # output directory for figures
        'fdir': (
            "T:/EAM/TECHNICAL/Heathfield/Technical/Data/Radon/Analysed_Data/"
            "Figures/"
            ),
        # processed file (with time lag)
        'pfile': '',
        # raw processed file (no time lag)
        'rfile': '',
        # flag-comments file
        'ffile': '',
        # settings file
        'sfile': '',
        # background figure
        'bg_fig': '',
        # calibration figure
        'cal_fig': '',
        # final Rn figure
        'Rn_fig': '',
        }
    }

# =============================================================================
# Constants
# =============================================================================
# NoneType
NoneType = type(None)
# Tab size for prints
tab = 4
# 30 minutes timedelta
td_30m = pd.to_timedelta('30min')

# =============================================================================
# Settings Properties
# =============================================================================
settings_dtypes = {
    # parameter: [
    #     type, {'', 'file', 'time', 'timedelta', 'times'}
    #     ]
    'event_flag': [int, ''],
    'constant': [float, ''],
    'y0': [float, ''],
    'y1': [float, ''],
    'source_str': [float, ''],
    'scaling_factor': [float, ''],
    'std_str': [float, ''],
    'std_delivery': [float, ''],
    'comments': [str, ''],
    'flag-str': [str, ''],
    'mode': [str, ''],
    'pvar': [str, ''],
    'pvar_prev': [str, ''],
    'site': [str, ''],
    'pfile': [str, ''],
    'rfile': [str, ''],
    'ffile': [str, ''],
    'sfile': [str, ''],
    'bg_fig': [str, ''],
    'cal_fig': [str, ''],
    'Rn_fig': [str, ''],
    'ifile': [str, 'path*'],
    'prev_sfile': [str, 'path*'],
    'odir': [str, 'path'],
    'fdir': [str, 'path'],
    't0': [str, 'time'],
    'time0': [str, 'time'],
    'time1': [str, 'time'],
    'x0': [str, 'time'],
    'x1': [str, 'time'],
    'exclude': [str, 'times'],
    'dt_decay': [str, 'timedelta'],
    'dt_injection': [str, 'timedelta'],
    'dt_recovery': [str, 'timedelta'],
    'lag': [str, 'timedelta'],
    }
settings_names = {
    'event_flag': 'Event Flag',
    'constant': 'Constant',
    'source_str': 'Src Strength',
    'scaling_factor': 'Scaling Factor',
    'std_str': 'Std Strength',
    'std_delivery': 'Std Delivery',
    'comments': 'Comments',
    'mode': 'Mode',
    'site': 'Site Code',
    'pfile': 'Data (t-shift)',
    'rfile': 'Data (no t-shift)',
    'ffile': 'Flag/Comments',
    'sfile': 'Settings',
    'ifile': 'Input File',
    'bg_fig': 'Background Fig',
    'cal_fig': 'Calibration Fig',
    'Rn_fig': 'Rn Fig',
    'prev_sfile': 'Prev. Settings File',
    'odir': 'Output Dir',
    'fdir': 'Figure Dir',
    't0': 'Year-Month',
    'time0': 'Start Time',
    'time1': 'End Time',
    'exclude': 'Excluded Times',
    'dt_decay': 'Decay Duration',
    'dt_injection': 'Injection Duration',
    'dt_recovery': 'Recovery Duration',
    'lag': 'Timestamp Shift',
    }
settings_tab_titles = {
    'input': 'Input',
    'bg': 'Background',
    'cal': 'Calibration',
    'inspect': 'Inspect',
    'temporal': 'Temporal',
    'output': 'Output',
    }
settings_tab_order = ['input', 'bg', 'cal', 'inspect', 'temporal', 'output']
# Name of the processed data
var_names = {
    'bg': 'background',
    'cal': 'calibration_factor',
    'Rn': 'radon',
    'Rn_unc': 'radon_uncertainty',
    'Rn_stp': 'radon_stp',
    'Rn_unc_stp': 'radon_uncertainty_stp',
    'flag': 'Flag_a',
    'comments': 'Comments_a',
    }

# Suppress matplotlib interactive mode
plt.ioff()
# Close all pre-existing figures held on memory
plt.close('all')
# Variables used for inspection
var_insp = [
        'ExFlow', 'InFlow', 'ULD', 'LLD',
        'AirT', 'HV', 'Press', 'TankP', 'RelHum', 'Temp', 'Flag'
        ]
# Plot variables - for diagnostics, must include LLD
plot_vars = {
    'bg': ['LLD', *[k for k in var_insp if k != 'LLD']],
    'cal': ['LLD', *[k for k in var_insp if k != 'LLD']],
    'inspect': (
        [var_names['Rn']] + var_insp
        + [var_names['bg'], var_names['cal'], var_names['flag']]
        ),
    'temporal': (
        [var_names['Rn']] + var_insp
        + [var_names['bg'], var_names['cal'], var_names['flag']]
        ),
    }
# Flags
flags = ['0', '1', '2', '3', '4', 'F']
pass_flags = ['0']

# Variables to include in the save file
save_cols = [
        'Year', 'Month', 'Day', 'Time',
        var_names['Rn'], 'ExFlow', 'InFlow', 'ULD',
        'AirT', 'HV', 'Press', 'TankP', 'RelHum', 'Temp',
        't0', var_names['bg'], var_names['cal'],
        var_names['flag'], var_names['comments']
        ]

# Stages/Final plots
fig_stages = [
    'bg', 'cal', 'inspect', 'temporal', 'bg_fig', 'cal_fig', 'Rn_fig'
    ]
# =============================================================================
# Set and launch GUI
# =============================================================================
app = Rn_processor.gui.RnWindow(
    #settings_titles, settings, settings_dtypes, settings_names
    )

## figure/ax kwargs
app.fig_kwargs = {
    stage: {'figsize': [5, 3], 'dpi': 200}
    for stage in fig_stages
    }
app.ax_kwargs = {
    stage: {
        'rect':
        [0.6/5, 0.4/3, 4.35/5, 2.55/3]
        if stage != 'temporal' else
        [0.6/5, 0.4/3, 3.8/5, 2.55/3]
        }
    for stage in fig_stages
    }

## load default settings/types/names
app.tab_titles = settings_tab_titles
app.tab_order = settings_tab_order
app.settings_b = settings
app.dtypes = settings_dtypes
app.names = settings_names
app.var_names = var_names
app.plot_vars = plot_vars
app.flags = flags
app.pass_flags = pass_flags

# load calc/plot/save functions
app.calc_funcs = {  # should return: main result, changed settings, all results
    'bg': lambda: Rn_processor.routines.routine_calc(
        calc_set={
            k: app.get_value('bg', k)
            for k in app.settings_f['bg']
            if k in app.settings_b['bg']
            },
        dtypes=app.dtypes, idata=app.idata, prev_col=('results', 'bg'),
        t0=app.t0, calc_func=Rn_processor.routines.process_bg,
        ),
    'cal': lambda: Rn_processor.routines.routine_calc(
        calc_set={
            k: app.get_value('cal', k)
            for k in app.settings_f['cal']
            if k in app.settings_b['cal']
            },
        dtypes=app.dtypes, idata=app.idata, prev_col=('results', 'cal'),
        t0=app.t0, calc_func=Rn_processor.routines.process_cal,
        ),
    'inspect': lambda: (
        pd.concat(
            Rn_processor.util.flag_str2series(
                app.idata.index,
                app.get_value('inspect', 'flag-str'),
                flag_name=app.var_names['flag'],
                comments_name=app.var_names['comments'],
                ),
            axis=1
            ),
        {},{}
        )
    }
app.calc_funcs_def = {
    'bg': lambda: (0.0, {}, {}),
    'cal': lambda: (0.0, {}, {}),
    'inspect': lambda: (['0', ''], {}, {})
    }
app.plot_funcs = {
    'bg':
        lambda: Rn_processor.routines.plot_bg(
            fig=app.figs['bg'], ax=app.axs['bg'],
            idata=app.idata, col=app.get_value('bg', 'pvar'),
            **app.results_calc['bg']
            ),
    'cal':
        lambda: Rn_processor.routines.plot_cal(
            fig=app.figs['cal'], ax=app.axs['cal'],
            idata=app.idata, col=app.get_value('cal', 'pvar'),
            **app.results_calc['cal']
            ),
    'inspect':
        lambda: Rn_processor.routines.plot_inspect(
            fig=app.figs['inspect'], ax=app.axs['inspect'],
            idata=app.idata, col=app.get_value('inspect', 'pvar'),
            flag_data=app.results['inspect'][app.var_names['flag']],
            flags=app.flags,
            pass_flags=app.pass_flags
        ),
    'temporal':
        lambda: Rn_processor.routines.plot_temporal0(
            fig=app.figs['temporal'], ax=app.axs['temporal'],
            idata=app.idata, col=app.get_value('temporal', 'pvar'),
            lag=app.settings_b['temporal']['lag'],
            flag_data=app.results['inspect'][app.var_names['flag']],
            flags=app.flags,
            pass_flags=app.pass_flags
            ),
    'temporal_ext':
        lambda: Rn_processor.routines.plot_temporal1(
            fig=app.figs['temporal'], ax=app.axs['temporal_ext'],
            idata=app.idata_ext['temporal_ext'],
            col=app.get_value('temporal_ext', 'pvar')
            ),
    'bg_fig':
        lambda: Rn_processor.routines.plot_bg(
            fig=app.figs['bg_fig'], ax=app.axs['bg_fig'],
            idata=app.idata, col='LLD',
            **app.results_calc['bg']
            ),
    'cal_fig':
        lambda: Rn_processor.routines.plot_cal(
            fig=app.figs['cal_fig'], ax=app.axs['cal_fig'],
            idata=app.idata, col='LLD',
            **app.results_calc['cal']
            ),
    'Rn_fig':
        lambda: Rn_processor.routines.plot_final(
            fig=app.figs['Rn_fig'], ax=app.axs['Rn_fig'],
            idata=app.idata, col=app.var_names['Rn'],
            lag=app.settings_b['temporal']['lag'],
            flag_data=app.results['inspect'][app.var_names['flag']],
            flags=app.flags,
            pass_flags=app.pass_flags
            ),
    }
app.plot_funcs_def = {
    'bg':
        lambda: Rn_processor.routines.plot_norm(
            fig=app.figs['bg'], ax=app.axs['bg'],
            idata=app.idata, col=app.get_value('bg', 'pvar')
            ),
    'cal':
        lambda: Rn_processor.routines.plot_norm(
            fig=app.figs['cal'], ax=app.axs['cal'],
            idata=app.idata, col=app.get_value('cal', 'pvar')
            ),
    'inspect':
        lambda: Rn_processor.routines.plot_norm(
            fig=app.figs['inspect'], ax=app.axs['inspect'],
            idata=app.idata, col=app.get_value('inspect', 'pvar')
        ),
    'temporal':
        lambda: Rn_processor.routines.plot_norm(
            fig=app.figs['temporal'], ax=app.axs['temporal'],
            idata=app.idata, col=app.get_value('temporal', 'pvar'),
            ),
    'temporal_ext':
        lambda: Rn_processor.routines.plot_temporal1(
            fig=app.figs['temporal'], ax=app.axs['temporal_ext'],
            idata=app.idata_ext['temporal_ext'],
            col=app.get_value('temporal_ext', 'pvar')
            ),
        }
app.stage_figs = {  # parent figure if axis name is not the same as figure name
    'temporal_ext': 'temporal'
    }
app.save_funcs = {
    'pfile': lambda: Rn_processor.routines.save_data(
        idata=app.idata,
        ofile=os.path.join(
            app.get_value('output', 'odir'),
            f"{app.get_value('output', 'pfile')}.hdf"
            ),
        fmt='hdf', t0=app.t0,
        lag=app.settings_b['temporal']['lag'],
        cols=save_cols
        ),
    'rfile': lambda: Rn_processor.routines.save_data(
        idata=app.idata,
        ofile=os.path.join(
            app.get_value('output', 'odir'),
            f"{app.get_value('output', 'rfile')}.hdf"
            ),
        fmt='hdf', t0=app.t0,
        lag='+0H',
        cols=save_cols
        ),
    'ffile': lambda: Rn_processor.io.w_flag_comments(
        idata=app.get_value('inspect', 'flag-str'),
        t0=app.t0,
        ofile=os.path.join(
            app.get_value('output', 'odir'),
            f"{app.get_value('output', 'ffile')}.csv"
            )
        ),
    'sfile': lambda: Rn_processor.routines.save_settings(
        settings=app.settings_b,
        ofile=os.path.join(
            app.get_value('output', 'odir'),
            f"{app.get_value('output', 'sfile')}.hdf"
            ),
        fmt='hdf', t0=app.t0,
        stages=['bg','cal','temporal'],
        results=app.results, results_calc=app.results_calc
        ),
    'bg_fig': lambda: app.figs['bg_fig'].savefig(
        os.path.join(
            app.get_value('output', 'fdir'),
            f"{app.get_value('output', 'bg_fig')}.png"
            ),
        dpi=300
        ),
    'cal_fig': lambda: app.figs['cal_fig'].savefig(
        os.path.join(
            app.get_value('output', 'fdir'),
            f"{app.get_value('output', 'cal_fig')}.png"
            ),
        dpi=300
        ),
    'Rn_fig': lambda: app.figs['Rn_fig'].savefig(
        os.path.join(
            app.get_value('output', 'fdir'),
            f"{app.get_value('output', 'Rn_fig')}.png"
            ),
        dpi=300
        )
    }

## build
app.init_internal_attr()
app.init_tabs()
app.init_input(stage='input')
app.init_calc(stage='bg')
app.init_calc(stage='cal')
app.init_inspect(stage='inspect')
app.init_temporal(stage='temporal')
app.init_output(stage='output')

app.mainloop()
