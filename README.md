# 222Rn Calibration Package
Package to process ANSTO 222Rn measurements.

## Setup
1. Downloadclone the package.
2. Choose a python virtual environment (optional.)
3. Install dependencies.
```
conda install --file requirements.txt 
```
or
```
pip install -r requirements.txt
```
Note that the script lists "tables" module as a dependency. This is named as "pytables" in conda repository. Edit the requirements.txt before running the command.
4. Run setup.py in a chosen python virtual environment.
```
python setup.py develop
```
or
```
python setup.py install
```
## How to use
Change the "settings" block on radon.py and run, or run radon_gui.py.

### Note
1. For GCCompare, the following has to be observed:
- The agreed name for background and calibration factor is "Background" and "Calibration".
- Use the timestamps as is from the raw data: no lags.

2.	On windows, the shell script can be run from git bash. If using python through conda, run the following from git bash console before running the script if problem occurs:
```
. <path to conda>/etc/profile.d/conda.sh

conda activate <conda environment – typically “base”>
```
