# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 16:46:41 2020

@author: ec5
"""
# Standard Library imports
import os
import sys

# Third party imports
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *



#QtWidgets - QMainWindow, QLabel, QApplication


class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__()
        self.setWindowTitle("222Rn Processor")
        self.setGeometry(100, 100, 1800, 600)
        self.layout = QHBoxLayout()

        # Internal variables
        self.figs = {}
        self.axs = {}
        # Tabs
        self.tabs = QTabWidget()
        self.tab0 = QWidget()  # 0: file settings
        self.tab1 = QWidget()  # 1: t_range
        self.tab2 = QWidget()  # 2: bg
        self.tab3 = QWidget()  # 3: cal
        self.tab4 = QWidget()  # 4: flag
        self.tab5 = QWidget()  # 5: time lag
        self.tab6 = QWidget()  # 6: save or not
        self.tabs.resize(300, 200)
        self.tabs.addTab(self.tab0, "File")
        self.tabs.addTab(self.tab1, "Temporal Range")
        self.tabs.addTab(self.tab2, "Background")
        self.tabs.addTab(self.tab0, "Calibration")
        self.tabs.addTab(self.tab1, "Flag")
        self.tabs.addTab(self.tab2, "Time Lag")
        self.tabs.addTab(self.tab2, "Save")
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)


    def draw_widgets(self):
        #self.setWindowTitle(self.title)
        # Header
        n = 0
        self.label1 = QLabel(self)
        self.label1.setText('Variable')
        self.label1.move(10,50)

        self.label2 = QLabel(self)
        self.label2.setText('Value')
        self.label2.move(100,50)

        self.label1 = QLabel(self)
        self.label1.setText('Error')
        self.label1.move(200,50)


def window():
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    win = MainWindow()#'title', {}, {}, {})
    win.draw_widgets()
    win.show()
    #app.exec_()
    sys.exit(app.exec_())
    #if not os.environ.get('SPY_UMR_ENABLED'):
    #    sys.exit(app.exec_())

if __name__ == '__main__':
    if not QApplication.instance():
        app = QApplication(sys.argv)
    else:
        app = QApplication.instance()
    win = MainWindow()#'title', {}, {}, {})
    win.draw_widgets()
    win.show()
