# -*- coding: utf-8 -*-
r"""
Created on Tue Jan 28 16:14:10 2020

@author: Edward Chung (edward.chung@npl.co.uk)

util.py

Module with utility functions.

"""
# Standard Library imports
import numpy as np
import os
import pandas as pd

# Local imports
from Rn_processor.io import r_coeffs


def convert_str(s, dtype, str_dtype, error='raise'):
    r"""Convert string according to dtype

    Parameters
    ----------
    s : str
        String to convert.
    dtype : type
        Type to convert s to.
    str_dtype : str {'', 'path', 'path*', 'time', 'timedelta', 'times'}
        What s represents if dtype(s) is a string.
        'times' means s is time strings delimited by ';'.
        'path*' means path should already exist.
        'path' still checks whether the base directory exists
    error : str {'raise', 'print'}
        When an error occurs during conversion, either raise an Exception or
        print the Exception.

    Returns
    -------
    o :
        Type depends on dtype. Returns empty if exceptions are raised, but
        error is set to 'raise'.

    """
    try:
        if dtype == type(None):
            o = None
        elif dtype == str:
            o = str(s)
            # Test if the string is valid for purpose
            if str_dtype == '':
                pass
            elif (
                    str_dtype == 'path'
                    and not os.path.exists(os.path.dirname(o))
                    and o
                    ):
                raise FileNotFoundError
            elif str_dtype == 'path*' and not os.path.exists(o) and o:
                raise FileNotFoundError
            elif str_dtype == 'time':
                pd.to_datetime(s)
            elif dtype == 'timedelta':
                pd.to_timedelta(s)
            elif str_dtype == 'times':
                [pd.to_datetime(s1.strip()) for s1 in s.split(';')]
        else:
            o = dtype(s)
    except Exception as err:
        if error == 'raise':
            raise err
        else:
            print(err)
            o = dtype()
    return o


def flag_processor(l):
    r"""Generator to give valid flag string

    Parameter
    ---------
    l : list of list of str
        List of lists containing processed flag strings.
        Each list should contain the following strings in order:
            timestamp, timestamp, flag, str
        Any exceptions will be raised silently (printed on terminal).

    Yields
    ------
    i : Valid list of flag strings

    """
    for i in l:
        try:
            convert_str(i[0], str, 'time', 'raise')
            convert_str(i[1], str, 'time', 'raise')
            #convert_str(i[2], int, '', 'raise')
            if len(i) != 4:
                raise Exception("Row should contain 4 items only.")
            yield i
        except Exception as err:
            print(err)


def flag_merge(flag_name='', comments_name=''):
    pass

def flag_series2str(flag, comments):
    r"""Combine flag and comments to a flag-string.

    Flag string is consisted of start date, end date, flag and comments, all
    delmited by ','.
    Comments are delimited by ';;' and should not contain ','.

    Parameters
    ----------
    flag, comments : Series
        pd.Series objects each respectively containing flags and comments.

    Returns
    -------
    s : str
        Flag string described above.

    """
    # Separate comments using delimeter
    s = comments.str.split(';;',expand=True).stack().str.strip()
    # Add flag number to separated strings
    su = (
        flag.loc[s[s != ''].index.get_level_values(0)].astype(str).values
        + ','
        + s[s != '']
        )
    # Get unique flag-strings
    unique_strings = [i for i in su.unique() if i]
    idx = flag.index
    trues = pd.Series(False, index=idx)
    groups = []
    for i in unique_strings:
        trues.loc[:] = False
        trues.loc[su.index[su == i].get_level_values(0).unique()] = True
        # find when change occur
        #change = trues.ne(trues.shift(-1) & trues.shift(1))
        periods = trues.loc[
            trues.ne(trues.shift(-1)) |
            trues.ne(trues.shift(1))
            ]
        t_s = periods[
            periods.ne(periods.shift(1)) & periods
            ].index.strftime('%Y-%m-%d %H:%M')
        t_e = periods[
            periods.ne(periods.shift(-1)) & periods
            ].index.strftime('%Y-%m-%d %H:%M')
        groups += (t_s + ',' + t_e + ',' + i).to_list()
    s = '\n'.join(sorted(groups))
    return s


def flag_str2series(idx, s, flag_name='', comments_name=''):
    r"""Combine flag and comments to a flag-string.

    Flag string is consisted of start date, end date, flag and comments, all
    delmited by ','.
    Comments are delimited by ';;' and should not contain ','.

    Parameters
    ----------
    idx : Index
        Index for the output.
    s : str
        Flag string described above.
    flag_name, comments_name : str
        Column names for the output.

    Returns
    -------
    flag, comments : Series
        pd.Series objects each respectively containing flags and comments.

    """
    l = list(flag_processor([
        [
            j.strip()
            for j in i.strip().split(',')
            ]
        for i in s.split('\n')
        if i.strip()
        ]))
    flag = pd.Series('0', index=idx, name=flag_name)
    comments0 = pd.Series('', index=idx, name=comments_name)
    for i in l:
        flag.loc[na_date(i[0]):na_date(i[1])] = i[2]
        comments0.loc[na_date(i[0]):na_date(i[1])] += ';;' + i[3]
    comments = comments0.str.strip(';')
    return flag, comments


def str2plot(idata, default_flags=[]):
    r"""

    Parameters
    ----------
    idata : Series
        pd.Series of dtype str/object.
    default_flags : list of str
        Default Flags. Any flags in idata not included in this list will be
        appended to the end of this list.

    Returns
    -------
    ticks : ndarray
        Tick locations in integers.
    labels : ndarray
        Tick labels in strings.
    odata : Series
        Output of dtype int.

    """
    extra = idata.loc[~idata.isin(default_flags)].unique()
    ticks = np.arange(len(default_flags) + extra.size)
    labels = np.concatenate([default_flags, extra])
    odata = idata.replace(labels, ticks, inplace=False)
    return ticks, labels, odata


def istype(key, obj, dtype, str_dtype='', error='raise'):
    r"""Check type and pre-defined subtype

    Parameters
    ----------
    key : str
        Name of the variable.
    obj : object
        Ojbect to check the type.
    dtype : type
        Type the object can be.
    str_dtype : str {'', 'path', 'time', 'timedelta'}, optional
        What s represents if obj is a string.
    error : {'raise', 'print'}, optional
        When an error occurs during conversion, either raise an Exception or
        print the Exception.

    Raises
    -------
    Exception if isinstance(obj, dtype) fails.

    """
    if not isinstance(obj, dtype):
        raise Exception(
            f"Settings Error: Invalid type\n"
            f"    {key} should be one of the following types\n"
            f"    {dtype}"
            )
    if dtype == str:
        convert_str(obj, dtype, str_dtype, error)


def na_date(s):
    r"""Return None if date string is empty

    Parameter
    ---------
    s : str
        date string.

    Returns
    None or s
        Returns None if date == ''.

    """
    if s == '':
        return None
    else:
        return s


def flag_fail2nan(idata, flag_name, pass_flags):
    r"""Replace flagged data with NaN.

    Parameter
    """
    odata = idata.copy()
    odata.loc[odata[flag_name].isin(pass_flags)] = np.nan
    return odata


def find_flagged_timestamps(
        idata, flag=None, dt_recovery='30min', t_res='30min'
        ):
    r"""

    Parameters
    ----------
    idata : DataFrame
        DataFrame that includes column 'Flag' and have time as index.
    flag : None or list of int
        Flags to select. If None, flags would not be filtered.
    df_recovery : str
        Input to pd.to_timedelta.
        Duration of recovery phase at the end of a flagged event.
        Giving negative numbers will extend the flagged period.
        This should not be negative if flag is set to None.
    t_res : str
        Maximum time difference between rows to ensure continous data.

    Returns
    -------
    t_range : pd.DatetimeIndex
        Selected time range.

    """
    iflag = idata['Flag']
    t_idx = idata.index if flag is None else idata.index[iflag.isin(flag)]
    t_s = t_idx[0]
    t_e = t_idx[-1] - pd.to_timedelta(dt_recovery)
    if t_e > idata.index[-1]:
        raise Exception(
            "End of selection later than end of data."
            )
    t_range = iflag.loc[t_s:t_e].index
    # Check continuity
    if (t_range.to_series().diff()[1:] >= pd.to_timedelta(t_res)).any():
        raise Exception(f"Not all data is within {t_res}.")
    return t_range


def find_closest_timestamp(index, reference):
    r"""Find the closest time in the DatetimeIndex to reference Timestamp.

    Parameters
    ----------
    index : DatetimeIndex
        List of timestamps.
    reference : Timestamp
        Reference timestamp.
    """
    closest_time = (index.to_series() - reference).abs().idxmin()
    return closest_time


def calc_background(idata, dt_decay, exclude=[]):
    r"""Calculate background values

    Parameters
    ----------
    idata : DataFrame
        DataFrame that includes columns 'LLD' and'ExFlow'.
        Index should be datetime.
    dt_decay : str
        Input to pd.to_timedelta.
        Duration of decay phase at the start of a flagged event to be excluded.
        This should not be negative if flag is set to None.
    exclude : list, optional
        Timestamp of data points to exclude.

    Returns
    -------
    bg_mean : float-like
        Mean background.
    bg : ndarray
        Background values.

    """
    t_s = idata.index[0] + pd.to_timedelta(dt_decay)
    bg0 = idata['LLD'].loc[t_s:]
    ExFlow = idata['ExFlow'].loc[t_s:]
    bg = bg0.loc[(~bg0.index.isin(exclude)) & (ExFlow <= 0)]
    bg_mean = bg.mean()
    return bg_mean, bg


def calc_calibration_factor(
        idata,
        max_idx=None,
        source_str=49.311, scaling_factor=1.007,
        std_str=20., std_delivery=2.52
        ):
    r"""Calculate calibration factor

    NPCPS is calculated from the highest 'LLD' peak from the selected data.
    Source delivery rate is calculated as
        source strength * standard strength / standard delivery rate

    Parameters
    ----------
    idata : DataFrame
        DataFrame that includes columns 'LLD' and 'ExFlow' and have time as
        index.
    max_idx : None or datetime-like
        Index to consider as the maximum value (NPCPS). If None, maximum value
        will be used.
    source_str : float, optional
        Strength of calibration source.
        kBq Ra-226
    scaling_factor : float, optional
        Reciprocal of amount filled in the tank compared to the simulated
        amount filled to steady state.
    std_str : float, optional
        Strength of standard calibration source.
        Defaults to 20, which is that of the standard passive Ra-226 source
        from Pylon Electronics.
        Unit is kBq Ra-226
    std_delivery : float, optional
        Delivery rate of standard calibration source.
        Defaults to 2.52, which is that of the standard passive Rn-222 source
        from Pylon Electronics.
        Unit is Bq Rn-222 min-1

    Returns
    -------
    cal_factor : float-like
        Calibration factor.
    ambient : ndarray
        Calculated ambient LLD.
    NPCPS_idx : Timestamp
        Timestamp at NPCPS.

    """
    NPCPS_idx = idata['LLD'].idxmax() if max_idx is None else max_idx
    ambient = idata['LLD'].copy()
    ambient.iloc[1:-1] = np.nan
    ambient.interpolate(method='time', inplace=True)
    NPCPS = (idata['LLD'].loc[NPCPS_idx] - ambient.loc[NPCPS_idx])/1800  # Bq

    source_delivery = std_delivery * source_str / std_str  # Bq Rn-222 min-1

    cal_conc = source_delivery / idata['ExFlow'][NPCPS_idx] * 1.e3

    cal_factor = scaling_factor * NPCPS / cal_conc
    return cal_factor, ambient, NPCPS_idx


def get_last_valid(files, t0, variables, flags=[0]):
    r"""Read from list of files and get the last valid data

    Parameters
    ----------
    files : list of path-like
        Files to read in, in descending order of importance.
    t0 : str
        Timestamp before which the last valid data should be found.
    variables : list of str
        Names of variables.
    flags : list of int
        Flags to be considered as valid.

    Returns
    -------
    out : dict of pd.Series
        Last valid data point for each variables.

    """
    out = {}
    for v in variables:
        for f in files:
            v_cfile = r_coeffs(f).loc[:t0, [v, f'{v}_Flag']]
            v_cfile_idx = (
                v_cfile[v_cfile[f'{v}_Flag'].isin(flags)].last_valid_index()
                )
            if v_cfile_idx is not None:
                break
            else:
                continue
        if v_cfile_idx is not None:
            out[v] = v_cfile.loc[v_cfile_idx, v]
        else:
            raise Exception("No valid value found in the list of files")
    return out
