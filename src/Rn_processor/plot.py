# -*- coding: utf-8 -*-
r"""
Created on Tue Jan 28 16:14:10 2020

@author: Edward Chung (edward.chung@npl.co.uk)

plot.py

Module with plotting functions.

"""


def generic(
        fig, ax, idata,
        texts=[],
        hlines=[], vlines=[],
        xlim=[None, None],
        xdate=[True, False, False],
        xticks={},
        xticklabels={},
        ylim=[None, None],
        yticks={},
        yticklabels={},
        tick_fontsize=8
        ):
    r"""Create generic plots

    Parameters
    ----------
    fig, ax : object
        Figure and axis objects.
    idata : dict
        {key: plot_type, [*args], {**kwargs}}
        Plot data. args and kwargs are directly fed into the plot function
        depending on plot_type.
    texts : list of dict
        Each dict is kwargs for ax.text object, i.e. ax.text(**kwargs).
    hlines, vlines : list of kwargs
        Kwargs for horizontal/vertical lines.
    xlim, ylim : list of floats or None
        X/Y-axis limit.
    xdate : list
        Three-element list where:
        [bool(data is time series), formatter, locator]
        If set true, xticks and xticklabels will be ignored.
    xticks, yticks : dict of kwargs
        Kwargs for ax.set_ticks.
    xticklabels, yticklabels : dict of kwargs
        Kwargs for ax.set_(axis)ticklabels().
        Ignored if corresponding ticks are empty
    tick_fontsize : float
        Fontsize of ticks.

    """
    ax.tick_params(
            axis='both', which='major', direction='in',
            labelsize=tick_fontsize,
            left=True, bottom=True, right=False, top=False,
            labelleft=True, labelbottom=True, labelright=False, labeltop=False
            )
    for l in hlines:
        ax.axhline(**l)
    for l in vlines:
        ax.axvline(**l)
    for k, v in idata.items():
        if v[0] == 'line':
            ax.plot(*v[1], **v[2])
        elif v[0] == 'err':
            ax.errorbar(*v[1], **v[2])
        elif v[0] == 'scatter':
            ax.scatter(*v[1], **v[2])
        elif v[0] == 'stack':
            ax.stackplot(*v[1], **v[2])
        elif v[0] == 'fill':
            ax.fill_between(*v[1], **v[2])
        elif v[0] == 'hline':
            ax.hline(*v[1], **v[2])
    if xdate[0]:
        ax.xaxis_date()
        if xdate[1] is not False:
            ax.xaxis.set_major_formatter(xdate[1])
        if xdate[2] is not False:
            ax.xaxis.set_major_locator(xdate[2])
    else:
        if xticks is not False and 'ticks' in xticks:
            ax.set_xticks(**xticks)
            if xticklabels is not False and 'labels' in xticklabels:
                ax.set_xticklabels(**xticklabels)
    if yticks is not False and 'ticks' in yticks:
        ax.set_yticks(**yticks)
        if yticklabels is not False and 'labels' in yticklabels:
            ax.set_yticklabels(**yticklabels)
    for text in texts:
        ax.text(**text)
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
