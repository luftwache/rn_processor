# -*- coding: utf-8 -*-
r"""
Created on Tue Jan 28 16:14:10 2020

@author: Edward Chung (edward.chung@npl.co.uk)

io.py

Input-Output module.

"""
# Standard Library imports
import numpy as np
import os
import pandas as pd


def concat(x, y):
    r"""Combine two DataFrames.

    Parameter
    ---------
    x, y : pd.DataFrame
        DataFrames to combine. Values and datatypes of x is prioritised.

    Returns
    -------
    odata : pd.DataFrame
        Combined data.

    """
    col_order = x.columns.unique().union(
        y.columns.unique(),
        sort=False
        ).sortlevel(level=0, sort_remaining=False)[0]
    y1 = y[(y.index > x.index[0]) | x.index[-1] < y.index]
    odata = pd.concat(
            [
                pd.concat([
                    (
                        pd.Series(
                            99
                            if pd.api.types.is_integer_dtype(x[k]) else
                            np.nan
                            if pd.api.types.is_float_dtype(x[k]) else
                            '',
                            x.index, dtype=x.dtypes
                            )
                        if k not in y1 else
                        y1[k]
                        if y1[k].dtypes == x.dtypes[k] else
                        pd.Series(
                            99
                            if pd.api.types.is_integer_dtype(x[k]) else
                            '',
                            x.index, dtype=x.dtypes
                            )
                        ),
                    x[k]
                    ])
                for k in col_order
                ],
            axis=1
            ).sort_index()
    return odata


def w_Rn_data(idata, columns, ofile, t0, fmt='hdf', key='key', hdf_file=''):
    r"""Write data to file.

    Parameter
    ---------
    idata : pd.DataFrame
        Input data to write to ofile.
    columns : list of str
        Column names in order.
    ofile : path-like
        Output file.
    fmt : str {'hdf', 'csv'}, optional
        Format of the output file.
    key : str, optional
        Defaults to 'key'.
        HDF key if fmt == 'hdf'.
    hdf_file : path-like
        Path of base HDF file if fmt == 'csv'.

    """
    if os.path.exists(ofile if fmt == 'hdf' else hdf_file):
        base = r_Rn_data(
            ofile if fmt == 'hdf' else hdf_file, fmt='hdf', key=key
            )
        odata = pd.concat(
            [base.loc[base['t0'] != t0], idata],
            ).sort_index()[columns]
    else:
        odata = idata[columns]
    if fmt == 'hdf':
        odata.to_hdf(ofile, key=key, mode='w')
    else:
        odata.to_csv(ofile, sep=',', index_label=False)


def w_settings(idata, columns, ofile, t0, fmt='hdf', key='key', hdf_file=''):
    r"""Write settings to history file.

    Parameter
    ---------
    idata : pd.DataFrame
        Input data to write to ofile.
    columns : list of str
        Column names in order. Required when saving new file, otherwise empty
        file will be created.
    ofile : path-like
        Output file.
    t0 : datetime-like
        Timestamp for idata.
    fmt : str {'hdf', 'csv'}, optional
        Format of the output file.
    key : str, optional
        Defaults to 'key'.
        HDF key if fmt == 'hdf'.
    hdf_file : path-like
        Path of base HDF file if fmt == 'csv'.

    """
    if os.path.exists(ofile if fmt == 'hdf' else hdf_file):
        # open file
        base = r_settings(
            ofile if fmt == 'hdf' else hdf_file, fmt='hdf', key=key
            )
        odata = pd.concat(
            [base.loc[base.index != t0], idata],
            ).sort_index()[columns]
    else:
        odata = pd.concat(
            [
                idata[k]
                if k in idata else
                pd.Series(np.nan, name=k, index=idata.index)
                for k in columns
                ],
            axis=1
            )
    if fmt == 'csv':
        odata.to_csv(ofile, sep=',', index_label=False)
    else:
        odata.to_hdf(ofile, key=key, mode='w')


def w_coeffs(idata, ofile, columns=[], fmt='hdf', key='key', hdf_file=''):
    r"""Write coefficients to history file.

    Parameter
    ---------
    idata : pd.DataFrame
        Input data to write to ofile.
    ofile : path-like
        Output file.
    columns : list of str, optional
        Column names in order. Required when saving new file, otherwise empty
        file will be created.
    fmt : str {'hdf', 'csv'}, optional
        Format of the output file.
    key : str, optional
        Defaults to 'key'.
        HDF key if fmt == 'hdf'.
    hdf_file : path-like
        Path of base HDF file if fmt == 'csv'.

    """
    if os.path.exists(ofile if fmt == 'hdf' else hdf_file):
        # open file
        base = r_coeffs(
            ofile if fmt == 'hdf' else hdf_file, fmt='hdf', key=key
            )
        odata = concat(idata, base)
    else:
        odata = pd.concat(
            [
                idata[k]
                if k in idata else
                pd.Series(np.nan, name=k, index=idata.index)
                for k in columns
                ],
            axis=1
            )
    if fmt == 'csv':
        odata.to_csv(ofile, sep=',', index_label=False)
    else:
        odata.to_hdf(ofile, key=key, mode='w')


def w_flag_comments(idata, t0, ofile):
    r"""
    """
    df = pd.DataFrame(
        [[t0]+i.split(',') for i in idata.split('\n')],
        columns=['t0', 'start_time', 'end_time', 'flag', 'comments']
        )
    if os.path.exists(ofile):
        base = r_flag_comments(ofile)
        odata = pd.concat(
            [base.loc[base['t0'] != t0], df],
            ).sort_values('t0')
    else:
        odata = df
    odata.to_csv(ofile, sep=',', index=False)


def r_flag_comments(ifile):
    idata = pd.read_csv(ifile, sep=',', skipinitialspace=True)
    return idata


def r_ANSTO(ifile):
    r"""Read ANSTO measurement file.
    Parameter
    ---------
    ifile : path-like
        Input file.

    Returns
    -------
    odata : pd.DataFrame
        Data read from file.

    """
    idata = pd.read_csv(
        ifile, sep=',', skipinitialspace=True,
        parse_dates={'Datetime': ['Year', 'Month', 'DOM', 'Time']},
        index_col='Datetime',
        keep_date_col=True,
        dtype={
            **{k: np.int16 for k in ['Year', 'DOY', 'Month', 'DOM', 'Flag']},
            **{k: str for k in ['Time', 'Comments']},
            },
        na_values=[-9999]
        )
    idata.rename(columns={'DOM': 'Day'}, inplace=True)
    return idata


def r_GCWERKS(ifile):
    odata = pd.read_csv(
        ifile, sep=',', header=0,
        parse_dates=['time'], index_col='time', keep_date_col=False,
        skipinitialspace=True
        )
    return odata


def r_Rn_data(ifile, fmt='hdf', key=None):
    r"""Read Rn-formated data.

    Parameter
    ---------
    ifile : path-like
        Input file.
    fmt : str {'hdf', 'csv'}, optional
        Format of the input file.
    key : None or str, optional
        HDF key if fmt == 'hdf'.

    Returns
    -------
    odata : pd.DataFrame
        Data read from file.

    """
    if fmt == 'csv':
        odata = pd.read_csv(
            ifile, sep=',', header=0,
            parse_dates={'Datetime': ['Year', 'Month', 'Day', 'Time']},
            index_col='Datetime',
            keep_date_col=True,
            skipinitialspace=True,
            )
    else:
        odata = pd.read_hdf(ifile, key=key, mode='r')
    return odata


def r_settings(ifile, fmt='hdf', key=None):
    r"""Read the settings file.

    Parameter
    ---------
    ifile : path-like
        Input file.
    fmt : str {'hdf', 'csv'}, optional
        Format of the input file.
    key : None or str, optional
        HDF key if fmt == 'hdf'.

    Returns
    -------
    odata : pd.DataFrame
        Data read from file.

    """
    if fmt == 'csv':
        odata = pd.read_csv(
            ifile, sep=',', skipinitialspace=True,
            header=[0, 1], parse_dates=True, index_col=0
            )
    else:
        odata = pd.read_hdf(ifile, key=key, mode='r')
    return odata


def r_coeffs(ifile, fmt='hdf', key=None):
    r"""Read the coefficients file.

    Parameter
    ---------
    ifile : path-like
        Input file.
    fmt : str {'hdf', 'csv'}, optional
        Format of the input file.
    key : None or str, optional
        HDF key if fmt == 'hdf'.

    Returns
    -------
    odata : pd.DataFrame
        Data read from file.

    """
    if fmt == 'csv':
        odata = pd.read_csv(
            ifile, sep=',', skipinitialspace=True,
            header=0, parse_dates=True, index_col=0,
            )
    else:
        odata = pd.read_hdf(ifile, key=key, mode='r')
    return odata
