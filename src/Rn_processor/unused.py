# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 16:14:10 2020

Back-up for functions no longer in use

@author: ec5
"""
def flag_point(event, ax):
    r"""
    """
    x_pos = event.xdata
    x_pos_idx = np.abs(ax.get_xdata)
    plt.draw()


def isint(s):
    r"""Check if string represents string

    Parameter
    ---------
    s : str
        String to test.

    Returns
    -------
    Boolean
        True if int.

    """
    try:
        int(s)
        return True
    except ValueError:
        return False


def isnumeric(s):
    r"""Check if string represents int or float

    Parameter
    ---------
    s : str
        String to test.

    Returns
    -------
    int, float, Boolean
        True if int.

    """
    if s.isdigit():
        return int
    try:
        float(s)
        return float
    except ValueError:
        return False


def convert_str_from_input(s, dtype, str_dtype, error='raise'):
    r"""Wrapper for convert_str in order to process lists

    Parameters
    ----------
    s : str
        String to convert.
    dtype : type
        Type to convert s to.
    str_dtype : str {'', 'path', 'time', 'timedelta', 'times'}
        What s represents if dtype(s) is a string.
    error : str {'raise', 'print'}
        When an error occurs during conversion, either raise an Exception or
        print the Exception.

    Returns
    -------
    o :
        Type depends on dtype.

    """
    if dtype is list:
        o = [
            convert_str(i.strip(), str, str_dtype, error)
            for i in s.split(',') if i
            ]
    else:
        o = convert_str(s, dtype, str_dtype, error)
    return o


def plot(
        idata, xlim=[None, None], ylim=[None, None], xlabel='', ylabel='',
        hlines=[], vlines=[],
        figsize=(5, 3), fontsize=8, rect=[0.1, 0.15, 0.89, 0.84],
        fig=False, ax=False
        ):
    r"""Generic plot function

    Parameters
    ----------
    idata : list of lists
        List of which the items are:
            [pd.Series, colour, plot_type, size]
        Size is for the specific plot types, e.g. marker size for scatter.
        plots, and linewidth for line plots.
    xlim, ylim : list, optional
        Axes limits.
    xlabel, ylabel : str, optional
        Labels for x- and y- axis.
    hlines, vlines : list, optional
        Positions of horizontal and vertical lines.
    figsize : tuple, optional
        Size of the figure.
    fontsize : int, optional
        Font size of axis and tick labels.
    rect : None or 4-tuple, optional
        None: pyplot default method of subplot(111).
        4-tuple of floats: [left, bottom, width, height]
    fig : False or plt.figure
        Figure object to overwrite.
    ax : False or plt.axis
        Axis object to overwrite.

    Returns
    -------
    fig : plt.figure
        Figure object.
    ax : matpotlib.axes.Axes
        Axes object in figure.

    """
    plot_types = [
        i[2] for i in idata if i[2] not in ['line', 'scatter']
        ]
    if plot_types:
        raise Exception(f'Unsupported plot type(s) detected {plot_types}')
    pd.plotting.register_matplotlib_converters()
    if not fig:
        fig = plt.figure(figsize=figsize, dpi=300)
    else:
        fig.clear()
    if not ax:
        ax = fig.add_axes(rect)
    else:
        ax.clear()
    ax.set_xlabel(xlabel, fontsize=fontsize)
    ax.set_ylabel(ylabel, fontsize=fontsize)
    ax.tick_params(
        axis='both', which='major', direction='in',
        labelsize=6,
        labelbottom=True, labeltop=False,
        labelleft=True, labelright=False
        )

    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(30)

    for l in hlines:
        ax.axhline(pd.to_datetime(l), c='#000000', lw='0.2')
    for l in vlines:
        ax.axvline(pd.to_datetime(l), c='#000000', lw='0.2')

    for i in idata:
        if i[2] == 'line':
            ax.plot(i[0].index.to_pydatetime(), i[0], c=i[1], lw=i[3])
        elif i[2] == 'scatter':
            ax.scatter(
                i[0].index.to_pydatetime(), i[0], c=i[1], s=i[3],
                edgecolors=None
                )
    ax.set_xlim(*xlim)
    ax.set_ylim(*ylim)
    ax.xaxis_date()
    ax.yaxis.set_label_coords(-0.08, 0.5)
    ax.set_position(rect)
    return fig, ax


class Sw_stringvar(tkinter.StringVar):
    def __init__(self, master, n, k):
        super(Sw_stringvar, self).__init__(master)
        self.k = k
        self.n = n


class Sw(tkinter.Tk):
    def __init__(self, title, dtypes, values, names):
        r"""

        Parameters
        ----------
        title : str
            Name of the window.
        dtypes : dict
            {key: [(types), {'', 'path', 'time', 'timedelta'}]}
            Data types and subtypes of the variables.
        values : dict
            {key: value}
            Values of the variables.
        names : dict
            {key: str}
            Names of the variables.

        """
        super(Sw, self).__init__()
        self.title(title)
        self.dtype = {k: v[0] for k, v in dtypes.items()}
        self.dtype_sub = {k: v[1] for k, v in dtypes.items()}
        self.values = values
        self.values_calc = copy.deepcopy(values)
        self.names = names
        self.geometry("800x500")
        self.entry = {
            'labels': {},
            'dtype': {}, 'dtype_v': {},
            'value': {}, 'value_v': {},
            'message': {}
            }
        self.continue_flag = True
        self.dtype_str = {
            str(i): i for i in [str, float, int, type(None), list]
            }
        self.row_no = {}
        self.draw_widgets()
        # Plot related
        self.calc_func = False
        self.plot_func = False
        self.idata = None
        self.plot_vars = []
        self.results = {}
        self.canvas = False
        self.fig = False
        self.ax = False
        self.fig_kwargs = {'figsize': [5, 3], 'dpi': 200}
        self.ax_kwargs = {'rect': [0.8/5, 0.6/3, 4.15/5, 2.35/3]}
        self.fig_main = False
        self.ax_main = False
        self.fig_main_kwargs = {'figsize': [5, 3], 'dpi': 300}
        self.ax_main_kwargs = {'rect': [0.6/5, 0.4/3, 4.35/5, 2.55/3]}

    def draw_widgets(self):
        r"""Draw widgets

        """
        # Header
        n = 0
        tkinter.Label(
            self, text='Variable', width=15, justify='left'
            ).grid(row=n, column=0, sticky='ew')
        tkinter.Label(
            self, text='Data Type', width=10, justify='left'
            ).grid(row=n, column=1, sticky='ew')
        tkinter.Label(
            self, text='Value', width=20, justify='left'
            ).grid(row=n, column=2, sticky='ew')
        tkinter.Label(
            self, text='Error', width=20, justify='left'
            ).grid(row=n, column=3, sticky='ew')
        # Widgets
        for n, (k, v) in enumerate(self.values.items()):
            n += 1
            self.row_no[k] = n
            # Label
            self.entry['labels'][k] = tkinter.Label(
                self, text=self.names[k], width=15, justify='left'
                )
            self.entry['labels'][k].grid(row=n, column=0, sticky='ew')
            # Datatype - Option Menu
            self.entry['dtype'][k] = tkinter.StringVar(self)
            self.entry['dtype'][k].set(str(type(self.values[k])))
            if k == 'mode':
                self.entry['dtype'][k].trace(
                    'w',
                    self.update_mode_dtype
                    )
            else:
                self.entry['dtype'][k].trace(
                    'w',
                    lambda *_, k=k: self.update_dtype(k)
                    )
            self.entry['dtype_v'][k] = tkinter.OptionMenu(
                self, self.entry['dtype'][k],
                *[str(i) for i in self.dtype[k]]
                )
            self.entry['dtype_v'][k]['width'] = 10
            self.entry['dtype_v'][k].grid(row=n, column=1, sticky='ew')
            if len(self.dtype[k]) == 1:
                self.entry['dtype_v'][k].config(state='disabled')
            # Value
            self.entry['value'][k] = tkinter.StringVar()
            if k == 'mode':
                self.mode1 = tkinter.OptionMenu(
                    self, self.entry['value'][k],
                    'calc', 'last', 'file', 'cons'
                    )
                self.mode1['width'] = 15
                self.mode2 = tkinter.Entry(
                    self, textvariable=self.entry['value'][k], width=20
                    )
                self.mode1.grid(row=n, column=2, sticky='ew')
                self.mode2.grid(row=n, column=2, sticky='ew')
                self.entry['value'][k].trace(
                    'w',
                    self.update_mode_value
                    )
            else:
                self.entry['value_v'][k] = tkinter.Entry(
                    self, textvariable=self.entry['value'][k], width=20
                    )
                self.entry['value_v'][k].grid(row=n, column=2, sticky='ew')
                if str(type(self.values[k])) == str(type(None)):
                    self.entry['value_v'][k].delete('0', 'end')
                    self.entry['value_v'][k].config(state='disabled')
                elif str(type(self.values[k])) == str(list):
                    self.entry['value_v'][k].insert(
                        0, ', '.join(self.values[k])
                        )
                else:
                    self.entry['value_v'][k].insert(0, f"{self.values[k]}")
                self.entry['value'][k].trace(
                    'w',
                    lambda *_, k=k: self.mark_change(k)
                    )
            # Messages
            self.entry['message'][k] = tkinter.Label(
                self, width=20, borderwidth=2, relief='sunken'
                )
            self.entry['message'][k].grid(row=n, column=3, sticky='ew')
            self.entry['message'][k].grid_propagate(False)
            """
            self.entry['value'][k] = tkinter.ttk.Combobox(
                self,
                width=15,
                #self.entry['dtype'][k],
                values=list(defaults[k])
                ).grid(row=n, column=2)
            """
        # True if modified
        self._modified = {k: False for k in self.values}
        # Buttons
        row_bot = n+1
        self.bot_frames = {
            0: tkinter.Frame(self, width=100, height=50),
            1: tkinter.Frame(self, width=150, height=50),
            2: tkinter.Frame(self, width=150, height=50),
            3: tkinter.Frame(self, width=100, height=50)
            }
        self.bot_frames[0].grid(row=row_bot, column=0, sticky=tkinter.W)
        self.bot_frames[1].grid(row=row_bot, column=1, sticky=tkinter.W)
        self.bot_frames[2].grid(row=row_bot, column=2, sticky=tkinter.W)
        self.bot_frames[3].grid(row=row_bot, column=3, sticky=tkinter.E)
        # Button for save/plot
        self.button_plot = tkinter.Button(
            self.bot_frames[0],
            text='Save',
            command=self.save
            )
        self.button_plot.place(x=0, y=0, relwidth=1, relheight=1)
        # Checkbox for invalid
        if 'mode' in self.values:
            self.var_invalid = tkinter.IntVar()
            self.check_invalid = tkinter.Checkbutton(
                self.bot_frames[1],
                text='Invalid',
                variable=self.var_invalid, onvalue=1, offvalue=0
                )
            self.check_invalid.place(x=0, y=0, relwidth=1, relheight=1)
            self.var_invalid.trace('w', self.update_checks)
        # Checkbox for no event
        if 'mode' in self.values:
            self.var_noevent = tkinter.IntVar()
            self.check_noevent = tkinter.Checkbutton(
                self.bot_frames[2],
                text='No event',
                variable=self.var_noevent, onvalue=1, offvalue=0
                )
            self.check_noevent.place(x=0, y=0, relwidth=1, relheight=1)
            self.var_noevent.trace('w', self.update_checks)
        # Button for close
        self.button_exit = tkinter.Button(
            self.bot_frames[3],
            text='Done',
            command=self.close
            )
        self.button_exit.place(x=0, y=0, relwidth=1, relheight=1)
        # Enable/Disable widgets
        if 'mode' in self.values and 'file' in self.values:
            self.update_mode_dtype()
        self.buttons_state()

    def get_final_value(self, k):
        r"""Convert string to desired value

        Parameter
        ---------
        k : str
            Name of the variable.

        """
        new_dtype = self.dtype_str[self.entry['dtype'][k].get()]
        new_entry = self.entry['value'][k].get()
        try:
            new_val = util.convert_str_from_input(
                new_entry, new_dtype, self.dtype_sub[k], error='raise'
                )
            self.entry['message'][k].config(text="")
        except FileNotFoundError:
            self.entry['message'][k].config(text="File not found")
            new_val = self.values[k]
        except Exception:
            self.entry['message'][k].config(text="Wrong value for dtype")
            new_val = self.values[k]
        return new_val

    def update_dtype(self, k):
        r"""Update dtype to selection

        Parameter
        ---------
        k : str
            Name of the variable.

        """
        self.entry['value_v'][k].delete('0', 'end')
        if self.entry['dtype'][k].get() == str(type(None)):
            self.entry['value_v'][k].config(state='disabled')
        else:
            self.entry['value_v'][k].config(state='normal')
        self.mark_change(k)

    def update_mode_dtype(self, *args):
        r"""Update dtype for mode

        If dtype is string, option menu will appear. If set as float, entry
        will appear.

        """
        if self.entry['dtype']['mode'].get() == str(float):
            self.mode1.grid_remove()  # list
            self.mode2.grid()  # entry
            self.entry['value']['mode'].set('0.0')
        else:
            self.mode1.grid()
            self.mode2.grid_remove()
            self.entry['value']['mode'].set('calc')
        self.update_mode_value()

    def update_mode_value(self, *args):
        r"""Update value for mode

        """
        self.mark_change('mode')
        if 'file' in self.values:
            if self.get_final_value('mode') == 'file':
                self.entry['value_v']['file'].grid()
            else:
                self.entry['value_v']['file'].grid_remove()
            self.mark_change('file')
        if 'constant' in self.values:
            if self.get_final_value('mode') == 'cons':
                self.entry['value_v']['constant'].grid()
            else:
                self.entry['value_v']['constant'].grid_remove()
            self.mark_change('constant')

    def mark_change(self, k):
        r"""Flag modified variable

        Only the modified variables will be updated upon save.

        Parameter
        ---------
        k : str
            Name of the variable.

        """
        old_val = self.values[k]
        new_val = self.get_final_value(k)
        if old_val == new_val:
            self._modified[k] = False
        else:
            self._modified[k] = True
        self.buttons_state()

    def update_checks(self, *args):
        r"""Update checkboxes

        If "invalid" is checked, "No Event" will get disabled, and vice versa.

        """
        if self.var_invalid.get():
            self.check_noevent.config(state='disabled')
        else:
            self.check_noevent.config(state='normal')
        if self.var_noevent.get():
            self.check_invalid.config(state='disabled')
        else:
            self.check_invalid.config(state='normal')

    def buttons_state(self):
        r"""Update buttons

        "Done" button will get disabled if there is a change in settings. If
        there is no change, "Save" will get disabled.

        """
        if any(self._modified.values()):
            self.button_plot.config(state='normal')
            self.button_exit.config(state='disabled')
        else:
            self.button_plot.config(state='disabled')
            self.button_exit.config(state='normal')

    def save(self):
        r"""Save changes

        """
        for k, v in self._modified.items():
            if v:
                self.values[k] = self.get_final_value(k)
            self._modified[k] = False
        if (
                self.plot_func and self.idata is not None and
                'mode' in self.values and self.values['mode'] == 'calc' and
                not self.var_noevent.get()
                ):
            self.values_calc.update(self.values)
            self.calc_and_plot()
        self.buttons_state()

    def update_plot(self, *args):
        r"""Update plot

        """
        # Axis limits
        self.xlim = [None, None]
        self.ylim = {k: [None, None] for k in self.plot_vars}
        # Clear and replot axes
        self.ax_main.cla()
        self.plot_func(
            self.fig_main, self.ax_main, self.idata, self.plot_vars[0],
            **self.results
            )
        self.ax.cla()
        self.plot_func(
            self.fig, self.ax, self.idata, self.plot_var.get(),
            **self.results
            )
        self.xlim = [
            *pd.to_datetime(
                mdate.num2date(self.ax.get_xlim()),
                format='%Y-%m-%d %H:%M'
                )
            ]
        self.ylim[self.plot_var.get()] = list(self.ax.get_ylim())
        # update lim
        self.update_entry_lim()
        self.canvas.figure.dpi = 200
        self.canvas.draw()
        self.toolbar.update()

    def update_plot_var(self, *args):
        r"""

        """
        # Remember limits
        self.xlim = [
            *pd.to_datetime(
                mdate.num2date(self.ax.get_xlim()),
                format='%Y-%m-%d %H:%M'
                )
            ]
        self.ylim[self.plot_var_prev] = list(self.ax.get_ylim())
        self.ax.cla()
        self.plot_func(
            self.fig, self.ax, self.idata, self.plot_var.get(),
            **self.results
            )
        self.ax.set_xlim(*self.xlim)
        if None in self.ylim[self.plot_var.get()]:
            self.ylim[self.plot_var.get()] = list(self.ax.get_ylim())
        else:
            self.ax.set_ylim(*self.ylim[self.plot_var.get()])
        self.plot_var_prev = self.plot_var.get()
        self.update_entry_lim()
        self.canvas.figure.dpi = 200
        self.canvas.draw()
        self.toolbar.update()

    def update_lim(self, *args):
        r"""

        """
        try:
            self.xlim[0] = pd.to_datetime(self.plot_x0.get())
            self.xlim[1] = pd.to_datetime(self.plot_x1.get())
            self.ylim[self.plot_var.get()][0] = self.plot_y0.get()
            self.ylim[self.plot_var.get()][1] = self.plot_y1.get()
            self.ax.set_xlim(*self.xlim)
            self.ax.set_ylim(*self.ylim[self.plot_var.get()])
            self.canvas.draw()
            #self.toolbar.update()
        except:
            pass

    def update_entry_lim(self, *args):
        r"""

        """
        self.entry_x0.delete(0, tkinter.END)
        self.entry_x0.insert(0, self.xlim[0].strftime('%Y-%m-%d %H:%M'))
        self.entry_x1.delete(0, tkinter.END)
        self.entry_x1.insert(0, self.xlim[1].strftime('%Y-%m-%d %H:%M'))
        self.plot_y0.set(self.ylim[self.plot_var.get()][0])
        self.plot_y1.set(self.ylim[self.plot_var.get()][1])

    def draw_plot_area(self):
        r"""

        """
        # Fig, Ax
        self.fig_main = plt.figure(**self.fig_main_kwargs)
        self.ax_main = self.fig_main.add_axes(
                self.ax_main_kwargs['rect'],
                **{k: v for k, v in self.ax_main_kwargs.items() if k != 'rect'}
                )
        self.fig = plt.figure(**self.fig_kwargs)
        self.ax = self.fig.add_axes(**self.ax_kwargs)
        # Increase window size
        self.geometry('1800x700')
        # Variable selector
        self.plot_var = tkinter.StringVar(self)
        self.plot_var.set(self.plot_vars[0])
        self.plot_var.trace('w', self.update_plot_var)
        self.option_plot = tkinter.OptionMenu(
            self, self.plot_var, *self.plot_vars
            )
        self.option_plot['width'] = 15
        self.option_plot.grid(row=0, column=4, sticky='ew')
        self.plot_var_prev = self.plot_vars[0]
        # x0
        tkinter.Label(
            self, text='x0:', width=5, justify='left'
            ).grid(row=1, column=7, sticky='ew')
        self.plot_x0 = tkinter.StringVar(self)
        self.entry_x0 = tkinter.Entry(
            textvariable=self.plot_x0, width=15
            )
        self.entry_x0.grid(row=1, column=8, sticky='ew')
        # x1
        tkinter.Label(
            self, text='x1:', width=5, justify='left'
            ).grid(row=1, column=9, sticky='ew')
        self.plot_x1 = tkinter.StringVar(self)
        self.entry_x1 = tkinter.Entry(
            textvariable=self.plot_x1, width=15
            )
        self.entry_x1.grid(row=1, column=10, sticky='ew')
        # y0
        tkinter.Label(
            self, text='y0:', width=5, justify='left'
            ).grid(row=1, column=11, sticky='ew')
        self.plot_y0 = tkinter.DoubleVar(self)
        self.entry_y0 = tkinter.Entry(
            textvariable=self.plot_y0, width=10
            )
        self.entry_y0.grid(row=1, column=12, sticky='ew')
        # y1
        tkinter.Label(
            self, text='y1:', width=5, justify='left'
            ).grid(row=1, column=13, sticky='ew')
        self.plot_y1 = tkinter.DoubleVar(self)
        self.entry_y1 = tkinter.Entry(
            textvariable=self.plot_y1, width=10
            )
        self.entry_y1.grid(row=1, column=14, sticky='ew')
        # Plot limit button
        #self.button_lim_frame = tkinter.Frame(self, width=100, height=25)
        #self.button_lim_frame.grid(row=1, column=15, sticky=tkinter.W)
        self.button_lim = tkinter.Button(
            self,#.button_lim_frame,
            text='Set',
            command=self.update_lim,
            width=10
            )
        self.button_lim.place(x=0, y=0, relwidth=1, relheight=1)
        # Plot area
        self.canvas_frame = tkinter.Frame(self, width=1000, height=600)
        self.canvas_frame.place(x=800, y=100)
        self.canvas = FigureCanvasTkAgg(
            self.fig, master=self.canvas_frame
            )
        # Toolbar
        self.toolbar_frame = tkinter.Frame(self, width=600, height=100)
        self.toolbar_frame.place(x=1200, y=0)
        self.toolbar = NavigationToolbar2Tk(
            self.canvas, self.toolbar_frame
            )
        self.toolbar.update()
        self.canvas.get_tk_widget().place(
            x=0, y=0, relwidth=1, relheight=1
            )

    def calc_and_plot(self):
        r"""Calculate and plot using self.plot_func

        """
        # Initialise plot window if it does not exist
        if not self.canvas:
            self.draw_plot_area()
        # Calculate
        try:
            self.results.update(self.calc_func(self.values, self.idata))
        except Exception as err:
            print(err)
            #raise Exception(err)
        else:
            # Save calculation settings
            if self.entry['value']['mode'] == 'calc':
                self.values_calc.update(self.values)
            self.update_plot()

    def close(self):
        r"""Close GUI

        """
        self.destroy()


class plot_manager():
    def __init__(self, fig, ax):
        r"""Class to manage interactive plots

        Parameters
        ----------
        fig : Figure
            Figure object to draw on.
        ax : Axes
            Axes object on figure.

        """
        self.fig = fig
        self.ax = ax
        self.canvas = ax.figure.canvas
        self.canvas.mpl_connect('close_event', self.close)
        self.canvas.mpl_connect('key_press_event', self.key_press)
        self.canvas.mpl_connect('button_press_event', self.button_press)

    def draw(self):
        r"""Draw fig on canvas

        """
        print('Click on or close the image to proceed')
        self.canvas.draw()
        self.canvas.start_event_loop()

    def close(self, event):
        r"""Response to close event.

        """
        self.canvas.stop_event_loop()

    def key_press(self, event):
        r"""Response to key press event

        """
        self.canvas.stop_event_loop()

    def button_press(self, event):
        r"""Response to mouse click event

        """
        self.canvas.stop_event_loop()
