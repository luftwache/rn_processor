# -*- coding: utf-8 -*-
r"""
Created on Tue May  5 00:44:55 2020

@author: Edward Chung (edward.chung@npl.co.uk)

routines.py

Module for routines to calculate background/calibration factor and plot.

"""
# Standard Library imports
import numpy as np
import os
import pandas as pd

# Local imports
from Rn_processor import util, plot
from Rn_processor.io import r_settings, w_settings, w_Rn_data


def process_bg(calc_set, idata):
    r"""Routine process for background calculation

    Parameters
    ----------
    calc_set : dict
        Settings.
    idata : pd.DataFrame
        Input data.

    Returns
    -------
    Output : dict
        Dict containing results.

    """
    time0 = util.na_date(calc_set['time0'])
    time1 = util.na_date(calc_set['time1'])
    dt_recovery = pd.to_timedelta(calc_set['dt_recovery'])
    date = util.find_flagged_timestamps(
        idata[['Flag']].loc[time0:time1],
        flag=None,
        dt_recovery=dt_recovery,
        t_res='35min'
        )
    date_plot = util.find_flagged_timestamps(
        idata[['Flag']].loc[time0:time1],
        flag=None,
        dt_recovery=min([dt_recovery, pd.to_timedelta('0H')]),
        t_res='35min'
        )
    if idata.reindex(date).size == 0:
        raise Exception(
            f"No data between {time0} and {time1}."
            f" Should be between {idata.index[0]} and {idata.index[-1]}"
            )
    date_exc = date_plot[1:-1]  # First and last are observations
    # Calculate
    bg_mean, bg = util.calc_background(
        idata.reindex(date), calc_set['dt_decay'],
        [t.strip() for t in calc_set['exclude'].split(';')]
        )
    if bg.reindex(date_plot).size == 0:
        raise Exception(
            f"No background data between {time0} and {time1}."
            )
    bg_std = np.std(bg)
    bg_slope, bg_intercept = np.polyfit(
        (bg.index-bg.index[0]).seconds/1800, bg, 1
        )
    main = bg_mean
    result = {
        'date': date, 'date_plot': date_plot, 'date_exc': date_exc,
        'bg_mean': bg_mean, 'bg': bg,
        'bg_std': bg_std, 'bg_slope': bg_slope, 'bg_intercept': bg_intercept
        }
    return main, result


def process_cal(calc_set, idata):
    r"""Routine process for calibration factor calculation

    Parameters
    ----------
    calc_set : dict
        Settings.
    idata : pd.DataFrame
        Input data.

    Returns
    -------
    Output : dict
        Dict containing results.

    """
    time0 = util.na_date(calc_set['time0'])
    time1 = util.na_date(calc_set['time1'])
    dt_recovery = pd.to_timedelta(calc_set['dt_recovery'])
    date = util.find_flagged_timestamps(
        idata[['Flag']].loc[time0:time1],
        flag=None,
        dt_recovery=dt_recovery,
        t_res='35min'
        )
    date_plot = util.find_flagged_timestamps(
        idata[['Flag']].loc[time0:time1],
        flag=None,
        dt_recovery=min([dt_recovery, pd.to_timedelta('0H')]),
        t_res='35min'
        )
    if idata.reindex(date).size == 0:
        raise Exception(
            f"No data between {time0} and {time1}."
            f" Should be between {idata.index[0]} and {idata.index[-1]}"
            )
    date_exc = date_plot[1:-1]  # First and last are observations
    # Calculate
    dt_injection = pd.to_timedelta(calc_set['dt_injection'])
    cal_factor, cal_ambient, NPCPS_idx = (
        util.calc_calibration_factor(
            idata.reindex(date),
            max_idx=(
                date.to_series()
                - pd.to_datetime(time0)
                - pd.to_timedelta(dt_injection)
                ).abs().idxmin(),
            source_str=calc_set['source_str'],
            scaling_factor=calc_set['scaling_factor'],
            std_str=calc_set['std_str'],
            std_delivery=calc_set['std_delivery']
            )
        )
    NPCPS_amb = cal_ambient[NPCPS_idx]
    main = cal_factor
    result = {
        'date': date, 'date_plot': date_plot, 'date_exc': date_exc,
        'cal_factor': cal_factor, 'cal_ambient': cal_ambient,
        'dt_injection': dt_injection,
        'NPCPS_idx': NPCPS_idx, 'NPCPS_amb': NPCPS_amb
        }
    return main, result


def routine_calc(calc_set, dtypes, idata, prev_col, t0, calc_func):
    if calc_set['event_flag'] == 2:  # no event
        if calc_set['mode'] == 'calc':
            calc_set['mode'] = 'cons'
        for key in calc_set:
            if key in ['mode', 'event_flag', 'comments']:
                continue
            if calc_set['mode'] == 'cons' and key == 'constant':
                continue
            elif calc_set['mode'] in ['file', 'last'] and key == 'ifile':
                continue
            calc_set[key] = (
                0.0 if dtypes[key][0] == float else
                0 if dtypes[key][0] == int else
                ''
                )
        calc = False
    elif calc_set['event_flag'] == 1:  # invalid
        if calc_set['mode'] == 'calc':
            calc_set['mode'] = 'cons'
            calc_set['constant'] = 0.0
        calc = True
    else:
        calc = True
    # Calculate and get plottable results.
    if calc:
        main, result_calc = calc_func(calc_set, idata)
    else:
        main, result_calc = 0., {}
    # Get main value if mode is not 'calc'.
    if calc_set['mode'] in ['file', 'last']:
        ifile = calc_set['ifile']
        prev = r_settings(
            ifile=ifile,
            fmt='csv' if ifile.endswith('csv') else 'hdf',
            key='Rn'
            )
        if calc_set['mode'] == 'file':
            if t0 not in prev.index:
                raise Exception(
                    "No data entry before the current month"
                    " is in the input file."
                    )
            main = prev[prev_col].loc[t0]
        elif calc_set['mode'] == 'last':
            if len(prev.loc[prev.index < t0]) == 0:
                raise Exception(
                    "No data entry before the current month"
                    " is in the input file."
                    )
            main = prev[prev_col].loc[prev.index < t0][-1]
    elif calc_set['mode'] == 'cons':
        main = calc_set['constant']
    return main, calc_set, result_calc


def plot_bg(
        fig, ax, idata, col,
        date, date_plot, date_exc,
        bg_mean, bg, bg_std, bg_slope, bg_intercept,
        **kwargs
        ):
    r"""Plot function for background event.

    Parameters
    ----------
    fig : Figure
        Figure object to draw on.
    ax : Axes
        Axes object to use.
    idata : pd.DataFrame
        Input data.
    col : str
        Column name.
    kwargs : kwargs
        Kwargs from process_bg result.

    """
    td_30m = pd.to_timedelta('30min')
    idx_bg_norm = bg.index
    idx_bg_diff = date_exc.difference(idx_bg_norm)
    idx_bg_obs = idata.index.difference(date_exc)
    #   time true background starts after dt_decay
    bg_t_decay_end = bg.index[0]-td_30m
    #   time when instrument starts to recover
    bg_t_recover = date[-1]
    # Plot
    ylabel = u'LLD (counts 30 min$^{-1}$)' if col == 'LLD' else col
    ymin = idata[col].reindex(date_plot).min()
    ymax = idata[col].reindex(date_plot).max()
    yrange = ymax - ymin
    ylim = (
        [0., ymax+np.abs(ymax)*0.05]
        if col == 'LLD' else
        [
            ymin - yrange*0.05,
            ymax + yrange*0.05
            ]
        if yrange > 0 else
        [
            ymin - np.abs(ymin)*0.05,
            ymax + np.abs(ymax)*0.05
            ]
        if ymin != 0 else
        [-0.5, 0.5]
        )
    plot.generic(
        fig=fig, ax=ax,
        idata={
            0: [
                'line',
                [idx_bg_obs, idata[col].reindex(idx_bg_obs), 'o'],
                {'c': '#BBBBBB', 'ms': 2.0, 'mew': 0., 'zorder': 1}
                ],
            1: [
                'line',
                [idx_bg_norm, idata[col].reindex(idx_bg_norm), 'o'],
                {'c': '#005E7E', 'ms': 2.0, 'mew': 0., 'zorder': 2}
                ],
            2: [
                'line',
                [idx_bg_diff, idata[col].reindex(idx_bg_diff), 'o'],
                {'c': '#C05A00', 'ms': 2.0, 'mew': 0., 'zorder': 3}
                ],
            },
        texts=[
            {
                'x': 0.05/5 , 'y': 1.67/3 , 's': ylabel,
                'fontsize': 8,
                'ha': 'left', 'va': 'center', 'rotation': 90,
                'transform': fig.transFigure
                }, # y label
            ],
        hlines=[],
        vlines=[
            {'x': pd.to_datetime(v), 'c': '#000000', 'lw': '0.2'}
            for v in [
                    date_plot[0], bg_t_decay_end,
                    bg_t_recover, date_plot[-2]
                    ]
            ],
        xlim=[date_exc[0]-2*td_30m, date_exc[-1]+2*td_30m],
        xdate=[True, False, False],
        xticks={},
        xticklabels={},
        ylim=ylim,
        yticks={},
        yticklabels={},
        tick_fontsize=6,
        )
    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(30)
    if col == 'LLD':
        ax.text(
            pd.DatetimeIndex([date_exc[0], bg_t_decay_end]).mean(),
            ax.get_ylim()[1]*0.99,
            'Decay',
            fontsize=6, c='#C05A00',
            va='top', ha='center',
            )
        ax.text(
            pd.DatetimeIndex([bg_t_decay_end, bg_t_recover]).mean(),
            ax.get_ylim()[1]*0.99,
            'Background',
            fontsize=6, c='#005E7E',
            va='top', ha='center',
            )
        if bg_t_recover < date_exc[-1]:
            ax.text(
                date_exc[-1],
                ax.get_ylim()[1]*0.99,
                'Recovery',
                fontsize=6, c='#C05A00',
                va='top', ha='right',
                )
        ax.text(
            0.3, 0.9,
            f"{'mean':8.8s} = {bg_mean:8.4f}\n"
            f"{'std dev':8.8s} = {bg_std:8.4f}\n"
            f"{'slope':8.8s} = {bg_slope:8.4f}",
            fontsize=6, fontfamily='monospace',
            va='top', ha='left', transform=ax.transAxes
            )


def plot_cal(
        fig, ax, idata, col,
        date, date_plot, date_exc,
        cal_factor, cal_ambient, NPCPS_idx, NPCPS_amb,
        **kwargs
        ):
    r"""Plot function for calibration event.

    Parameters
    ----------
    fig : Figure
        Figure object to draw on.
    ax : Axes
        Axes object to use.
    idata : pd.DataFrame
        Input data.
    col : str
        Column name.
    kwargs : kwargs
        Kwargs from process_cal result.

    """
    td_30m = pd.to_timedelta('30min')
    idx_cal_norm = date_exc.difference([NPCPS_idx])
    idx_cal_obs = idata.index.difference(date_exc)
    # Plot
    ylabel =u'LLD (counts 30 min$^{-1}$)' if col == 'LLD' else col
    ymin = idata[col].reindex(date_plot).min()
    ymax = idata[col].reindex(date_plot).max()
    yrange = ymax - ymin
    ylim = (
        [0., ymax+np.abs(ymax)*0.05]
        if col == 'LLD' else
        [
            ymin - yrange*0.05,
            ymax + yrange*0.05
            ]
        if yrange > 0 else
        [
            ymin - np.abs(ymin)*0.05,
            ymax + np.abs(ymax)*0.05
            ]
        if ymin != 0 else
        [-0.5, 0.5]
        )
    plot.generic(
        fig=fig, ax=ax,
        idata={
            0: [
                'line',
                [idx_cal_obs, idata[col].reindex(idx_cal_obs), 'o'],
                {'c': '#BBBBBB', 'ms': 2.0, 'mew': 0., 'zorder': 1}
                ],
            1: [
                'line',
                [idx_cal_norm, idata[col].reindex(idx_cal_norm), 'o'],
                {'c': '#005E7E', 'ms': 2.0, 'mew': 0., 'zorder': 2}
                ],
            2: [
                'line',
                [NPCPS_idx, idata[col].reindex([NPCPS_idx]), 'o'],
                {'c': '#C05A00', 'ms': 2.0, 'mew': 0., 'zorder': 3}
                ],
            },
        texts=[
            {
                'x':0.05/5 , 'y':1.67/3 , 's': ylabel,
                'fontsize': 8,
                'ha': 'left', 'va': 'center', 'rotation': 90,
                'transform': fig.transFigure
                }, # y label
            ],
        hlines=[],
        vlines=[
            {'x': pd.to_datetime(v), 'c': '#000000', 'lw': '0.2'}
            for v in [date[0], NPCPS_idx, date[-1]]
            ],
        xlim=[date_exc[0]-2*td_30m, date_exc[-1]+2*td_30m],
        xdate=[True, False, False],
        xticks={},
        xticklabels={},
        ylim=ylim,
        yticks={},
        yticklabels={},
        tick_fontsize=6,
        )
    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(30)
    if col == 'LLD':
        ax.text(
            pd.DatetimeIndex([date[0], NPCPS_idx]).mean(),
            ax.get_ylim()[1]*0.1,
            'Injection Phase',
            fontsize=6, c='#005E7E',
            va='top', ha='center',
            )
        ax.text(
            pd.DatetimeIndex([NPCPS_idx, date[-2]]).mean(),
            ax.get_ylim()[1]*0.99,
            'Flushing Phase',
            fontsize=6, c='#005E7E',
            va='top', ha='center',
            )
        ax.text(
            0.6, 0.8,
            f'cal_factor = {cal_factor:8.4f}',
            fontsize=6, fontfamily='monospace',
            va='top', ha='left', transform=ax.transAxes,
            )
        ax.annotate(
            'Peak Count',
            xy=(NPCPS_idx, idata['LLD'].loc[NPCPS_idx]),
            xytext=(NPCPS_idx, idata['LLD'].loc[NPCPS_idx]),
            ha='left',
            va='bottom',
            size=6,
            c='#C05A00'
            )


def plot_norm(fig, ax, idata, col):
    r"""Plot function for normal usage.

    Parameters
    ----------
    fig : Figure
        Figure object to draw on.
    ax : Axes
        Axes object to use.
    idata : pd.DataFrame
        Input data.
    col : str
        Column name.

    """
    pdata = idata[col]
    yticks, yticklabels, pdata0 = (
        (False, False, pdata.copy())
        if pd.api.types.is_numeric_dtype(pdata) else
        util.str2plot(pdata, [])
        )
    plot.generic(
        fig=fig, ax=ax,
        idata={
            0: [  # passed flag
                'line',
                [pdata0.index, pdata0, 'o'],
                {'c': '#005E7E', 'ms': 2.0, 'mew': 0., 'zorder': 1}
                ],
            },
        texts=[
            {
                'x': 0.05/5 , 'y': 1.67/3 , 's': col,
                'fontsize': 8,
                'ha': 'left', 'va': 'center', 'rotation': 90,
                'transform': fig.transFigure
                }, # y label
            ],
        hlines=[],
        vlines=[],
        xlim=[None, None],
        xdate=[True, False, False],
        xticks={},
        xticklabels={},
        ylim=[None, None],
        yticks=(
            {'ticks': yticks}
            if yticks is not False else
            {}
            ),
        yticklabels=(
            {'labels': yticklabels}
            if yticklabels is not False else
            {}
            ),
        tick_fontsize=6,
        )
    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(30)


def plot_inspect(fig, ax, idata, col, flag_data, flags, pass_flags):
    r"""Plot function for inpection stage.

    Parameters
    ----------
    fig : Figure
        Figure object to draw on.
    ax : Axes
        Axes object to use.
    idata : pd.DataFrame
        Input data.
    col : str
        Column name.
    kwargs : kwargs
        Kwargs from process_cal result.

    """
    passed = flag_data.isin(pass_flags)
    pdata = idata[col]
    yticks, yticklabels, pdata0 = (
        (False, False, pdata.copy())
        if pd.api.types.is_numeric_dtype(pdata) else
        util.str2plot(pdata, flags)
        )
    pdata1 = pdata0.copy()
    pdata0.loc[~passed] = np.nan
    pdata1.loc[passed] = np.nan
    plot.generic(
        fig=fig, ax=ax,
        idata={
            0: [  # passed flag
                'line',
                [pdata0.index, pdata0, 'o'],
                {'c': '#005E7E', 'ms': 2.0, 'mew': 0., 'zorder': 1}
                ],
            1: [  # failed flag
                'line',
                [pdata1.index, pdata1, 'o'],
                {'c': '#C05A00', 'ms': 2.0, 'mew': 0., 'zorder': 2}
                ],
            },
        texts=[
            {
                'x': 0.05/5 , 'y': 1.67/3 , 's': col,
                'fontsize': 8,
                'ha': 'left', 'va': 'center', 'rotation': 90,
                'transform': fig.transFigure
                }, # y label
            ],
        hlines=[],
        vlines=[],
        xlim=[None, None],
        xdate=[True, False, False],
        xticks={},
        xticklabels={},
        ylim=[None, None],
        yticks=(
            {'ticks': yticks}
            if yticks is not False else
            {}
            ),
        yticklabels=(
            {'labels': yticklabels}
            if yticklabels is not False else
            {}
            ),
        tick_fontsize=6,
        )
    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(30)


def plot_temporal0(fig, ax, idata, col, lag, flag_data, flags, pass_flags):
    r"""Plot function for inpection stage.

    Parameters
    ----------
    fig : Figure
        Figure object to draw on.
    ax : Axes
        Axes object to use.
    idata : pd.DataFrame or False
        Input data. 0 is the Rn, 1 is the external data. Only the second axis
        data can be False, for which rest of the associated parameters are
        ignored.
    col : str
        Column names.
    lag : str
        Time lag for idata0.
    kwargs : kwargs
        Kwargs from process_cal result.

    """
    passed = flag_data.isin(pass_flags)
    pdata = idata[col]
    yticks, yticklabels, pdata0 = (
        (False, False, pdata.copy())
        if pd.api.types.is_numeric_dtype(pdata) else
        util.str2plot(pdata, [])
        )
    pdata0[~passed] = np.nan
    plot.generic(
        fig=fig, ax=ax,
        idata={
            0: [  # original
                'line',
                [pdata0.index, pdata0, 'o'],
                {'c': '#BBBBBB', 'ms': 2.0, 'mew': 0., 'zorder': 1}
                ],
            1: [  # passed flag
                'line',
                [pdata0.index + pd.to_timedelta(lag), pdata0, 'o'],
                {'c': '#005E7E', 'ms': 2.0, 'mew': 0., 'zorder': 1}
                ],
            },
        texts=[
            {
                'x': 0.05/5 , 'y': 1.67/3 , 's': col,
                'fontsize': 8,
                'ha': 'left', 'va': 'center', 'rotation': 90,
                'transform': fig.transFigure
                }, # y label
            ],
        hlines=[],
        vlines=[],
        xlim=[None, None],
        xdate=[True, False, False],
        xticks={},
        xticklabels={},
        ylim=[None, None],
        yticks={},
        yticklabels={},
        tick_fontsize=6,
        )
    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(30)


def plot_temporal1(fig, ax, idata, col):
    r"""Plot function for normal usage.

    Parameters
    ----------
    fig : Figure
        Figure object to draw on.
    ax : Axes
        Axes object to use.
    idata : pd.DataFrame
        Input data.
    col : str
        Column name.

    """
    pdata = idata[col]
    yticks, yticklabels, pdata0 = (
        (False, False, pdata.copy())
        if pd.api.types.is_numeric_dtype(pdata) else
        util.str2plot(pdata, [])
        )
    plot.generic(
        fig=fig, ax=ax,
        idata={
            0: [  # passed flag
                'line',
                [pdata0.index, pdata0, 'o'],
                {'c': '#C05A00', 'ms': 2.0, 'mew': 0., 'zorder': 1}
                ],
            },
        texts=[
            {
                'x': 4.95/5 , 'y': 1.67/3 , 's': col,
                'fontsize': 8,
                'ha': 'right', 'va': 'center', 'rotation': 90,
                'transform': fig.transFigure
                }, # y label
            ],
        hlines=[],
        vlines=[],
        xlim=[None, None],
        xdate=[True, False, False],
        xticks={},
        xticklabels={},
        ylim=[None, None],
        yticks=(
            {'ticks': yticks}
            if yticks is not False else
            {}
            ),
        yticklabels=(
            {'labels': yticklabels}
            if yticklabels is not False else
            {}
            ),
        tick_fontsize=6,
        )
    ax.tick_params(
            axis='both', which='major', direction='in',
            labelsize=6,
            left=False, bottom=True, right=True, top=False,
            labelleft=False, labelbottom=True,
            labelright=True, labeltop=False
            )
    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(30)


def plot_final(fig, ax, idata, col, lag, flag_data, flags, pass_flags):
    periods = flag_data.loc[
        flag_data.ne(flag_data.shift(-1)) | flag_data.ne(flag_data.shift(1))
        ]
    t_e = periods.loc[periods.ne(periods.shift(-1))].index
    t_s = pd.concat([
        periods.index[[0]].to_series(), t_e[:-1].to_series()
        ]).index
    plot.generic(
        fig=fig, ax=ax,
        idata={
            **{
                i: [
                    'line',
                    [
                        idata[col].loc[t_s[i]:t_e[i]].index
                        + pd.to_timedelta(lag),
                        idata[col].loc[t_s[i]:t_e[i]],
                        '-'
                        ],
                    {
                        'c': (
                            '#005E7E'
                            if flag_data[v] in pass_flags else
                            '#C05A00'
                            ),
                        'lw': '0.5'
                        }
                    ]
                for i, v in enumerate(t_e)
                },
            },
        texts=[
            {
                'x':0.05/5 , 'y':1.67/3 , 's': u'$^{222}$Rn (mBq m$^{-3}$)',
                'fontsize': 8,
                'ha': 'left', 'va': 'center', 'rotation': 90,
                'transform': fig.transFigure
                }, # y label
            ],
        hlines=[],
        vlines=[],
        xlim=[None, None],
        xdate=[True, False, False],
        ylim=[0., 8.e3],
        tick_fontsize=6,
        )
    for label in ax.get_xticklabels():
        label.set_ha("right")
        label.set_rotation(30)


def save_data(idata, ofile, fmt, t0, lag, cols):
    df = idata.copy()
    df.index = df.index + pd.to_timedelta(lag)
    w_Rn_data(
        idata=df, columns=cols, ofile=ofile, t0=t0,
        fmt=fmt, key='Rn',
        hdf_file=ofile
        )


def save_settings(settings, ofile, fmt, t0, stages, results, results_calc):
    df = pd.concat(
        [
            pd.Series(
                [v2],
                dtype=(
                    'int8' if np.issubdtype(type(v2), np.integer) else
                    'float64' if np.issubdtype(type(v2), np.floating) else
                    'str'
                    ),
                index=[t0], name=(k1, k2)
                )
            for k1, v1 in settings.items()
            if k1 in stages
            for k2, v2 in v1.items()
            ] +
        [
            pd.Series(
                [results['bg']], dtype='float64',
                index=[t0], name=('results', 'bg')
                ),
            pd.Series(
                [
                    results_calc['bg']['bg_mean']
                     if (
                             'bg' in results_calc and
                             'bg_mean' in results_calc['bg']
                             )
                     else
                     np.nan
                     ],
                dtype='float64',
                index=[t0], name=('results', 'bg_calc')
                ),
            pd.Series(
                [results['cal']], dtype='float64',
                index=[t0], name=('results', 'cal')
                ),
            pd.Series(
                [
                    results_calc['cal']['cal_factor']
                    if (
                            'cal' in results_calc and
                            'cal_factor' in results_calc['cal']
                            )
                    else
                    np.nan
                    ],
                dtype='float64',
                index=[t0], name=('results', 'cal_calc')
                ),
            ],
        axis=1,
        )
    w_settings(
        idata=df,
        ofile=ofile,
        columns=df.columns,
        t0=t0,
        fmt=fmt,
        key='Rn',
        hdf_file=ofile
        )
