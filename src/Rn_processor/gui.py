# -*- coding: utf-8 -*-
r"""
Created on Thu Apr 30 21:36:11 2020

@author: Edward Chung (edward.chung@npl.co.uk)

gui.py

GUI using TK.

"""
# Standard Library imports
import copy
import matplotlib.dates as mdate
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import re
import tkinter
import tkinter.ttk
import traceback

# Third party imports
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)

# Local imports
from Rn_processor import util, plot
from Rn_processor.io import r_ANSTO, r_GCWERKS, r_settings


class RnWindow(tkinter.Tk):
    def __init__(self):
        super(RnWindow, self).__init__()
        self.title('222Rn Processing')
        self.geometry("1800x850+0+0")
        self.resizable(width=0, height=0)
        # modifiable
        self.tab_titles = {}
        self.tab_order = []
        self.settings_f = {}  # front end
        self.settings_b = {}  # back end
        self.dtypes = {}
        self.names = {}
        self.var_names = {}
        # functions
        self.calc_funcs = {}
        self.calc_funcs_def = {}
        self.plot_funcs = {}
        self.plot_funcs_def = {}
        self.save_funcs = {}
        self.plot_vars = {}
        self.flags = []
        self.pass_flags = []
        # internal
        self.idata = False
        self.idata_ext = {}
        self.t0 = pd.to_datetime('2000-01-01')
        self.tabs = {}
        self.frames = {}
        self.flag_strs = {}
        self.results = {}
        self.results_calc = {}
        self.figs = {}  # for display
        self.axs = {}
        self.figs_save = {}  # for save
        self.axs_save = {}
        self.fig_kwargs = {}
        self.ax_kwargs = {}
        self.xlims = {}
        self.ylims = {}
        self.notebook = tkinter.ttk.Notebook(self)
        self.notebook.grid()

    def init_internal_attr(self):
        r"""

        """
        self.stages = np.array([False for k in self.tab_titles])
        self.widgets = {k: {} for k in self.tab_titles}
        self._modified = {k: {} for k in self.tab_titles}

    def init_tabs(self):
        r"""

        """
        for stage in self.tab_order:
            # create frames
            self.frames[stage] = tkinter.Frame(
                self, width=1800, height=800
                )
            # add notebook
            self.notebook.add(
                self.frames[stage], text=self.tab_titles[stage]
                )
            # widget list
            self.widgets[stage].update({
                k: {}
                for k in [
                    'label', 'dtype', 'value', 'error',
                    'button', 'checkbutton', 'canvas', 'toolbar',
                    'hscroll', 'vscroll'
                    ]
                })
            # frontend
            self.settings_f[stage] = {}
        self.update_tabs()

    def init_tabframe(self, parent):
        r"""

        Parameter
        ---------
        parent : tkinter.Frame
            Frame object to populate widgets on.

        """
        parent.grid_propagate(False)
        parent.grid_columnconfigure(index=0, minsize=800)
        parent.grid_columnconfigure(index=1, minsize=1000)
        # Pane 0
        parent.pane0 = tkinter.Frame(parent, width=800, height=800)
        parent.pane0.grid_propagate(False)
        parent.pane0.grid(row=0, column=0, sticky='EW')
        # Pane 1
        parent.pane1 = tkinter.Frame(parent, width=1000, height=800)
        parent.pane1.grid_propagate(False)
        parent.pane1.grid(row=0, column=1, sticky='EW')
        # Frame 00 - instructions
        parent.frame00 = tkinter.Frame(parent.pane0, width=800, height=100)
        parent.frame00.grid(row=0, column=0, sticky='EW')
        parent.frame00.grid_propagate(False)
        # Frame 01 - main
        parent.frame01 = tkinter.Frame(parent.pane0, width=800, height=650)
        parent.frame01.grid(row=1, column=0, sticky='EW')
        parent.frame01.grid_propagate(False)
        # Frame 02 - buttons
        parent.frame02 = tkinter.Frame(parent.pane0, width=800, height=50)
        parent.frame02.grid(row=2, column=0, sticky='EW')
        parent.frame02.grid_propagate(False)
        # Frame 10 - plot
        parent.frame10 = tkinter.Frame(parent.pane1, width=1000, height=700)
        parent.frame10.grid(row=0, column=0, sticky='EW')
        parent.frame10.grid_propagate(False)
        # Frame 11 - misc
        parent.frame11 = tkinter.Frame(parent.pane1, width=1000, height=100)
        parent.frame11.grid(row=1, column=0, sticky='EW')
        parent.frame11.grid_propagate(False)

    def init_input(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        parent = self.frames[stage]
        self.init_tabframe(parent)
        # Instructions
        parent.frame00.grid_columnconfigure(index=0, minsize=800, weight=1)
        parent.frame00.grid_rowconfigure(index=0, minsize=50, weight=1)
        tkinter.Label(
            parent.frame00, text="Enter variables.",
            bg='#ffffff', fg='#000000', justify='center'
            ).grid(row=0, column=0, sticky='NSEW')
        # Table
        self.install_table(parent.frame01, stage)
        # Button space
        parent.frame02.grid_columnconfigure(index=0, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=2, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=3, minsize=200, weight=1)
        parent.frame02.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Save Button
        self.install_button(
            parent.frame02, stage, 'save', 'Save',
            lambda *_, stage=stage: self.save_input(stage),
            row=0, column=0
            )
        # Initialise modified flag
        #self._modified['input'] = {k: False for k in self.settings_b['input']}
        for key in self.settings_b[stage]:
            self._modified[stage][key] = True
            self.mark_change(stage, key)

    def init_calc(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        parent = self.frames[stage]
        self.init_tabframe(parent)
        self.results_calc[stage] = {}
        # Instructions
        parent.frame00.grid_columnconfigure(index=0, minsize=800, weight=1)
        parent.frame00.grid_rowconfigure(index=0, minsize=50, weight=1)
        tkinter.Label(
            parent.frame00, text="Enter variables.",
            bg='#ffffff', fg='#000000', justify='left'
            ).grid(row=0, column=0, sticky='NSEW')
        # Table
        self.install_table(parent.frame01, stage)
        # Button space
        parent.frame02.grid_columnconfigure(index=0, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=2, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=3, minsize=200, weight=1)
        parent.frame02.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Save Button
        self.install_button(
            parent=parent.frame02, stage=stage,
            key='save', text='Save/Plot',
            func=lambda *_, stage=stage: self.save_calc(stage),
            row=0, column=0
            )
        # Initialise modified flag
        self._modified[stage] = {k: False for k in self.settings_b[stage]}
        for key in self.settings_b[stage]:
            self._modified[stage][key] = True
            self.mark_change(stage, key)
        # Canvas
        self.install_canvas(parent.frame10, stage)
        self.settings_f[stage]['pvar'].trace(
            'w', lambda *_, stage=stage: self.update_pvar(stage=stage)
            )
        self.refresh_canvas_lims(stage, none=False, axis='both')
        # Current value
        parent.frame11.grid_columnconfigure(index=0, weight=1)
        parent.frame11.grid_rowconfigure(index=0, weight=1)
        self.widgets[stage]['label']['current_value'] = tkinter.Label(
            parent.frame11, text='Current Value:',
            bg='#ffffff', fg='#000000', justify='left', anchor='w',
            borderwidth=2, relief='groove'
            )
        self.widgets[stage]['label']['current_value'].grid(
            row=0, column=0, sticky='NSEW'
            )

    def init_inspect(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        parent = self.frames[stage]
        self.init_tabframe(parent)
        self.results_calc[stage] = {}
        # Instructions
        parent.frame00.grid_columnconfigure(index=0, weight=1)
        parent.frame00.grid_rowconfigure(index=0, weight=1)
        tkinter.Label(
            parent.frame00,
            text=(
                "Enter start time, end time (both inclusive),"
                " flag and comments, delimited by ','.\n"
                "Delimit comments with ';;'. to treat them separately."
                ),
            bg='#ffffff', fg='#000000', justify='left'
            ).grid(row=0, column=0, sticky='NSEW')
        # Textbox
        self.install_textbox(parent.frame01, stage, 'flag-str')
        # Button space
        parent.frame02.grid_columnconfigure(index=0, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=2, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=3, minsize=200, weight=1)
        parent.frame02.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Construct from backend
        self.install_button(
            parent=parent.frame02, stage=stage,
            key='event', text='Add Events',
            func=lambda *_: self.widgets[stage]['value']['flag-str'].insert(
                'end',(
                    '\n'.join([v for v in self.flag_strs.values()])+'\n'
                    if any([v for v in self.flag_strs.values()]) else
                    ''
                    )
                ),
            row=0, column=0
            )
        # Read from file
        '''
        self.install_button(
            parent.frame02, stage, 'read', 'Read from File',
            lambda *_: '',
            row=0, column=1
            )
        '''
        # Save Button
        self.install_button(
            parent=parent.frame02, stage=stage,
            key='save', text='Save/Plot',
            func=lambda *_, stage=stage: self.save_inspect(stage),
            row=0, column=3
            )
        # Canvas
        self.install_canvas(parent.frame10, stage)
        self.settings_f[stage]['pvar'].trace(
            'w', lambda *_, stage=stage: self.update_pvar(stage=stage)
            )
        self.refresh_canvas_lims(stage, none=False, axis='both')
        # Initialise modified flag
        self._modified[stage] = {k: False for k in self.settings_b[stage]}

    def init_temporal(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        parent = self.frames[stage]
        self.init_tabframe(parent)
        self.results_calc[stage] = {}
        stage2 = f"{stage}_ext"
        # Instructions
        parent.frame00.grid_columnconfigure(index=0, weight=1)
        parent.frame00.grid_rowconfigure(index=0, weight=1)
        tkinter.Label(
            parent.frame00,
            text=(
                "Enter variables.\n"
                "Use bottom left controls to read and plot data from"
                " an external file."
                ),
            bg='#ffffff', fg='#000000', justify='left'
            ).grid(row=0, column=0, sticky='NSEW')
        # Table
        self.install_table(parent.frame01, stage)
        # Button space
        parent.frame02.grid_columnconfigure(index=0, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=2, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=3, minsize=200, weight=1)
        parent.frame02.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Save Button
        self.install_button(
            parent=parent.frame02, stage=stage, key='save', text='Save/Plot',
            func=lambda *_, stage=stage: self.save_temporal(stage),
            row=0, column=0
            )
        # Canvas
        self.install_canvas(parent.frame10, stage)
        self.settings_f[stage]['pvar'].trace(
            'w', lambda *_, stage=stage: self.update_pvar(stage=stage)
            )
        self.refresh_canvas_lims(stage, none=False, axis='both')
        # External File
        ## initialise
        if stage2 not in self.settings_f:
            self.settings_f[stage2] = {}
        if stage2 not in self.widgets:
            self.widgets[stage2] = {}
        for k in ['label', 'value', 'error', 'button'] :
            if k not in self.widgets[stage2]:
                self.widgets[stage2][k] = {}
        ## create axes
        self.refresh_canvas(stage=stage2, which='ax')
        self.axs[stage].set_zorder(2)
        self.axs[stage2].set_zorder(1)
        self.axs[stage].patch.set_visible(False)
        self.axs[stage2].axis(False)
        #self.axs[stage2].twinx(self.axs[stage])
        ## set frames
        parent.frame11.config(height=50)
        parent.frame12 = tkinter.Frame(parent.pane1, width=1000, height=50)
        parent.frame12.grid(row=2, column=0, sticky='EW')
        parent.frame12.grid_propagate(False)
        ##-# file
        parent.frame11.grid_columnconfigure(index=0, minsize=100, weight=1)
        parent.frame11.grid_columnconfigure(index=1, minsize=300, weight=1)
        parent.frame11.grid_columnconfigure(index=2, minsize=200, weight=1)
        parent.frame11.grid_columnconfigure(index=3, minsize=100, weight=1)
        ##-# variable selector
        parent.frame11.grid_columnconfigure(index=4, minsize=100, weight=1)
        parent.frame11.grid_columnconfigure(index=5, minsize=200, weight=1)
        ##-# xlim
        parent.frame12.grid_columnconfigure(index=0, minsize=50, weight=1)
        parent.frame12.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.frame12.grid_columnconfigure(index=2, minsize=50, weight=1)
        parent.frame12.grid_columnconfigure(index=3, minsize=200, weight=1)
        ##-# ylim
        parent.frame12.grid_columnconfigure(index=4, minsize=50, weight=1)
        parent.frame12.grid_columnconfigure(index=5, minsize=150, weight=1)
        parent.frame12.grid_columnconfigure(index=6, minsize=50, weight=1)
        parent.frame12.grid_columnconfigure(index=7, minsize=150, weight=1)
        ##-# set
        parent.frame12.grid_columnconfigure(index=8, minsize=100, weight=1)
        ## file
        ##=# label
        self.widgets[stage2]['label']['ifile'] = tkinter.Label(
            parent.frame11, text='Ext. File'
            )
        self.widgets[stage2]['label']['ifile'].grid(
            row=0, column=0, sticky='NSEW'
            )
        ##-# entry
        self.settings_f[stage2]['ifile'] = tkinter.StringVar()
        self.widgets[stage2]['value']['ifile'] = tkinter.Entry(
            parent.frame11, textvariable=self.settings_f[stage2]['ifile']
            )
        self.widgets[stage2]['value']['ifile'].grid(
            row=0, column=1, sticky='NSEW'
            )
        self.settings_f[stage2]['ifile'].set('')
        self.settings_f[stage2]['ifile'].trace(
            'w',
            lambda *_: self.get_value_error(stage2, 'ifile', return_empty=True)
            )
        ##-# file exists?
        self.widgets[stage2]['error']['ifile'] = tkinter.Label(
            parent.frame11, borderwidth=2, relief='sunken'
            )
        self.widgets[stage2]['error']['ifile'].grid(
            row=0, column=2, sticky='NSEW'
            )
        ##-# read button
        self.install_button(
            parent=parent.frame11, stage=stage2, key='read', text='Read',
            func=lambda *_, stage=stage2: self.open_GCWERKS(stage=stage),
            row=0, column=3
            )
        ## variable selector
        tkinter.Label(
            parent.frame11, text='Variable:',justify='left'
            ).grid(row=0, column=4, sticky='NSEW')
        self.settings_f[stage2]['pvar'] = tkinter.StringVar(self)
        self.settings_f[stage2]['pvar'].set('')
        self.settings_f[stage2]['pvar_prev'] = tkinter.StringVar(self)
        self.settings_f[stage2]['pvar_prev'].set('')
        self.widgets[stage2]['value']['pvar'] = tkinter.ttk.Combobox(
            parent.frame11, textvariable=self.settings_f[stage2]['pvar'],
            state='readonly', values=[],
            )
        self.widgets[stage2]['value']['pvar'].grid(
            row=0, column=5, sticky='NSEW'
            )
        self.settings_f[stage2]['pvar_prev'].set('')
        self.settings_f[stage2]['pvar'].trace(
            'w',
            lambda *_, stage=stage2:
                self.update_pvar(stage=stage, which='ax', force=True)
            )
        ## ylim
        for i, key in enumerate(['x0', 'x1', 'y0', 'y1']):
            # label
            tkinter.Label(
                parent.frame12, text=f'{key}:', justify='left'
                ).grid(row=0, column=i*2, sticky='NSEW')
            # frontend
            self.settings_f[stage2][key] = (
                tkinter.StringVar(self)
                if key.startswith('y') else
                self.settings_f[stage][key]
                )
            # entry
            self.widgets[stage2]['value'][key] = tkinter.Entry(
                parent.frame12,
                textvariable=self.settings_f[stage2][key],
                )
            self.widgets[stage2]['value'][key].grid(
                row=0, column=i*2+1, sticky='NSEW'
                )
        self.widgets[stage2]['value']['x0'].config(state='disabled')
        self.widgets[stage2]['value']['x1'].config(state='disabled')
        ## set
        self.install_button(
            parent=parent.frame12, stage=stage2, key='lim', text='Set',
            func=lambda *_, stage=stage2: self.update_ctrl2lim(stage),
            row=0, column=8
            )
        self.widgets[stage]['button']['lim'].configure(
            command=lambda *_, stage=stage, stage2=stage2:
                [self.update_ctrl2lim(stage), self.update_ctrl2lim(stage2)]
            )
        ## initialise external data
        self.open_GCWERKS(stage=stage2)
        self.results_calc[stage2] = {}
        # Initialise modified flags
        for key in self.settings_b[stage]:
            self._modified[stage][key] = True
            self.mark_change(stage, key)

    def init_output(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        parent = self.frames[stage]
        self.init_tabframe(parent)
        self.results_calc[stage] = {}
        # Instructions
        parent.frame00.grid_columnconfigure(index=0, weight=1)
        parent.frame00.grid_rowconfigure(index=0, weight=1)
        tkinter.Label(
            parent.frame00,
            text=(
                "Enter variables.\n"
                "Data file names should not include extension"
                ),
            bg='#ffffff', fg='#000000', justify='left'
            ).grid(row=0, column=0, sticky='NSEW')
        # Table
        self.install_table(parent.frame01, stage)
        # Button space
        parent.frame02.grid_columnconfigure(index=0, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=2, minsize=200, weight=1)
        parent.frame02.grid_columnconfigure(index=3, minsize=200, weight=1)
        parent.frame02.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Save Button
        self.install_button(
            parent=parent.frame02, stage=stage,
            key='save', text='Save',
            func=lambda *_, stage=stage: self.save_output(stage),
            row=0, column=0
            )
        # Currently saving
        tkinter.Label(
            parent.frame02, text='Current File:',
            bg='#ffffff', fg='#000000', justify='left'
            ).grid(row=0, column=2, sticky='NSEW')
        self.widgets[stage]['label']['current'] = tkinter.Label(
            parent.frame02, text='',
            bg='#ffffff', fg='#000000', justify='left'
            )
        self.widgets[stage]['label']['current'].grid(
            row=0, column=3, sticky='NSEW'
            )
        # Initialise modified flag
        self._modified[stage] = {k: False for k in self.settings_b[stage]}
        for key in self.settings_b[stage]:
            self._modified[stage][key] = True
            self.mark_change(stage, key)

    def refresh_canvas(self, stage, which='both'):
        r"""Initialise/Reset figure and axes.

        Parameter
        ---------
        stage : str
            Name of the stage.
        which : str {'both', 'x', 'y'}
            Axis to refresh.

        """
        base = (
            stage if stage not in self.stage_figs else self.stage_figs[stage]
            )
        to_change = ['fig', 'ax'] if which == 'both' else [which]
        if 'fig' in to_change:
            if stage not in self.figs or not self.figs[stage]:
                self.figs[base] = plt.figure(
                    **self.fig_kwargs[base]
                    )
        if 'ax' in to_change:
            if stage not in self.axs or not self.axs[stage]:
                self.axs[stage] = self.figs[base].add_axes(
                    self.ax_kwargs[base]['rect'], label=stage,
                    **{
                        k: v for k, v in self.ax_kwargs[base].items()
                        if k != 'rect'
                        }
                    )
            else:
                self.axs[stage].cla()
            self.axs[stage].xaxis_date()
            self.axs[stage].patch.set_alpha(0.)

    def refresh_canvas_lims(self, stage, none=False, axis='both'):
        r"""Initialise/Reset figure axes limits.

        Parameter
        ---------
        stage : str
            Name of the stage.
        none : bool, optional
            If True, axes limits are set to [None, None].
        which : str {'both', 'x', 'y'}, optional
            Axis to refresh.

        """
        idata = (
            self.idata
            if stage not in self.idata_ext else
            self.idata_ext[stage]
            )
        if idata is False:
            none=True
        # Set xlim, ylim
        if axis in ['both', 'x']:
            if none:
                self.xlims[stage] = [None, None]
            else:
                self.xlims[stage] = [
                    idata.index[0]-pd.to_timedelta('30min'),
                    idata.index[-1]+pd.to_timedelta('30min')
                    ]
        if axis in ['both', 'y']:
            if none:
                self.ylims[stage] = {
                    key: [None, None] for key in self.plot_vars[stage]
                    }
            else:
                ymin = {
                    i:
                    idata[i].min()
                    if pd.api.types.is_numeric_dtype(idata[i]) else
                    util.str2plot(idata[i], self.flags)[0].min()
                    for i in self.plot_vars[stage]
                    }
                ymax = {
                    i:
                    idata[i].max()
                    if pd.api.types.is_numeric_dtype(idata[i]) else
                    util.str2plot(idata[i], self.flags)[0].max()
                    for i in self.plot_vars[stage]
                    }
                yrange = {i: ymax[i] - ymin[i] for i in self.plot_vars[stage]}
                self.ylims[stage] = {
                    i:
                    [
                        ymin[i] - yrange[i]*0.05,
                        ymax[i] + yrange[i]*0.05
                        ]
                    if yrange[i] > 0 else
                    [
                        ymin[i] - np.abs(ymin[i])*0.05,
                        ymin[i] + np.abs(ymin[i])*0.05
                        ]
                    if ymin[i] != 0 and np.isfinite(ymin[i]) else
                    [-0.05, 0.05]
                    for i in self.plot_vars[stage]
                    }

    def save_input(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        try:
            if self._modified[stage]['ifile'] or not self.stages[0]:
                idata = r_ANSTO(self.get_value(stage, 'ifile'))
                if len(idata.index) == 0:
                    raise Exception("No data are present in the input file")
                check_date = True
            elif self._modified[stage]['t0']:
                idata = self.idata
                check_date = True
            else:
                check_date = False
            if check_date:
                t0 = pd.to_datetime(self.get_value(stage, 't0'))
                tmin = (
                    idata.index[0].floor('d') - pd.offsets.MonthBegin(1)
                    )
                tmax = (
                    idata.index[-1].floor('d') + pd.offsets.MonthBegin(1)
                    )
                if t0 > tmin and tmax < t0:
                    raise Exception(
                        "t0 is outside the temporal range of input data."
                        )
            if self.get_value(stage, 'prev_sfile'):
                self.read_settings(stage)
        except Exception as err:
            self.stages[0] = False
            self.error_handler(err)
            self.update_tabs()
        else:
            # Set data
            if self._modified[stage]['ifile'] or not self.stages[0]:
                self.idata = idata
                self.t0 = self.get_value(stage, 't0')
                self.idata['t0'] = pd.to_datetime(self.t0)
                self.idata[self.var_names['bg']] = np.nan
                self.idata[self.var_names['cal']] = np.nan
                self.idata[self.var_names['Rn']] = np.nan
                self.idata[self.var_names['flag']] = '0'
                self.idata[self.var_names['comments']] = ''
            if self._modified[stage]['t0']:
                self.t0 = self.get_value(stage, 't0')
                self.idata['t0'] = pd.to_datetime(self.t0)
            # Update settings
            self.update_backend(stage)
            if not self.get_value(stage, 'prev_sfile'):
                self.widgets[stage]['error']['prev_sfile'].config(text='')
            # Enable/Disable tabs
            self.stages[0] = True
            self.stages[1:] = False
            self.update_tabs()
            # Initialise Figures
            self.update_subseq_canvas()
            # Set output filenames
            if 'output' in self.settings_b:
                for key in self.settings_f['output']:
                    if key.endswith(('file',)):
                        self.settings_f['output'][key].set(
                            f"{self.get_value(stage, 'site')}_222Rn_"
                            f"{pd.to_datetime(self.t0).strftime('%Y')}_{key}"
                            )
                    if key.endswith(('fig',)):
                        self.settings_f['output'][key].set(
                            f"{self.get_value(stage, 'site')}_222Rn_"
                            f"{pd.to_datetime(self.t0).strftime('%Y%m')}_{key}"
                            )

    def read_settings(self, stage):
        r"""Read settings file

        """
        ifile = self.get_value(stage, 'prev_sfile')
        prev = r_settings(
            ifile, fmt='csv' if ifile.endswith('csv') else 'hdf',
            key='Rn'
            )
        t0 = self.get_value(stage, 't0')
        stages = [
            k for k in prev.loc[t0].index.levels[0]
            if k not in ['results']
            ]
        if t0 in prev.index:
            self.settings_b.update(
                {k: prev.loc[t0].xs(k).to_dict() for k in stages}
                )
        for s in stages:
            self.update_frontend(s)

    def save_calc(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        try:
            main, settings, result = self.calc_funcs[stage]()
            tab_state = True
        except Exception as err:
            main, settings, result = self.calc_funcs_def[stage]()
            tab_state = False
            self.error_handler(err)
        # Update settings
        for key, value in settings.items():
            self.settings_f[stage][key].set(value)
        self.update_backend(stage)
        # Apply changes
        self.results[stage] = main
        self.results_calc[stage] = result
        self.idata[self.var_names[stage]] = main
        self.idata[self.var_names['Rn']] = (
            (self.idata['LLD'] - self.idata[self.var_names['bg']])
            / 1800  # 30 min * 60 s min-1
            / self.idata[self.var_names['cal']]
            * 1.e3  # Bq -> mBq
            )
        self.idata[self.var_names['Rn_unc']] = 0.
        self.idata[self.var_names['Rn_stp']] = (
            self.idata[self.var_names['Rn']]
            * (1013.25/288.15)  # standard atmosphere
            / (self.idata['Press']/(self.idata['AirT'] + 273.15))
            )
        self.idata[self.var_names['Rn_unc_stp']] = 0.
        # Display final value
        self.widgets[stage]['label']['current_value'].config(
            text=f"Current Value: {self.results[stage]:8.4f}"
            )
        # Enable/Disable tabs
        tabno = self.tab_order.index(stage)
        self.stages[tabno] = tab_state
        self.update_tabs()
        # Plot and save flag-comments for inspection stage
        if result:
            self.update_canvas(stage=stage, refresh_lims=True)
            flag_inst = 1 if stage == 'bg' else 2
            event_name = (
                self.var_names[stage]
                if stage in self.var_names else
                stage
                )
            self.flag_strs[stage] = (
                f"{self.results_calc[stage]['date_exc'][0]},"
                f"{self.results_calc[stage]['date_exc'][-1]},"
                f"{flag_inst},"
                f"{event_name}"
                )
        else:
            self.update_canvas(stage=stage, refresh_lims=False)
            self.flag_strs[stage] = ''
        # Update figures of subsequent figures
        self.update_subseq_canvas()

    def save_inspect(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        try:
            main, settings, result = self.calc_funcs[stage]()
            tab_state = True
        except Exception as err:
            main, settings, result = self.calc_funcs_def[stage]()
            tab_state = False
            self.error_handler(err)
        # Update settings
        for key, value in settings.items():
            self.settings_f[stage][key].set(value)
        self.update_backend(stage)
        # Apply changes
        self.results[stage] = main
        self.results_calc[stage] = result
        self.idata[main.columns] = main
        # Enable/Disable tabs
        tabno = self.tab_order.index(stage)
        self.stages[tabno] = tab_state
        self.update_tabs()
        # Plot
        self.update_canvas(stage=stage, refresh_lims=False)
        # Update figures of subsequent figures
        self.update_subseq_canvas()

    def save_temporal(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        try: # nothing to save
            for key in self.settings_f[stage]:
                if key not in self.settings_b[stage] or key not in self.dtypes:
                    continue
                self.get_value(stage, key)
            tab_state = True
        except Exception as err:
            self.error_handler(err)
            tab_state = False
        # Update settings
        self.update_backend(stage)
        # Enable/Disable tabs
        tabno = self.tab_order.index(stage)
        self.stages[tabno] = tab_state
        self.update_tabs()
        # Plot
        self.update_canvas(stage=stage, refresh_lims=False)
        # Update figures of subsequent figures
        self.update_subseq_canvas()

    def save_output(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        try:
            for key in self.settings_b[stage]:
                self.get_value(stage, key)
            tab_state = True
        except Exception as err:
            self.error_handler(err)
            tab_state = False
        # Update settings
        self.update_backend(stage)
        # Enable/Disable tabs
        tabno = self.tab_order.index(stage)
        self.stages[tabno] = tab_state
        self.update_tabs()
        # Save files
        for key, value in self.settings_b[stage].items():
            if key not in self.save_funcs:
                continue
            self.widgets[stage]['label']['current'].config(text=key)
            try:
                if key in self.plot_funcs:  # Create plots
                    self.refresh_canvas(stage=key)
                    try:
                        self.plot_funcs[key]()
                    except:
                        continue
                self.save_funcs[key]()
            except Exception as err:
                self.error_handler(err)
        self.widgets[stage]['label']['current'].config(text=f'{self.t0}')

    def update_frontend(self, stage):
        r"""Update frontend settings with backend values.

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        for key, value in self.settings_b[stage].items():
            self.settings_f[stage][key].set(value)
            self._modified[stage][key] = False

    def update_backend(self, stage):
        r"""Update backend settings with frontend values.

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        for key, value in self._modified[stage].items():
            if key in self.settings_b[stage] and value:
                self.settings_b[stage][key] = self.get_value(stage, key)
                self._modified[stage][key] = False

    def update_tabs(self):
        r"""Update tab availability.

        """
        flag = True
        for i, stage in enumerate(self.stages[:-1]):
            if stage and flag:
                self.notebook.tab(i+1, state='normal')
            else:
                flag = False
                self.notebook.tab(i+1, state='disabled')

    def update_canvas(
            self, stage, refresh_lims=False, which='both', force=False
            ):
        r"""Update canvas.

        Only the modified variables will be updated upon save.

        Parameter
        ---------
        stage : str
            Name of the stage.
        refresh_lims : boolean, obtional, defaults to False
            If True, refresh xlims and ylims.
        which : {'both', 'fig', 'ax'}
            Refresh either fig or axes or both.
        key : str
            Name of the variable.

        """
        self.refresh_canvas(stage=stage, which=which)
        pvar = self.get_value(stage, 'pvar')
        base = (
            stage if stage not in self.stage_figs else self.stage_figs[stage]
            )
        try:
            tabno = self.tab_order.index(base)
            proceed = self.stages[tabno]
        except:
            proceed = False
        if proceed or force:
            try:
                self.plot_funcs[stage]()
            except:
                self.plot_funcs_def[stage]()
        else:
            self.plot_funcs_def[stage]()
        if stage in self.stage_figs and self.stage_figs[stage] in self.figs:
            self.xlims[stage] = self.xlims[self.stage_figs[stage]]
        if None in self.xlims[stage] or refresh_lims:
            self.xlims[stage] = [
                *pd.to_datetime(
                    mdate.num2date(self.axs[stage].get_xlim()),
                    format='%Y-%m-%d %H:%M'
                    )
                ]
        else:
            self.axs[stage].set_xlim(*self.xlims[stage])
        if None in self.ylims[stage][pvar] or refresh_lims:
            self.ylims[stage][pvar] = list(self.axs[stage].get_ylim())
        else:
            self.axs[stage].set_ylim(*self.ylims[stage][pvar])
        self.update_lim2ctrl(stage)
        self.widgets[base]['canvas']['main'].draw()

    def update_subseq_canvas(self):
        r"""Recursively perform self.update_canvas.

        """
        for stage in self.figs:
            if stage not in self.tab_order:
                continue
            if self.stages[self.tab_order.index(stage)]:
                continue
            if stage not in self.settings_f or not self.settings_f[stage]:
                continue
            self.refresh_canvas_lims(stage, none=False)
            self.update_canvas(stage=stage, refresh_lims=False)
        for stage in self.axs:
            if stage in self.figs:
                continue
            if self.stages[self.tab_order.index(self.stage_figs[stage])]:
                continue
            if stage not in self.idata_ext:
                continue
            self.refresh_canvas_lims(stage, axis='y', none=False)
            self.update_canvas(stage=stage, refresh_lims=False, which='ax')

    def update_pvar(self, stage, which='both', force=False):
        r"""Redraw figure when plot variable changes.

        """
        # Remember limits
        base = (
            stage if stage not in self.stage_figs else self.stage_figs[stage]
            )
        self.xlims[stage] = [
            *pd.to_datetime(
                mdate.num2date(self.axs[base].get_xlim()),
                format='%Y-%m-%d %H:%M'
                )
            ]
        self.ylims[stage][self.get_value(stage, 'pvar_prev')] = list(
            self.axs[stage].get_ylim()
            )
        # Draw new plot
        self.refresh_canvas(stage, which=which)
        self.update_canvas(
            stage, refresh_lims=False, which=which, force=force
            )
        self.settings_f[stage]['pvar_prev'].set(self.get_value(stage, 'pvar'))

    def update_ctrl2lim(self, stage):
        r"""Update figure limits from plot controls.

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        base = (
            stage if stage not in self.stage_figs else self.stage_figs[stage]
            )
        try:
            pvar = self.get_value(stage, 'pvar')
            for key in ['x0', 'x1', 'y0', 'y1']:
                try:
                    self.get_value(stage, key)
                except:
                    raise Exception(f"Wrong value for {key}")
            self.xlims[stage][0] = pd.to_datetime(self.get_value(stage, 'x0'))
            self.xlims[stage][1] = pd.to_datetime(self.get_value(stage, 'x1'))
            self.ylims[stage][pvar][0] = self.get_value(stage, 'y0')
            self.ylims[stage][pvar][1] = self.get_value(stage, 'y1')
            self.axs[stage].set_xlim(*self.xlims[stage])
            self.axs[stage].set_ylim(*self.ylims[stage][pvar])
            self.widgets[base]['canvas']['main'].draw()
        except Exception as err:
            raise(err)

    def update_lim2ctrl(self, stage):
        r"""Update plot controls from figure limits.

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        pvar = self.get_value(stage, 'pvar')
        self.settings_f[stage]['x0'].set(
            self.xlims[stage][0].strftime('%Y-%m-%d %H:%M')
            )
        self.settings_f[stage]['x1'].set(
            self.xlims[stage][1].strftime('%Y-%m-%d %H:%M')
            )
        self.settings_f[stage]['y0'].set(self.ylims[stage][pvar][0])
        self.settings_f[stage]['y1'].set(self.ylims[stage][pvar][1])

    def update_textbox(self, stage, key):
        r"""

        Parameters
        ----------
        stage : str
            Name of the stage.
        key : str
            Name of the variable.

        """
        self.settings_f[stage][key].set(
            self.widgets[stage]['value'][key].get(1.0, tkinter.END).strip()
            )
        self.mark_change(stage, key)
        self.widgets[stage]['value'][key].edit_modified(False)

    def install_table(self, parent, stage):
        r"""

        Parameters
        ----------
        parent : tkinter.Frame
            Frame object where table gets populated.
        stage : str
            Name of the stage.

        """
        settings = self.settings_b[stage]
        # Set columm width
        col_width = [150, 150, 300, 200]
        for i, w in enumerate(col_width):
            parent.grid_columnconfigure(index=i, minsize=w, weight=1)
        # Row number
        n = 0
        # Header
        tkinter.Label(
            parent, text='Variable', justify='left'
            ).grid(row=n, column=0, sticky='ew')
        tkinter.Label(
            parent, text='Data Type', justify='left'
            ).grid(row=n, column=1, sticky='ew')
        tkinter.Label(
            parent, text='Value', justify='left'
            ).grid(row=n, column=2, sticky='ew')
        tkinter.Label(
            parent, text='Error', justify='left'
            ).grid(row=n, column=3, sticky='ew')
        # Main
        for n, (k, v) in enumerate(settings.items()):
            # Add +1 as we want to start from 1 not 0
            n += 1

            # Label
            self.widgets[stage]['label'][k] = tkinter.Label(
                parent, text=self.names[k] if k in self.names else k,
                justify='left'
                )
            self.widgets[stage]['label'][k].grid(row=n, column=0, sticky='ew')

            # Datatype
            self.widgets[stage]['dtype'][k] = tkinter.Label(
                parent,
                text=(
                    re.sub("<class '|'>", "", str(self.dtypes[k][0]))
                    if self.dtypes[k][1] == '' else
                    self.dtypes[k][1]
                    ),
                justify='left'
                )
            self.widgets[stage]['dtype'][k].grid(row=n, column=1, sticky='ew')

            # Value
            self.settings_f[stage][k] = (
                tkinter.IntVar() if self.dtypes[k][0] == int else
                tkinter.DoubleVar() if self.dtypes[k][0] == float else
                tkinter.StringVar()
                )
            if k == 'mode':
                self.widgets[stage]['value'][k] = tkinter.OptionMenu(
                    parent, self.settings_f[stage][k],
                    'calc', 'file', 'last', 'cons'
                    )
                self.settings_f[stage][k].set('calc')
            else:
                self.widgets[stage]['value'][k] = tkinter.Entry(
                    parent, textvariable=self.settings_f[stage][k]
                    )
                self.settings_f[stage][k].set(v)
            self.settings_f[stage][k].trace(
                'w',
                lambda *_, stage=stage, k=k: self.mark_change(stage, k)
                )
            self.widgets[stage]['value'][k].grid(row=n, column=2, sticky='ew')

            # Error messages
            self.widgets[stage]['error'][k] = tkinter.Label(
                parent, borderwidth=2, relief='sunken'
                )
            self.widgets[stage]['error'][k].grid(row=n, column=3, sticky='ew')
            self.widgets[stage]['error'][k].grid_propagate(False)

    def install_textbox(self, parent, stage, key):
        r"""

        Parameters
        ----------
        parent : tkinter.Frame
            Frame object where table gets populated.
        stage : str
            Name of the stage.
        key : str
            Name of the variable.

        """
        self.settings_f[stage][key] = tkinter.StringVar()
        # Textbox and scrollbar
        parent.grid_columnconfigure(index=0, minsize=780, weight=1)
        parent.grid_columnconfigure(index=1, minsize=20, weight=1)
        parent.grid_rowconfigure(index=0, minsize=530, weight=1)
        parent.grid_rowconfigure(index=1, minsize=20, weight=1)
        parent.grid_rowconfigure(index=2, minsize=100, weight=1)
        self.widgets[stage]['value'][key] = tkinter.Text(
            parent, wrap="none", cursor="arrow"
            )
        self.widgets[stage]['vscroll'][key] = tkinter.Scrollbar(
            parent, orient='vertical',
            command=self.widgets[stage]['value'][key].yview
            )
        self.widgets[stage]['hscroll'][key] = tkinter.Scrollbar(
            parent, orient='horizontal',
            command=self.widgets[stage]['value'][key].xview
            )
        self.widgets[stage]['value'][key].configure(
            yscrollcommand=self.widgets[stage]['vscroll'][key].set,
            xscrollcommand=self.widgets[stage]['hscroll'][key].set
            )
        self.widgets[stage]['value'][key].grid(row=0, column=0, sticky='NSEW')
        self.widgets[stage]['vscroll'][key].grid(row=0, column=1, sticky='NS')
        self.widgets[stage]['hscroll'][key].grid(row=1, column=0, sticky='EW')
        self.widgets[stage]['value'][key].insert(
            1.0, self.settings_b[stage][key]
            )
        self.widgets[stage]['value'][key].bind(
            '<<Modified>>',
            lambda *_, stage=stage, key=key: self.update_textbox(stage, key)
            )
        # Error messages
        self.widgets[stage]['error'][key] = tkinter.Label(
            parent, borderwidth=2, relief='sunken'
            )
        self.widgets[stage]['error'][key].grid(
            row=2, column=0, columnspan=2, sticky='nsew'
            )

    def install_button(self, parent, stage, key, text, func, row=0, column=0):
        r"""

        Parameters
        ----------
        parent : tkinter.Frame
            Frame object where table gets populated.
        stage : str
            Name of the stage.
        key : str
            Name of the variable.
        text : str
            Text to display on the button
        func : function
            Function to run when the button is clicked.
        row, column : int, optional, defaults to 0
            Row and column numbers in the parent frame grid.

        """
        self.widgets[stage]['button'][key] = tkinter.Button(
            master=parent,
            text=text, command=func
            )
        self.widgets[stage]['button'][key].grid(
            row=row, column=column, sticky='NSEW'
            )

    def install_checkbutton(
            self, parent, stage, key, text, func, row=0, column=0
            ):
        r"""

        Parameters
        ----------
        parent : tkinter.Frame
            Frame object where table gets populated.
        stage : str
            Name of the stage.
        key : str
            Name of the variable.
        text : str
            Text to display on the button
        func : function
            Function to run when the button is clicked.
        row, column : int, optional, defaults to 0
            Row and column numbers in the parent frame grid.

        """
        self.settings_f[stage][key] = tkinter.IntVar()
        self.widgets[stage]['checkbutton'][key] = tkinter.Checkbutton(
            master=parent, text=text, variable=self.settings_f[stage][key],
            onvalue=1, offvalue=0
            )
        self.widgets[stage]['checkbutton'][key].grid(
            row=row, column=column, sticky='NSEW'
            )
        self.settings_f[stage][key].trace('w', func)

    def install_canvas(self, parent, stage):
        r"""

        Parameters
        ----------
        parent : tkinter.Frame
            Frame object where table gets populated.
        stage : str
            Name of the stage.

        """
        # Frame 00 - variable selector and toolbar
        parent.frame00 = tkinter.Frame(parent, width=1000, height=50)
        parent.frame00.grid_propagate(False)
        parent.frame00.grid(row=0, column=0, sticky='NSEW')
        parent.frame00.grid_columnconfigure(index=0, minsize=100, weight=1)
        parent.frame00.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.frame00.grid_columnconfigure(index=2, minsize=700, weight=1)
        # Frame 02 - plot control
        parent.frame01 = tkinter.Frame(parent, width=1000, height=50)
        parent.frame01.grid_propagate(False)
        parent.frame01.grid(row=1, column=0, sticky='NSEW')
        ## xlim
        parent.frame01.grid_columnconfigure(index=0, minsize=50, weight=1)
        parent.frame01.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.frame01.grid_columnconfigure(index=2, minsize=50, weight=1)
        parent.frame01.grid_columnconfigure(index=3, minsize=200, weight=1)
        ## ylim
        parent.frame01.grid_columnconfigure(index=4, minsize=50, weight=1)
        parent.frame01.grid_columnconfigure(index=5, minsize=150, weight=1)
        parent.frame01.grid_columnconfigure(index=6, minsize=50, weight=1)
        parent.frame01.grid_columnconfigure(index=7, minsize=150, weight=1)
        ## set
        parent.frame01.grid_columnconfigure(index=8, minsize=100, weight=1)
        # Frame 02 - plot area
        parent.frame02 = tkinter.Frame(parent, width=1000, height=600)
        parent.frame02.grid_propagate(False)
        parent.frame02.grid(row=2, column=0, sticky='NSEW')

        # Fig, Ax
        self.refresh_canvas(stage)
        # Variable selector - set trace for list menu separately.
        tkinter.Label(
            parent.frame00, text='Variable:',justify='left'
            ).grid(row=0, column=0, sticky='nsew')
        self.settings_f[stage]['pvar'] = tkinter.StringVar(self)
        self.settings_f[stage]['pvar'].set(self.plot_vars[stage][0])
        self.settings_f[stage]['pvar_prev'] = tkinter.StringVar(self)
        self.settings_f[stage]['pvar_prev'].set(self.plot_vars[stage][0])
        self.widgets[stage]['value']['pvar'] = tkinter.OptionMenu(
            parent.frame00, self.settings_f[stage]['pvar'],
            *self.plot_vars[stage]
            )
        self.widgets[stage]['value']['pvar'].grid(
            row=0, column=1, sticky='nsew'
            )
        self.settings_f[stage]['pvar_prev'].set(self.plot_vars[stage][0])
        for i, key in enumerate(['x0', 'x1', 'y0', 'y1']):
            # label
            tkinter.Label(
                parent.frame01, text=f'{key}:', justify='left'
                ).grid(row=0, column=i*2, sticky='NSEW')
            # frontend
            self.settings_f[stage][key] = tkinter.StringVar(self)
            # entry
            self.widgets[stage]['value'][key] = tkinter.Entry(
                parent.frame01,
                textvariable=self.settings_f[stage][key],
                )
            self.widgets[stage]['value'][key].grid(
                row=0, column=i*2+1, sticky='NSEW'
                )
        # Set limit button
        self.install_button(
            parent=parent.frame01, stage=stage, key='lim', text='Set',
            func=lambda *_, stage=stage: self.update_ctrl2lim(stage),
            row=0, column=8
            )
        # Plot area
        self.widgets[stage]['canvas']['main'] = FigureCanvasTkAgg(
            self.figs[stage], master=parent.frame02
            )
        self.widgets[stage]['canvas']['main'].get_tk_widget().grid(
            row=0, column=0, sticky='NSEW'
            )
        # Toolbar
        self.widgets[stage]['toolbar']['main'] = NavigationToolbar2Tk(
            self.widgets[stage]['canvas']['main'], parent.frame00
            )
        self.widgets[stage]['toolbar']['main'].grid(
            row=0, column=2, sticky='NSEW'
            )
        self.widgets[stage]['toolbar']['main'].update()

    def install_limctrl(self, parent, stage):
        r"""

        Parameters
        ----------
        parent : tkinter.Frame
            Frame object where table gets populated.
        stage : str
            Name of the stage.

        """
        ## xlim
        parent.grid_columnconfigure(index=0, minsize=50, weight=1)
        parent.grid_columnconfigure(index=1, minsize=200, weight=1)
        parent.grid_columnconfigure(index=2, minsize=50, weight=1)
        parent.grid_columnconfigure(index=3, minsize=200, weight=1)
        ## ylim
        parent.grid_columnconfigure(index=4, minsize=50, weight=1)
        parent.grid_columnconfigure(index=5, minsize=150, weight=1)
        parent.grid_columnconfigure(index=6, minsize=50, weight=1)
        parent.grid_columnconfigure(index=7, minsize=150, weight=1)
        ## set
        parent.grid_columnconfigure(index=8, minsize=100, weight=1)

    def open_GCWERKS(self, stage):
        r"""

        Parameter
        ---------
        stage : str
            Name of the stage.

        """
        try:
            self.idata_ext[stage] = r_GCWERKS(
                self.get_value(stage, 'ifile')
                )
            nodata = False
        except:
            self.idata_ext[stage] = pd.DataFrame(
                np.nan, index=[self.t0], columns=['']
                )
            nodata = True
        self.plot_vars[stage] = self.idata_ext[stage].columns.to_list()
        self.widgets[stage]['value']['pvar']['values'] = self.plot_vars[stage]
        # Set limits
        base = (
            stage if stage not in self.stage_figs else self.stage_figs[stage]
            )
        self.xlims[stage] = self.xlims[base]
        self.refresh_canvas_lims(stage=stage, none=True, axis='y')
        # Set default variable
        self.settings_f[stage]['pvar'].set(
            self.idata_ext[stage].columns[0]
            )
        if self.idata is not False:
            self.update_canvas(
                stage=stage, refresh_lims=False, which='ax', force=False
                )
            if nodata:
                self.axs[stage].axis('off')
            else:
                self.axs[stage].axis('on')


    def get_value(self, stage, key):
        r"""Get value from frontend.

        Only the modified variables will be updated upon save.

        Parameters
        ----------
        stage : str
            Name of the stage.
        key : str
            Name of the variable.

        """
        raw = self.settings_f[stage][key].get()
        if key in self.dtypes:
            value = util.convert_str(
                s=raw,
                dtype=self.dtypes[key][0],
                str_dtype=self.dtypes[key][1],
                error='raise'
                )
        else:
            value = raw
        return value

    def get_value_error(self, stage, key, return_empty=False):
        r"""Get value from frontend with error.

        Only the modified variables will be updated upon save.

        Parameter
        ---------
        stage : str
            Name of the stage.
        key : str
            Name of the variable.
        return_empty : bool, optional, defaults to False
            If True, return empty string when exception occurs.

        """
        try:
            value = self.get_value(stage, key)
            error_text = ''
        except FileNotFoundError:
            value = self.settings_b[stage][key] if not return_empty else ''
            error_text = "File not found"
        except Exception:
            value = self.settings_b[stage][key] if not return_empty else ''
            error_text = "Wrong value for dtype"
        if key in self.widgets[stage]['error']:
            self.widgets[stage]['error'][key].config(text=error_text)
        elif error_text:
            print(f"{key}: {error_text}")
        return value

    def mark_change(self, stage, key):
        r"""Flag modified variable.

        Only the modified variables will be updated upon save.

        Parameter
        ---------
        stage : str
            Name of the stage.
        key : str
            Name of the variable.

        """
        old_val = self.settings_b[stage][key]
        new_val = self.get_value_error(stage, key)
        if old_val == new_val:
            self._modified[stage][key] = False
        else:
            self._modified[stage][key] = True
        if key == 'mode' and 'ifile' in self.settings_b[stage]:
            if new_val in ['last', 'file']:
                self.widgets[stage]['value']['ifile'].grid()
                self.settings_f[stage]['ifile'].set(
                    self.settings_b[stage]['ifile']
                    )
            else:
                self.widgets[stage]['value']['ifile'].grid_remove()
                self.settings_f[stage]['ifile'].set('')
                self.widgets[stage]['error']['ifile'].config(text='')
        if key == 'mode' and 'constant' in self.settings_b[stage]:
            if new_val == 'cons':
                self.widgets[stage]['value']['constant'].grid()
                self.settings_f[stage]['constant'].set(
                    self.settings_b[stage]['constant']
                    )
            else:
                self.widgets[stage]['value']['constant'].grid_remove()
                self.settings_f[stage]['constant'].set('0.0')
                self.widgets[stage]['error']['constant'].config(text='')

    def error_handler(self, text):
        r"""

        Parameter
        ---------
        text : str
            Error message.

        """
        print(text)
        traceback.print_exc()


class SettingsWindow(tkinter.Tk):
    def __init__(self, title, settings, dtypes, names={}):
        r"""Settings window.

        Parameters
        ----------
        title : str
            Name of the window.
        dtypes : dict
            {key: [(types), {'', 'path', 'time', 'timedelta'}]}
            Data types and subtypes of the variables.
        values : dict
            {key: value}
            Values of the variables.
        names : dict
            {key: str}
            Names of the variables.

        """
        super(SettingsWindow, self).__init__()
        self.title(title)
        self.settings = settings
        self.settings_calc = copy.deepcopy(settings)
        self.dtype = {k: v[0] for k, v in dtypes.items()}
        self.dtype_sub = {k: v[1] for k, v in dtypes.items()}
        self.dtype_str = {
            str(i): i for i in [str, float, int, type(None), list]
            }
        self.names = names
        self.idata = []
        self.geometry("1800x800+0+0")
        self.grid_propagate(False)
        self.grid_columnconfigure(index=0, minsize=800)
        self.grid_columnconfigure(index=1, minsize=1000)
        self.entry = {
            'labels': {},
            'var_dtype': {}, 'wid_dtype': {},
            'var_value': {}, 'wid_value': {},
            'error': {}
            }
        self.flags = ['0']
        self.pass_flags = ['0']
        self._modified = {}
        self.saved = True
        self.plot_vars = []
        self._pvar_prev = ''
        self.calc_func = False
        self.plot_func = False
        self._save_lock = False
        self.results = {}
        self.canvas = False
        self.fig = False
        self.ax = False
        self.fig_kwargs = {'figsize': [5, 3], 'dpi': 200}
        self.ax_kwargs = {'rect': [0.8/5, 0.6/3, 4.15/5, 2.35/3]}
        self.fig_main = False
        self.ax_main = False
        self.fig_main_kwargs = {'figsize': [5, 3], 'dpi': 300}
        self.ax_main_kwargs = {'rect': [0.6/5, 0.4/3, 4.35/5, 2.55/3]}
        self.xlim = []
        self.ylim = {}

    def draw_pane0(self):
        r"""Draw settings area.

        """
        self.pane0 = tkinter.Frame(self, width=800, height=800)
        self.pane0.grid(row=0, column=0, sticky='nsew')
        # Frame 01 - label
        self.frame01 = tkinter.Frame(self.pane0, width=800, height=50)
        self.frame01.grid(row=0, column=0, sticky='ew')
        self.frame01.grid_propagate(False)
        self.frame01.grid_columnconfigure(index=0, minsize=800, weight=1)
        self.frame01.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Frame 02 - settings
        self.frame02 = tkinter.Frame(self.pane0, width=800, height=700)
        self.frame02.grid(row=1, column=0, sticky='EW')
        self.frame02.grid_propagate(False)
        col_width = [150, 150, 300, 200]
        for i, w in enumerate(col_width):
            self.frame02.grid_columnconfigure(index=i, minsize=w, weight=1)
        # Frame 03 - buttons
        self.frame03 = tkinter.Frame(self.pane0, width=800, height=50)
        self.frame03.grid(row=2, column=0, sticky='ew')
        self.frame03.grid_propagate(False)
        self.frame03.grid_columnconfigure(index=0, minsize=200, weight=1)
        self.frame03.grid_columnconfigure(index=1, minsize=200, weight=1)
        self.frame03.grid_columnconfigure(index=2, minsize=200, weight=1)
        self.frame03.grid_columnconfigure(index=3, minsize=200, weight=1)
        self.frame03.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Instruction label
        tkinter.Label(
            self.frame01,
            text=str(
                "Enter variables."
                ),
            bg='#ffffff', fg='#000000', justify='center'
            ).grid(row=0, column=0, sticky='nsew')
        # Settings area
        n = 0  # row no
        # header
        tkinter.Label(
            self.frame02, text='Variable', justify='left'
            ).grid(row=n, column=0, sticky='ew')
        tkinter.Label(
            self.frame02, text='Data Type', justify='left'
            ).grid(row=n, column=1, sticky='ew')
        tkinter.Label(
            self.frame02, text='Value', justify='left'
            ).grid(row=n, column=2, sticky='ew')
        tkinter.Label(
            self.frame02, text='Error', justify='left'
            ).grid(row=n, column=3, sticky='ew')
        # main
        for n, (k, v) in enumerate(self.settings.items()):
            n += 1
            # Label
            self.entry['labels'][k] = tkinter.Label(
                self.frame02, text=self.names[k] if k in self.names else k,
                justify='left'
                )
            self.entry['labels'][k].grid(row=n, column=0, sticky='ew')
            # Datatype - Option Menu
            self.entry['var_dtype'][k] = tkinter.StringVar(self)
            self.entry['var_dtype'][k].set(str(type(self.settings[k])))
            if k == 'mode':
                self.entry['var_dtype'][k].trace(
                    'w',
                    self.update_mode_dtype
                    )
            else:
                self.entry['var_dtype'][k].trace(
                    'w',
                    lambda *_, k=k: self.update_dtype(k)
                    )
            self.entry['wid_dtype'][k] = tkinter.OptionMenu(
                self.frame02, self.entry['var_dtype'][k],
                *[str(i) for i in self.dtype[k]]
                )
            self.entry['wid_dtype'][k].grid(row=n, column=1, sticky='ew')
            if len(self.dtype[k]) == 1:
                self.entry['wid_dtype'][k].config(state='disabled')
            # Value
            self.entry['var_value'][k] = tkinter.StringVar()
            if k == 'mode':
                self.entry['wid_value'][k] = tkinter.OptionMenu(
                    self.frame02, self.entry['var_value'][k],
                    'calc', 'last', 'file', 'cons'
                    )
                self.entry['var_value']['mode'].set('calc')
                self.entry['var_value'][k].trace(
                    'w',
                    self.update_mode_value
                    )
            else:
                self.entry['wid_value'][k] = tkinter.Entry(
                    self.frame02, textvariable=self.entry['var_value'][k]
                    )
                if str(type(self.settings[k])) == str(type(None)):
                    self.entry['wid_value'][k].delete('0', 'end')
                    self.entry['wid_value'][k].config(state='disabled')
                elif str(type(self.settings[k])) == str(list):
                    self.entry['wid_value'][k].insert(
                        0, ', '.join(self.settings[k])
                        )
                else:
                    self.entry['wid_value'][k].insert(0, f"{self.settings[k]}")
                self.entry['var_value'][k].trace(
                    'w',
                    lambda *_, k=k: self.mark_change(k)
                    )
            self.entry['wid_value'][k].grid(row=n, column=2, sticky='ew')
            # Error messages
            self.entry['error'][k] = tkinter.Label(
                self.frame02, borderwidth=2, relief='sunken'
                )
            self.entry['error'][k].grid(row=n, column=3, sticky='ew')
            self.entry['error'][k].grid_propagate(False)
        # Checkbox for invalid
        if 'mode' in self.settings:
            self.var_invalid = tkinter.IntVar()
            self.check_invalid = tkinter.Checkbutton(
                self.frame03,
                text='Invalid',
                variable=self.var_invalid, onvalue=1, offvalue=0
                )
            self.check_invalid.grid(row=0, column=1, sticky='nsew')
            self.var_invalid.trace('w', self.update_checks)
        # Checkbox for no event
        if 'mode' in self.settings:
            self.var_noevent = tkinter.IntVar()
            self.check_noevent = tkinter.Checkbutton(
                self.frame03,
                text='No event',
                variable=self.var_noevent, onvalue=1, offvalue=0
                )
            self.check_noevent.grid(row=0, column=2, sticky='nsew')
            self.var_noevent.trace('w', self.update_checks)
        # Save button
        self.button_save_f = tkinter.Frame(self.frame03, width=100, height=50)
        self.button_save_f.grid(row=0, column=0)
        self.button_save_f.grid_columnconfigure(
            index=0, minsize=100, weight=1
            )
        self.button_save_f.grid_rowconfigure(
            index=0, minsize=50, weight=1
            )
        self.button_save = tkinter.Button(
            master=self.button_save_f,
            text='Save', command=self.save_settings
            )
        self.button_save.grid(row=0, column=0, sticky='nsew')
        # Done button
        self.button_done_f = tkinter.Frame(self.frame03, width=100, height=50)
        self.button_done_f.grid(row=0, column=3)
        self.button_done_f.grid_columnconfigure(
            index=0, minsize=100, weight=1
            )
        self.button_done_f.grid_rowconfigure(
            index=0, minsize=50, weight=1
            )
        self.button_done = tkinter.Button(
            master=self.button_done_f,
            text='Done', command=self.close
            )
        self.button_done.grid(row=0, column=0, sticky='nsew')
        # Enable/Disable widgets
        if 'mode' in self.settings and 'file' in self.settings:
            self.update_mode_dtype()
        # Initialise
        self.save_settings()

    def draw_pane1(self):
        r"""Draw plot area.

        """
        self.pane1 = tkinter.Frame(self, width=1000, height=800)
        self.pane1.grid(row=0, column=1, sticky='nsew')
        # Frame 11 - variable selector and toolbar
        self.frame11 = tkinter.Frame(self.pane1, width=1000, height=50)
        self.frame11.grid(row=0, column=0, sticky='nsew')
        self.frame11.grid_propagate(False)
        self.frame11.grid_columnconfigure(index=0, minsize=100, weight=1)
        self.frame11.grid_columnconfigure(index=1, minsize=200, weight=1)
        self.frame11.grid_columnconfigure(index=2, minsize=700, weight=1)
        # Frame 12 - control
        self.frame12 = tkinter.Frame(self.pane1, width=1000, height=50)
        self.frame12.grid(row=1, column=0, sticky='nsew')
        self.frame12.grid_propagate(False)
        self.frame12.grid_columnconfigure(index=0, minsize=50, weight=1) # xlim
        self.frame12.grid_columnconfigure(index=1, minsize=200, weight=1)
        self.frame12.grid_columnconfigure(index=2, minsize=50, weight=1)
        self.frame12.grid_columnconfigure(index=3, minsize=200, weight=1)
        self.frame12.grid_columnconfigure(index=4, minsize=50, weight=1) # ylim
        self.frame12.grid_columnconfigure(index=5, minsize=100, weight=1)
        self.frame12.grid_columnconfigure(index=6, minsize=50, weight=1)
        self.frame12.grid_columnconfigure(index=7, minsize=100, weight=1)
        self.frame12.grid_columnconfigure(index=8, minsize=100, weight=1) # set
        self.frame12.grid_columnconfigure(index=9, minsize=100, weight=1)
        # Frame 13 - figure
        self.frame13 = tkinter.Frame(self.pane1, width=1000, height=600)
        self.frame13.grid(row=2, column=0, sticky='nsew')
        self.frame13.grid_columnconfigure(index=0, minsize=1000, weight=1)
        self.frame13.grid_rowconfigure(index=0, minsize=600, weight=1)
        # Fig, Ax
        self.fig_main = plt.figure(**self.fig_main_kwargs)
        self.ax_main = self.fig_main.add_axes(
            self.ax_main_kwargs['rect'],
            **{k: v for k, v in self.ax_main_kwargs.items() if k != 'rect'}
            )
        self.init_canvas()
        # Variable selector
        tkinter.Label(
            self.frame11, text='Variable:',justify='left'
            ).grid(row=0, column=0, sticky='nsew')
        self.var_pvar = tkinter.StringVar(self)
        self.var_pvar.set(self.plot_vars[0])
        self.var_pvar.trace('w', self.update_pvar)
        self.option_pvar = tkinter.OptionMenu(
            self.frame11, self.var_pvar, *self.plot_vars
            )
        self.option_pvar.grid(row=0, column=1, sticky='nsew')
        self._pvar_prev = self.plot_vars[0]
        # x0
        tkinter.Label(
            self.frame12, text='x0:',justify='left'
            ).grid(row=0, column=0, sticky='nsew')
        self.var_x0 = tkinter.StringVar(self)
        self.entry_x0 = tkinter.Entry(
            self.frame12,
            textvariable=self.var_x0,
            )
        self.entry_x0.grid(row=0, column=1, sticky='nsew')
        # x1
        tkinter.Label(
            self.frame12, text='x1:', justify='left'
            ).grid(row=0, column=2, sticky='nsew')
        self.var_x1 = tkinter.StringVar(self)
        self.entry_x1 = tkinter.Entry(
            self.frame12,
            textvariable=self.var_x1,
            )
        self.entry_x1.grid(row=0, column=3, sticky='nsew')
        # y0
        tkinter.Label(
            self.frame12, text='y0:', justify='left'
            ).grid(row=0, column=4, sticky='nsew')
        self.var_y0 = tkinter.DoubleVar(self)
        self.entry_y0 = tkinter.Entry(
            self.frame12,
            textvariable=self.var_y0,
            )
        self.entry_y0.grid(row=0, column=5, sticky='nsew')
        # y1
        tkinter.Label(
            self.frame12, text='y1:', justify='left'
            ).grid(row=0, column=6, sticky='nsew')
        self.var_y1 = tkinter.DoubleVar(self)
        self.entry_y1 = tkinter.Entry(
            self.frame12,
            textvariable=self.var_y1,
            )
        self.entry_y1.grid(row=0, column=7, sticky='nsew')
        # Plot limit button
        self.button_lim = tkinter.Button(
            self.frame12,
            text='Set',
            command=self.update_ctrl2lim,
            )
        self.button_lim.grid(row=0, column=8, sticky='nsew')
        # Plot area
        self.init_canvas()
        self.canvas = FigureCanvasTkAgg(
            self.fig, master=self.frame13
            )
        # Toolbar
        self.toolbar_f = tkinter.Frame(self.frame11)
        self.toolbar_f.grid(row=0, column=2, sticky='nsew')
        self.toolbar = NavigationToolbar2Tk(
            self.canvas, self.toolbar_f
            )
        self.toolbar.update()
        self.canvas.get_tk_widget().grid(row=0, column=0, sticky='nsew')
        # Set xlim, ylim
        self.xlim = [
            self.idata.index[0]-pd.to_timedelta('30min'),
            self.idata.index[-1]+pd.to_timedelta('30min')
            ]
        yrange = {
            i:
            self.idata[i].max() - self.idata[i].min()
            for i in self.plot_vars
            }
        self.ylim = {
            i:
            [
                self.idata[i].min()-yrange[i]*0.05,
                self.idata[i].max()+yrange[i]*0.05
                ]
            if yrange[i] > 0 else
            [
                self.idata[i].min()-np.abs(self.idata[i].min())*0.05,
                self.idata[i].min()+np.abs(self.idata[i].min())*0.05
                ]
            if self.idata[i] != 0 else
            [-0.05, 0.05]
            for i in self.plot_vars
            }
        self.init_canvas()
        self.update_canvas()

    def get_final_value(self, k):
        r"""Convert string to desired value.

        Parameter
        ---------
        k : str
            Name of the variable.

        """
        new_dtype = self.dtype_str[self.entry['var_dtype'][k].get()]
        new_entry = self.entry['var_value'][k].get()
        try:
            new_val = util.convert_str(
                new_entry, new_dtype, self.dtype_sub[k], error='raise'
                )
            self.entry['error'][k].config(text="")
        except FileNotFoundError:
            self.entry['error'][k].config(text="File not found")
            new_val = self.settings[k]
        except Exception:
            self.entry['error'][k].config(text="Wrong value for dtype")
            new_val = self.settings[k]
        return new_val

    def update_dtype(self, k):
        r"""Update dtype to selection.

        Parameter
        ---------
        k : str
            Name of the variable.

        """
        # Delete Entry
        self.entry['wid_value'][k].delete('0', tkinter.END)
        # Disable if NoneType
        if self.entry['var_dtype'][k].get() == str(type(None)):
            self.entry['wid_value'][k].config(state='disabled')
        else:
            self.entry['wid_value'][k].config(state='normal')
        # Insert 0 if int
        if self.entry['var_dtype'][k].get() == str(int):
            self.entry['wid_value'][k].insert('0', '0')
        # Insert 0 if float
        if self.entry['var_dtype'][k].get() == str(float):
            self.entry['wid_value'][k].insert('0', '0.0')
        self.mark_change(k)

    def update_mode_dtype(self, *args):
        r"""Update dtype for mode.

        """
        self.update_mode_value()

    def update_mode_value(self, *args):
        r"""Update value for mode.

        """
        self.mark_change('mode')
        if 'file' in self.settings:
            if self.get_final_value('mode') == 'file':
                self.entry['wid_value']['file'].grid()
            else:
                self.entry['wid_value']['file'].grid_remove()
            self.mark_change('file')
        if 'constant' in self.settings:
            if self.get_final_value('mode') == 'cons':
                self.entry['wid_value']['constant'].grid()
            else:
                self.entry['wid_value']['constant'].grid_remove()
            self.mark_change('constant')

    def mark_change(self, k):
        r"""Flag modified variable.

        Only the modified variables will be updated upon save.

        Parameter
        ---------
        k : str
            Name of the variable.

        """
        old_val = self.settings[k]
        new_val = self.get_final_value(k)
        if old_val == new_val:
            self._modified[k] = False
        else:
            self._modified[k] = True
        self.update_button_states()

    def save_settings(self, *args):
        r"""Save input to data.

        """
        for k, v in self._modified.items():
            if v:
                self.settings[k] = self.get_final_value(k)
            self._modified[k] = False
        if (
                self.plot_func and self.idata is not None and
                'mode' in self.settings and
                self.settings['mode'] == 'calc' and
                not self.var_noevent.get()
                ):
            # Calculate
            try:
                self.results.update(self.calc_func(self.settings, self.idata))
            except Exception as err:
                raise Exception(err)
            else:
                # Save calculation settings
                self.settings_calc.update(self.settings)
                # Set plot variable
                self.var_pvar.set(self.plot_vars[0])
                # Axis limits
                self.xlim = [None, None]
                self.ylim = {i: [None, None] for i in self.plot_vars}
                self.init_canvas()
                self.update_canvas()
                self.saved = True
                self._save_lock = False
        self.update_button_states()

    def lock_save(self, *args):
        r"""Enable Save button and lock until self.save_settings is called.

        """
        self._save_lock = True
        self.update_button_states()

    def update_checks(self, *args):
        r"""Update checkboxes

        If "invalid" is checked, "No Event" will get disabled, and vice versa.

        """
        if self.var_invalid.get():
            self.check_noevent.config(state='disabled')
        else:
            self.check_noevent.config(state='normal')
        if self.var_noevent.get():
            self.check_invalid.config(state='disabled')
        else:
            self.check_invalid.config(state='normal')

    def update_button_states(self):
        r"""Update buttons availability

        "Done" button will get disabled if there is a change in settings. If
        there is no change, "Save" will get disabled.

        """
        if any(self._modified.values()) or self._save_lock:
            self.button_save.config(state='normal')
            self.button_done.config(state='disabled')
        else:
            self.button_save.config(state='disabled')
            self.button_done.config(state='normal')

    def update_pvar(self, *args):
        r"""Redraw figure when plot variable changes.

        """
        # Remember limits
        self.xlim = [
            *pd.to_datetime(
                mdate.num2date(self.ax.get_xlim()),
                format='%Y-%m-%d %H:%M'
                )
            ]
        self.ylim[self._pvar_prev] = list(self.ax.get_ylim())
        # Apply new
        self.init_canvas()
        self.update_canvas(main=False)
        self._pvar_prev = self.var_pvar.get()

    def update_ctrl2lim(self, *args):
        r"""Update figure limits from plot controls.

        """
        try:
            self.xlim[0] = pd.to_datetime(self.var_x0.get())
            self.xlim[1] = pd.to_datetime(self.var_x1.get())
            self.ylim[self.var_pvar.get()][0] = self.var_y0.get()
            self.ylim[self.var_pvar.get()][1] = self.var_y1.get()
            self.ax.set_xlim(*self.xlim)
            self.ax.set_ylim(*self.ylim[self.var_pvar.get()])
            self.canvas.draw()
        except:
            pass

    def update_lim2ctrl(self, *args):
        r"""Update plot controls from figure limits.

        """
        self.entry_x0.delete(0, tkinter.END)
        self.entry_x0.insert(0, self.xlim[0].strftime('%Y-%m-%d %H:%M'))
        self.entry_x1.delete(0, tkinter.END)
        self.entry_x1.insert(0, self.xlim[1].strftime('%Y-%m-%d %H:%M'))
        self.var_y0.set(self.ylim[self.var_pvar.get()][0])
        self.var_y1.set(self.ylim[self.var_pvar.get()][1])

    def init_canvas(self):
        r"""Initialise/Reset figure and axes.

        """
        if not self.fig:
            self.fig = plt.figure(**self.fig_kwargs)
        if not self.ax:
            self.ax = self.fig.add_axes(
                self.ax_kwargs['rect'],
                **{k: v for k, v in self.ax_kwargs.items() if k != 'rect'}
                )
        else:
            self.ax.cla()

    def update_canvas(self, main=True, ptype=0, *args):
        r"""Update figure and axes.

        Parameters
        ----------
        main : bool, optional (default True)
            Update main figure if true.
        ptype : int (default 0)
            Plot type. 0 uses user provided function, 1 uses built-in.

        """
        if ptype == 0 and self.calc_func and self.plot_func:
            try:
                if main:
                    self.ax_main.cla()
                    self.plot_func(
                        self.fig_main, self.ax_main, self.idata,
                        self.plot_vars[0],
                        **self.results
                        )
                self.plot_func(
                    self.fig, self.ax, self.idata, self.var_pvar.get(),
                    **self.results
                    )
            except:
                ptype = 1
        elif ptype == 0:
            ptype = 1
        if ptype == 1:
            pdata = self.idata[self.var_pvar.get()]
            yticks, yticklabels, pdata0 = (
                (False, False, pdata)
                if pd.api.types.is_numeric_dtype(pdata) else
                util.str2plot(pdata, self.flags)
                )
            plot.generic(
                fig=self.fig, ax=self.ax,
                idata={
                    0: [
                        'line',
                        [pdata0.index, pdata0, 'o'],
                        {'c': '#005E7E', 'ms': 2.0, 'mew': 0., 'zorder': 2}
                        ],
                    },
                texts=[
                    {
                        'x': 0.05/5 , 'y': 1.67/3 , 's': self.var_pvar.get(),
                        'fontsize': 8,
                        'ha': 'left', 'va': 'center', 'rotation': 90,
                        'transform': self.fig.transFigure
                        }, # y label
                    ],
                hlines=[],
                vlines=[],
                xlim=self.xlim,
                xdate=[True, False, False],
                xticks={},
                xticklabels={},
                ylim=self.ylim[self.var_pvar.get()],
                yticks=(
                    {'ticks': yticks}
                    if yticks is not False else
                    {}
                    ),
                yticklabels=(
                    {'labels': yticklabels}
                    if yticklabels is not False else
                    {}
                    ),
                tick_fontsize=6,
                )
            for label in self.ax.get_xticklabels():
                label.set_ha("right")
                label.set_rotation(30)
        # Adjust plot
        if None in self.xlim:
            self.xlim = [
                *pd.to_datetime(
                    mdate.num2date(self.ax.get_xlim()),
                    format='%Y-%m-%d %H:%M'
                    )
                ]
        else:
            self.ax.set_xlim(*self.xlim)
        if None in self.ylim[self.var_pvar.get()]:
            self.ylim[self.var_pvar.get()] = list(self.ax.get_ylim())
        else:
            self.ax.set_ylim(*self.ylim[self.var_pvar.get()])
        self.canvas.figure.dpi = 200
        self.update_lim2ctrl()
        self.canvas.draw()
        self.toolbar.update()

    def close(self):
        r"""Close GUI.

        """
        self.destroy()


class FlagWindow(tkinter.Tk):
    def __init__(self, title, idata):
        r"""Inspection window to flag data.

        Parameters
        ----------
        title : str
            Name of the window.
        dtypes : dict
            {key: [(types), {'', 'path', 'time', 'timedelta'}]}
            Data types and subtypes of the variables.
        values : dict
            {key: value}
            Values of the variables.
        names : dict
            {key: str}
            Names of the variables.

        """
        super(FlagWindow, self).__init__()
        self.title(title)
        self.settings = {}
        self.idata = idata
        self.geometry("1800x800+0+0")
        self.grid_propagate(False)
        self.grid_columnconfigure(index=0, minsize=800)
        self.grid_columnconfigure(index=1, minsize=1000)
        self.obj = {
            'labels': {},
            'var_dtype': {}, 'wid_dtype': {},
            'var_value': {}, 'wid_value': {},
            'error': {},
            'frames':{}
            }
        self.flagged = None
        self.flagged_c = None
        self.flag_name = 'Flag_a'
        self.comments_name = 'Comments_a'
        self.flags = ['0']
        self.pass_flags = ['0']
        self._modified = {}
        self.saved = True
        self.plot_vars = []
        self._pvar_prev = ''
        self.canvas = False
        self.fig = False
        self.ax = False
        self.fig_kwargs = {'figsize': [5, 3], 'dpi': 200}
        self.ax_kwargs = {'rect': [0.8/5, 0.6/3, 4.15/5, 2.35/3]}
        self.fig_main = False
        self.ax_main = False
        self.fig_main_kwargs = {'figsize': [5, 3], 'dpi': 300}
        self.ax_main_kwargs = {'rect': [0.6/5, 0.4/3, 4.35/5, 2.55/3]}
        self.xlim = []
        self.ylim = {}

    def draw_pane0(self):
        r"""Draw text area.

        """
        self.pane0 = tkinter.Frame(self, width=800, height=800)
        self.pane0.grid(row=0, column=0, sticky='nsew')
        # Frame 01 - label
        self.frame01 = tkinter.Frame(self.pane0, width=800, height=50)
        self.frame01.grid(row=0, column=0, sticky='ew')
        self.frame01.grid_propagate(False)
        self.frame01.grid_columnconfigure(index=0, minsize=800, weight=1)
        self.frame01.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Frame 02 - text
        self.frame02 = tkinter.Frame(self.pane0, width=800, height=700)
        self.frame02.grid(row=1, column=0, sticky='EW')
        self.frame02.grid_propagate(False)
        self.frame02.grid_columnconfigure(index=0, minsize=780, weight=1)
        self.frame02.grid_columnconfigure(index=1, minsize=20, weight=1)
        self.frame02.grid_rowconfigure(index=0, minsize=530, weight=1)
        self.frame02.grid_rowconfigure(index=1, minsize=20, weight=1)
        # Frame 03 - buttons
        self.frame03 = tkinter.Frame(self.pane0, width=800, height=50)
        self.frame03.grid(row=2, column=0, sticky='ew')
        self.frame03.grid_propagate(False)
        self.frame03.grid_columnconfigure(index=0, minsize=200, weight=1)
        self.frame03.grid_columnconfigure(index=1, minsize=200, weight=1)
        self.frame03.grid_columnconfigure(index=2, minsize=200, weight=1)
        self.frame03.grid_columnconfigure(index=3, minsize=200, weight=1)
        self.frame03.grid_rowconfigure(index=0, minsize=50, weight=1)
        # Instruction label
        tkinter.Label(
            self.frame01,
            text=str(
                "Enter start time, end time (both inclusive),"
                " flag and comments, delimited by ','.\n"
                "Delimit comments with ';;'. to treat them separately."
                ),
            bg='#ffffff', fg='#000000', justify='center'
            ).grid(row=0, column=0, sticky='nsew')
        # Text area
        if 'text_entry' not in self.settings:
            self.settings['text_entry'] = ''
        self.text_entry = tkinter.Text(
            self.frame02, wrap="none", cursor="arrow"
            )
        self.text_entry_vscroll = tkinter.Scrollbar(
            self.frame02, orient='vertical',
            command=self.text_entry.yview
            )
        self.text_entry_hscroll = tkinter.Scrollbar(
            self.frame02, orient='horizontal',
            command=self.text_entry.xview
            )
        self.text_entry.configure(
            yscrollcommand=self.text_entry_vscroll.set,
            xscrollcommand=self.text_entry_hscroll.set
            )
        self.text_entry.grid(row=0, column=0, sticky='nsew')
        self.text_entry_vscroll.grid(row=0, column=1, sticky='ns')
        self.text_entry_hscroll.grid(row=1, column=0, sticky='ew')
        self.text_entry.insert(1.0, self.settings['text_entry'])
        self._modified['text_entry'] = False
        self.text_entry.bind('<<Modified>>', self.update_text_entry)
        # Apply button
        self.button_apply_f = tkinter.Frame(self.frame03, width=100, height=50)
        self.button_apply_f.grid(row=0, column=0)
        self.button_apply_f.grid_columnconfigure(
            index=0, minsize=100, weight=1
            )
        self.button_apply_f.grid_rowconfigure(
            index=0, minsize=50, weight=1
            )
        self.button_apply = tkinter.Button(
            master=self.button_apply_f,
            text='Apply', command=self.apply_text
            )
        self.button_apply.grid(row=0, column=0, sticky='nsew')
        # Reset button
        self.button_reset_f = tkinter.Frame(self.frame03, width=100, height=50)
        self.button_reset_f.grid(row=0, column=1)
        self.button_reset_f.grid_columnconfigure(
            index=0, minsize=100, weight=1
            )
        self.button_reset_f.grid_rowconfigure(
            index=0, minsize=50, weight=1
            )
        self.button_reset = tkinter.Button(
            master=self.button_reset_f,
            text='Reset', command=self.reset_text
            )
        self.button_reset.grid(row=0, column=0, sticky='nsew')
        # Save button
        self.button_save_f = tkinter.Frame(self.frame03, width=100, height=50)
        self.button_save_f.grid(row=0, column=2)
        self.button_save_f.grid_columnconfigure(
            index=0, minsize=100, weight=1
            )
        self.button_save_f.grid_rowconfigure(
            index=0, minsize=50, weight=1
            )
        self.button_save = tkinter.Button(
            master=self.button_save_f,
            text='Save', command=self.save_text
            )
        self.button_save.grid(row=0, column=0, sticky='nsew')
        # Done button
        self.button_done_f = tkinter.Frame(self.frame03, width=100, height=50)
        self.button_done_f.grid(row=0, column=3)
        self.button_done_f.grid_columnconfigure(
            index=0, minsize=100, weight=1
            )
        self.button_done_f.grid_rowconfigure(
            index=0, minsize=50, weight=1
            )
        self.button_done = tkinter.Button(
            master=self.button_done_f,
            text='Done', command=self.close
            )
        self.button_done.grid(row=0, column=0, sticky='nsew')
        # Update button states
        #self.update_button_states()
        # Initialise
        self.init_flagged()
        self.save_text()

    def draw_pane1(self):
        r"""Draw plot area.

        """
        self.pane1 = tkinter.Frame(self, width=1000, height=800)
        self.pane1.grid(row=0, column=1, sticky='nsew')
        # Frame 11 - variable selector and toolbar
        self.frame11 = tkinter.Frame(self.pane1, width=1000, height=50)
        self.frame11.grid(row=0, column=0, sticky='nsew')
        self.frame11.grid_propagate(False)
        self.frame11.grid_columnconfigure(index=0, minsize=100, weight=1)
        self.frame11.grid_columnconfigure(index=1, minsize=200, weight=1)
        self.frame11.grid_columnconfigure(index=2, minsize=700, weight=1)
        # Frame 12 - control
        self.frame12 = tkinter.Frame(self.pane1, width=1000, height=50)
        self.frame12.grid(row=1, column=0, sticky='nsew')
        self.frame12.grid_propagate(False)
        self.frame12.grid_columnconfigure(index=0, minsize=50, weight=1) # xlim
        self.frame12.grid_columnconfigure(index=1, minsize=200, weight=1)
        self.frame12.grid_columnconfigure(index=2, minsize=50, weight=1)
        self.frame12.grid_columnconfigure(index=3, minsize=200, weight=1)
        self.frame12.grid_columnconfigure(index=4, minsize=50, weight=1) # ylim
        self.frame12.grid_columnconfigure(index=5, minsize=100, weight=1)
        self.frame12.grid_columnconfigure(index=6, minsize=50, weight=1)
        self.frame12.grid_columnconfigure(index=7, minsize=100, weight=1)
        self.frame12.grid_columnconfigure(index=8, minsize=100, weight=1) # set
        self.frame12.grid_columnconfigure(index=9, minsize=100, weight=1)
        # Frame 13 - figure
        self.frame13 = tkinter.Frame(self.pane1, width=1000, height=600)
        self.frame13.grid(row=2, column=0, sticky='nsew')
        self.frame13.grid_columnconfigure(index=0, minsize=1000, weight=1)
        self.frame13.grid_rowconfigure(index=0, minsize=600, weight=1)
        # Fig, Ax
        self.fig_main = plt.figure(**self.fig_main_kwargs)
        self.ax_main = self.fig_main.add_axes(
            self.ax_main_kwargs['rect'],
            **{k: v for k, v in self.ax_main_kwargs.items() if k != 'rect'}
            )
        self.init_canvas()
        # Variable selector
        tkinter.Label(
            self.frame11, text='Variable:',justify='left'
            ).grid(row=0, column=0, sticky='nsew')
        self.var_pvar = tkinter.StringVar(self)
        self.var_pvar.set(self.plot_vars[0])
        self.var_pvar.trace('w', self.update_pvar)
        self.option_pvar = tkinter.OptionMenu(
            self.frame11, self.var_pvar, *self.plot_vars
            )
        self.option_pvar.grid(row=0, column=1, sticky='nsew')
        self._pvar_prev = self.plot_vars[0]
        # x0
        tkinter.Label(
            self.frame12, text='x0:',justify='left'
            ).grid(row=0, column=0, sticky='nsew')
        self.var_x0 = tkinter.StringVar(self)
        self.entry_x0 = tkinter.Entry(
            self.frame12,
            textvariable=self.var_x0,
            )
        self.entry_x0.grid(row=0, column=1, sticky='nsew')
        # x1
        tkinter.Label(
            self.frame12, text='x1:', justify='left'
            ).grid(row=0, column=2, sticky='nsew')
        self.var_x1 = tkinter.StringVar(self)
        self.entry_x1 = tkinter.Entry(
            self.frame12,
            textvariable=self.var_x1,
            )
        self.entry_x1.grid(row=0, column=3, sticky='nsew')
        # y0
        tkinter.Label(
            self.frame12, text='y0:', justify='left'
            ).grid(row=0, column=4, sticky='nsew')
        self.var_y0 = tkinter.DoubleVar(self)
        self.entry_y0 = tkinter.Entry(
            self.frame12,
            textvariable=self.var_y0,
            )
        self.entry_y0.grid(row=0, column=5, sticky='nsew')
        # y1
        tkinter.Label(
            self.frame12, text='y1:', justify='left'
            ).grid(row=0, column=6, sticky='nsew')
        self.var_y1 = tkinter.DoubleVar(self)
        self.entry_y1 = tkinter.Entry(
            self.frame12,
            textvariable=self.var_y1,
            )
        self.entry_y1.grid(row=0, column=7, sticky='nsew')
        # Plot limit button
        self.button_lim = tkinter.Button(
            self.frame12,
            text='Set',
            command=self.update_ctrl2lim,
            )
        self.button_lim.grid(row=0, column=8, sticky='nsew')
        # Plot area
        self.init_canvas()
        self.canvas = FigureCanvasTkAgg(
            self.fig, master=self.frame13
            )
        # Toolbar
        self.toolbar_f = tkinter.Frame(self.frame11)
        self.toolbar_f.grid(row=0, column=2, sticky='nsew')
        self.toolbar = NavigationToolbar2Tk(
            self.canvas, self.toolbar_f
            )
        self.toolbar.update()
        self.canvas.get_tk_widget().grid(row=0, column=0, sticky='nsew')
        # Set xlim, ylim
        self.xlim = [
            self.idata.index[0]-pd.to_timedelta('30min'),
            self.idata.index[-1]+pd.to_timedelta('30min')
            ]
        ymin = {
            i:
            self.idata[i].min()
            if pd.api.types.is_numeric_dtype(self.idata[i]) else
            util.str2plot(self.idata[i], self.flags)[0].min()
            for i in self.plot_vars
            }
        ymax = {
            i:
            self.idata[i].max()
            if pd.api.types.is_numeric_dtype(self.idata[i]) else
            util.str2plot(self.idata[i], self.flags)[0].max()
            for i in self.plot_vars
            }
        yrange = {i: ymax[i] - ymin[i] for i in self.plot_vars}
        self.ylim = {
            i:
            [
                ymin[i] - yrange[i]*0.05,
                ymax[i] + yrange[i]*0.05
                ]
            if yrange[i] > 0 else
            [
                ymin[i] - np.abs(ymin[i])*0.05,
                ymin[i] + np.abs(ymin[i])*0.05
                ]
            for i in self.plot_vars
            }
        self.init_canvas()
        self.update_canvas()

    def update_text_entry(self, *args):
        r"""Update text entry.

        Mark change if detected.

        """
        self.mark_change('text_entry')

    def mark_change(self, k):
        r"""Flag modified variable.

        Only the modified variables will be updated upon save.

        Parameter
        ---------
        k : str
            Name of the variable.

        """
        old_var = self.settings[k]
        new_var = (
            self.text_entry.get('1.0', tkinter.END).strip()
            if k in 'text_entry' else
            ''
            )
        if k in ['text_entry']:
            self.text_entry.edit_modified(False)
        if old_var != new_var:
            self._modified[k] = True
        else:
            self._modified[k] = False
        self.update_button_states()

    def init_flagged(self):
        r"""Initialise flag-comments data.

        """
        if self.flagged is None:
            self.flagged = pd.concat(
                [
                    self.idata[self.flag_name].copy()
                    if self.flag_name in self.idata else
                    pd.Series(
                        '0', index=self.idata.index, name=self.flag_name
                        )
                    ,
                    self.idata[self.comments_name].copy()
                    if self.comments_name in self.idata else
                    pd.Series(
                        '', index=self.idata.index, name=self.comments_name
                        )
                    ],
                axis=1
                )
        if self.flagged_c is None:
            self.flagged_c = copy.deepcopy(self.flagged)

    def apply_text(self, *args):
        r"""Apply input.

        """
        self.flagged_c[[self.flag_name, self.comments_name]] = pd.concat(
            util.flag_str2series(
                self.idata.index,
                self.text_entry.get('1.0', tkinter.END),
                flag_name=self.flag_name, comments_name=self.comments_name
                ),
            axis=1
            )
        self.settings['text_entry'] = util.flag_series2str(
            flag=self.flagged_c[self.flag_name],
            comments=self.flagged_c[self.comments_name]
            )
        self.text_entry.delete('1.0', tkinter.END)
        self.text_entry.insert('1.0', self.settings['text_entry'])
        self.saved = False
        self.init_canvas()
        self.update_canvas()
        self.text_entry.edit_modified(False)
        for k in self._modified:
            self._modified[k] = False
        self.update_button_states()

    def reset_text(self, *args):
        r"""Reset input.

        """
        self.flagged_c = copy.deepcopy(self.flagged)
        self.settings['text_entry'] = util.flag_series2str(
            flag=self.flagged_c[self.flag_name],
            comments=self.flagged_c[self.comments_name]
            )
        self.text_entry.delete('1.0', tkinter.END)
        self.text_entry.insert('1.0', self.settings['text_entry'])
        self.saved = True
        self.init_canvas()
        self.update_canvas()
        self.text_entry.edit_modified(False)
        for k in self._modified:
            self._modified[k] = False
        self.update_button_states()

    def save_text(self, *args):
        r"""Save input.

        Save makes permanent change.

        """
        self.flagged = copy.deepcopy(self.flagged_c)
        self.idata[[self.flag_name, self.comments_name]] = self.flagged
        self.settings['text_entry'] = util.flag_series2str(
            flag=self.flagged[self.flag_name],
            comments=self.flagged[self.comments_name]
            )
        self.text_entry.delete('1.0', tkinter.END)
        self.text_entry.insert('1.0', self.settings['text_entry'])
        self.saved = True
        self.text_entry.edit_modified(False)
        for k in self._modified:
            self._modified[k] = False
        self.update_button_states()

    def update_button_states(self):
        r"""Update buttons availability.

        "Done" button will get disabled if there is a change in settings. If
        there is no change, "Save" will get disabled.

        """
        if any(self._modified.values()):
            self.button_apply.config(state='normal')
        else:
            self.button_apply.config(state='disabled')
        if self.saved:
            self.button_save.config(state='disabled')
            self.button_reset.config(state='disabled')
            self.button_done.config(state='normal')
        else:
            self.button_save.config(state='normal')
            self.button_reset.config(state='normal')
            self.button_done.config(state='disabled')

    def update_pvar(self, *args):
        r"""Redraw figure when plot variable changes.


        """
        # Remember limits
        self.xlim = [
            *pd.to_datetime(
                mdate.num2date(self.ax.get_xlim()),
                format='%Y-%m-%d %H:%M'
                )
            ]
        self.ylim[self._pvar_prev] = list(self.ax.get_ylim())
        self.init_canvas()
        self.update_canvas()
        self.ax.set_xlim(*self.xlim)
        if None in self.ylim[self.var_pvar.get()]:
            self.ylim[self.var_pvar.get()] = list(self.ax.get_ylim())
        else:
            self.ax.set_ylim(*self.ylim[self.var_pvar.get()])
        self._pvar_prev = self.var_pvar.get()

    def update_ctrl2lim(self, *args):
        r"""Update figure limits from plot controls.

        """
        try:
            self.xlim[0] = pd.to_datetime(self.var_x0.get())
            self.xlim[1] = pd.to_datetime(self.var_x1.get())
            self.ylim[self.var_pvar.get()][0] = self.var_y0.get()
            self.ylim[self.var_pvar.get()][1] = self.var_y1.get()
            self.ax.set_xlim(*self.xlim)
            self.ax.set_ylim(*self.ylim[self.var_pvar.get()])
            self.canvas.draw()
        except:
            pass

    def update_lim2ctrl(self, *args):
        r"""Update plot controls from figure limits.

        """
        self.entry_x0.delete(0, tkinter.END)
        self.entry_x0.insert(0, self.xlim[0].strftime('%Y-%m-%d %H:%M'))
        self.entry_x1.delete(0, tkinter.END)
        self.entry_x1.insert(0, self.xlim[1].strftime('%Y-%m-%d %H:%M'))
        self.var_y0.set(self.ylim[self.var_pvar.get()][0])
        self.var_y1.set(self.ylim[self.var_pvar.get()][1])

    def init_canvas(self):
        r"""Initialise/Reset figure and axes.

        """
        if not self.fig:
            self.fig = plt.figure(**self.fig_kwargs)
        if not self.ax:
            self.ax = self.fig.add_axes(
                self.ax_kwargs['rect'],
                **{k: v for k, v in self.ax_kwargs.items() if k != 'rect'}
                )
        else:
            self.ax.cla()

    def update_canvas(self, *args):
        r"""Update figure and axes.

        """
        passed = self.flagged_c[self.flag_name].isin(self.pass_flags)
        pdata = self.idata[self.var_pvar.get()]
        yticks, yticklabels, pdata0 = (
            (False, False, pdata.copy())
            if pd.api.types.is_numeric_dtype(pdata) else
            util.str2plot(pdata, self.flags)
            )
        pdata0.loc[~passed] = np.nan
        yticks, yticklabels, pdata1 = (
            (False, False, pdata.copy())
            if pd.api.types.is_numeric_dtype(pdata) else
            util.str2plot(pdata, self.flags)
            )
        pdata1.loc[passed] = np.nan
        plot.generic(
            fig=self.fig, ax=self.ax,
            idata={
                0: [
                    'line',
                    [pdata1.index, pdata1, 'o'],
                    {'c': '#C05A00', 'ms': 2.0, 'mew': 0., 'zorder': 1}
                    ],
                1: [
                    'line',
                    [pdata0.index, pdata0, 'o'],
                    {'c': '#005E7E', 'ms': 2.0, 'mew': 0., 'zorder': 2}
                    ],
                },
            texts=[
                {
                    'x': 0.05/5 , 'y': 1.67/3 , 's': self.var_pvar.get(),
                    'fontsize': 8,
                    'ha': 'left', 'va': 'center', 'rotation': 90,
                    'transform': self.fig.transFigure
                    }, # y label
                ],
            hlines=[],
            vlines=[],
            xlim=self.xlim,
            xdate=[True, False, False],
            xticks={},
            xticklabels={},
            ylim=self.ylim[self.var_pvar.get()],
            yticks=(
                {'ticks': yticks}
                if yticks is not False else
                {}
                ),
            yticklabels=(
                {'labels': yticklabels}
                if yticklabels is not False else
                {}
                ),
            tick_fontsize=6,
            )
        for label in self.ax.get_xticklabels():
            label.set_ha("right")
            label.set_rotation(30)
        self.canvas.figure.dpi = 200
        self.update_lim2ctrl()
        self.canvas.draw()
        self.toolbar.update()

    def close(self):
        r"""Close GUI.

        """
        self.destroy()
